// generate source: 
//                  cur-function
//                  @callsite output
//                  function argument
// generate sink: 
//                  cur-function
//                  @callsite input
// condition(false/true/conditional): 
//                  generate source: conditional
//                  generate sink: conditional 

struct src{
	int* val;
	bool cond1;
    bool cond2;
};

int global = 1;

void source(src* s) {
	if(s->cond1) {
        s->val = nullptr;
        s->cond2 = true;
    } else {
        s->val = &global;
    }
}

void sink(src* s) {
	if(s->cond1 && s->cond2)
		global = *(s->val);
}


void falsesink1(src* s) {
	if(!s->cond1 || !s->cond2)
		global = *s->val;
}

void falsesink2(src* s) {
    global = *(s->val);
}


int bug() {
	src s;
    s.cond1 = true;
    s.cond2 = false;
	source(&s);
	sink(&s);
	return 0;
}

int nonBug1 () {
	src s;
    s.cond1 = true;
    s.cond2 = false;
	source(&s);
	falsesink1(&s);
	return 0;
}

int nonBug2 () {
	src s;
    s.cond1 = false;
    s.cond2 = false;
	source(&s);
    falsesink2(&s);
	// sink(s);
	return 0;
}

int main() {
	int a=1,b=2;
	int num1 = nonBug1() + nonBug2();
	int num2= bug();
	return num2 + num1;
}