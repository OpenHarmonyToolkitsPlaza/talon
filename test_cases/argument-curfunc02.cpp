// generate source: 
//                  cur-function
//                  callsite output
//                  @function argument
// generate sink: 
//                  @cur-function
//                  callsite input
// condition(false/true/conditional): 
//                  generate source: conditional
//                  generate sink: true

struct src{
	int* val;
	bool cond;
};

int global = 1;

void init(src* s) {
    if(global)
        s->cond = true;
    else
        s->cond = false;
}

void source(src* s) {
    if(s->cond)
	    s->val = 0;
    else
        s->val = &global;
}

int bug(src* s) {
    return *s->val;
}

int nonBug (src* s) {
	return 1;
}

int main() {
    src s;
    s.val = &global;
    init(&s);
    if(s.cond) 
        s.val = nullptr;
	int num1 = nonBug(&s);
	int num2= bug(&s);
	return num1 + num2;
}
