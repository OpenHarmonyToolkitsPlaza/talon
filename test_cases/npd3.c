#include<stdio.h>
#include<stdlib.h>

int *bad_callsite() {
    int i = 10;
    int *np;
    if (i>1) {
        np = NULL;
    }
    
    return np;
}

int* good_callsite() {
    int * np = NULL;
    return np;
}

int main () {
    int *p,*q,*r;
    int a = 100;
    if(a > 9) {
        p = bad_callsite();
        
    } else {
        p = good_callsite();
    }
    int res1 = *p;
    q = bad_callsite();
    r = bad_callsite();
    int res2 = *q;
    int res3 = *r;
    return 0;
}