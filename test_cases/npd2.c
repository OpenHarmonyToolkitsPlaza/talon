#include <stdio.h>
#include <stdlib.h>

int test() {
  int j = 5;
  int i = 1;

  return j;
}

int *gen_null(int i) {
  if (i)
    return NULL;
  else
    return malloc(4);
}

int trigger() {
  int *ret = gen_null(1);
  if (ret == NULL) test();

  return *ret;
}
