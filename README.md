# 毕方Talon

## talon_tools
本项目是一个端到端工具的集合，用于自动化分析鸿蒙工程下的C语言项目中的漏洞。该工具会首先使用 `IRGen` 工具将鸿蒙下的C项目代码编译生成特定的IR,再通过静态分析工具 `Talon` 分析IR，输出漏洞报告。

`Talon` 使用静态程序分析构建程序数据依赖图和稀疏值流图，并基于稀疏值流和路径条件进行路径敏感的漏洞检测，目前支持包含 `空指针解引用`、`除零错误`、`内存泄漏`、`双重释放`、`文件描述符泄漏`等类型的漏洞检查。

### 运行工具链

```shell
cd /where/is/project
python3 /where/is/talon_toolchain.py -- <build_command>
Example: python3 /Talon_tools/tool_chain/talon_toolchain.py -- make
```
如果运行遇到error:可以删除 `.IRGen` 和 `bug_reports`两个文件，重新尝试
### 运行产物
在执行命令的路径下生成以下两个文件夹:
- .IRGen: 生成的Maple IR
- bug_reports: 存放漏洞检测过程中的运行日志以及漏洞报告
    - error: 存放检测的错误日志
    - log: 存放检测的运行日志
    - total_bug.json: 漏洞报告
### 环境准备
1. 配置好 Talon 环境
2. 配置好生成特定的IR的 compiler环境(非必须)
3. 配置好 IRGen 环境
4. 配置好 `tool_chain/config.json`
5. 运行端到端工具链
### 配置IRGen
进入IRGen的目录
> 安装好python 3.6(请保持/usr/bin/python3也指向python 3.6), 将 /usr/bin/python 软链接成python3
1. apt install redis  
2. python3 -m pip install -r requirements.txt  
3. make -C ./capture  



### 配置文件config.json
位于`tool_chain/config.json`，用于设置IR生成以及漏洞检测的相关配置信息。

#### 路径配置
配置源代码编译生成IR的相关可执行文件路径
- `"MAPLE_ROOT"`:指定编译IR compiler Home目录
- `"MAPLE_CLANG"`:指定编译IR clang路径
- `"MAPLE_CLANGCPP"`:指定编译IR clang++路径
- `"MAPLE_BIN"`:指定编译IR hir2mpl路径
- `"MAPLE_GCC"`:指定编译IR  aarch64-linux-gnu-gcc路径
- `"MAPLE_MAPLE"`:指定编译IR maple路径

配置漏洞检测工具的路径
- `"talon_executable"`: 指定检测漏洞的Talon的可执行文件的具体路径

#### talon漏洞检测相关参数
#### 检查器列表
- ps-npd 空指针解引用

- ps-dbz 除零错误

- ps-ml 内存泄漏

- ps-rsa 返回栈地址

- ps-fnhm 释放非堆地址

- ps-uuv 使用未定义变量

- ps-fduac 使用失效文件描述符

- ps-dbf 双重释放

- ps-fdl 文件描述符泄漏

- ps-fdl2 文件描述符泄漏

- ps-bbsf Bad-Buffer-Size-From-System-Function

- ps-bcns Buffer-Copy-without-Checking-Input-Size

##### 性能相关时间
talon根据不同时间及分析深度需求设定了5种运行模式（由以下参数指定）：
-execution-mode=<string> 指定运行模式从快速浅层，慢速深度挖掘分别有以下5个等级：glancing/quick/normal(默认)/thorough/digging

其中5个档位设定如下：

（1）glancing : 快速得到扫描结果，深度不高，适用于需要快速结果的场景，比如程序员软件开发时的临时正确性检测 (预期30-40分钟百万行代码）

（2）quick: 标准运行查找漏洞，更加注重运行效率，适用于不希望等很久的阶段性程序检查，比如在代码提交之前的整体快速检查 (预期60-90分钟百万行代码）

（3）normal: 标准运行查找漏洞，平衡的运行效率以及查找深度，适用于阶段性程序检查，比如在主代码库变动之后的日常检查 (预期2-3小时百万行代码）

（4）thorough: 全面分析，适用于保证程序稳定性时的需求，比如在新版本对外发布以前 (预期4-6小时百万行代码）

（5）digging: 漏洞挖掘，适用于有需求发现极深层漏洞或者稳定性极度重要的场景 (预期10-15小时百万行代码）

这里5个档位主要是分析时间和分析深度的区分，digging会用较长的分析时间达到更高的召回率，而glancing使用更短时间快速浅层分析。这里不同档位都没有牺牲精度，都尽量保证低误报。另外5个档位在内存使用上只有微小差别，digging模式会略高于glancing模式。

###### 时间消耗相关参数：
-execution-mode=<string> 指定如上所述运行模式

-system-timeout=<uint> 指定最长运行时间（按秒记，默认7200），如果超时，系统将会停止执行并输出已经得到的程序漏洞挖掘结果


#### 分析能力调节参数列表（将会覆盖-execution-mode指定运行模式相关项的默认设置）
##### (重要) psa-enable-side-effect-source 启用副作用分析，默认不开启 (增加召回，降低分析速度)

##### (重要) psa-enable-arg-symbol 在确定bug时考虑由参数的具体input，开启后更精确，但降低召回

talon-restrict-cg-size=<int> 设置每个函数指针可以指向的最大函数数量，默认1（增加召回，降低分析精度，增加误报)）

inline-depth 设置函数调用追踪深度 (调节召回与分析速度, 默认为6)

enable-fd-alloc-failure 考虑文件打开可能失败的情况，默认不开启                                              

enable-heap-alloc-failure 考虑内存分配可能失败的情况，默认不开启

#### 其他可能有用的参数                                                       
##### (重要)nworkers=<uint> 指定开启的并行分析的线程数量                                                   

report-format=<string> 设置报告提示的格式，可以是html和plain-text

report-pass-line 设置报告的可靠度/分数阈值（0-100），默认1,也就是1分以上可靠度的bug都会出现在报告中

#### 报告文件格式

每次运行talon会生成一份json格式的报告文件
```json
{
    "Info": [“string”],  # 分析时一些信息说明
    "Target": "string.bc", # 输入文件
    "TotalBugs": int, # 找到的bug总数
    "TotalFunc": int, # bitcode中的函数总数
    "ExternalFuncRatio": "float string" , # bitcode中外部函数百分比
    "TotalInst": int, # bitcode中总语句数量
    "AvgInstPerFunc": "float string", # bitcode中平均每个函数语句数量
    "MaxInstPerFunc": int, # bitcode中包含语句最多的函数包含语句的数量
    "FuncWithMostInst": "string", # bitcode中包含语句最多的函数（最大的函数）
    "AvgCyclomaticComplexity": "float string", # bitcode中函数平均圈复杂度
    "MaxCyclomaticComplexity": int, # bitcode中函数最大圈复杂度
    "FuncWithMaxCyclomaticComplexity": "string", # bitcode中圈复杂度最大的函数（最复杂的函数）
    "SrcFiles": [ # 报告中包含的源文件，按数组顺序是每个源文件的ID（从0开始数）,后边报告中使用
        "string.cpp"
        "string.h" 
    ],
    "BugTypes": [ # 分类型报告列表
        {
            "Name": "Null Pointer Deference", # 漏洞类型名
            "Description": "CWE-476", # 漏洞额外注解，比如CWE编号等
            "TotalReports": int, # 同类报告数量
            "Reports": [ # 具体同类报告列表
                {
                    "Dominated": bool, # 是否被覆盖，也即当这个bug发生时是否一定在前面有同类bug发生（标为true的时候表示bug重要性降低）
                    "Score": 96, # bug可靠度，0-100 分数越高越可能是真bug
                    "Valid": true,  # 调试用吗，标记是否为真bug，只有标为true的才需要展示给用户
                    "Bug Type": "Checked NULL Dereference", # 漏洞类型名
                    "Bug Type Description": "CWE-476", # 漏洞额外注解，比如CWE编号等
                    "Importance": "string", # 漏洞重要程度 critical/high/medium/low
                    "Classification": “string” # 漏洞分类 error/security/performance等,
                    "DiagSteps": [ # 报告的漏洞详细触发步骤
                        {
                            "Inst": "string", # 当前步骤在llvm-bitcode中的对应语句（-debug-report参数开启是适用）
                            "File": int, # 当前步骤所在的源文件ID (详见report json文件最顶层SrcFiles列表)
                            "Line": int, # 当前步骤所在的源文件行号
                            "Tip": "string", # 当前步骤提示信息
                            "RawTip": "string", # 当前步骤提示信息（英文版）
                            "Importance": int, # 当前步骤的重要性 [-100, 100], -100或以下表示该步骤完全不需要展示，0以上表示该步骤更倾向与展示给用户，100或以上表示该步骤是关键步骤，一定要展示给用户。可以根据importance分量有选择的给用户展示trace信息
                            "FuncKey": "string", # 当前步骤所在函数在llvm-bitcode中的名字，不同函数的名字一定不一样
                            "FuncName": "string" # 当前步骤所在函数的人可读的名字，不同函数的名字可能一样，比如C++中的函数重载
                        },
                        {
                            "File": int,
                            "Line": int,
                            "Tip": "string",
                            "RawTip": "string",
                            "Importance": int,
                            "FuncKey": "string",
                            "FuncName": "string"
                        }
                    ],
                    "ReportChecker": "PSA Null Pointer Deference Checker" # 报告漏洞的检查器,
                },
            ]
        }
    ]
}
```

