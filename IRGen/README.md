1.apt install redis  
2.python3 -m pip install -r requirements.txt  
3.make -C ./capture  
4.编译好maple  
5. 配置`base/context`中MAPLE相关的设置(以MAPLE作为搜索关键词)  
6. 按以下说明使用工具  

``` $ cd /path/to/test-project ```  

python /path/to/IRGen/irgen_entry.py [options] -- build command  
e.g. python irgen_entry.py -- make  
python /path/to/IRGen/irgen_entry.py -- help 看更多选项说明  

输出文件在测试项目下的.IRGEN目录下，maple是生成所有maple IR的目录，top是top targets maple IR的目录，log是日志目录，其中IRGen.log是replay过程中的日志，error是replay过程中转化命令执行失败的日志  
