class CompileType(object):
    
    GCC = 0
    CLANG = 1
    LD = 3
    AR = 4
    
    OTHERS = 99


class FileType(object):
    SOURCE = 1
    ASSEMBLE = 2
    STATIC_LIB = 3
    DYNAMIC_LIB = 4
    COMPILE = 5
    LINK = 6
    UNKNOWN = 99


class Attribute(object):
    CWD = 1
    COMPILER = 2
    COMMAND = 3
    DB_HANDLER = 4
    PARSER = 5
    FILETYPE = 6
    IN_DEGREE = 7  #自顶向下
    OUT_DEGREE = 8 # 自底向上
    NAME = 9