import logging


from base.context import Context

class BuildStatistics(object):
    def __init__(self):
        self.__context = Context()
        self.__total_targets = 0
        self.__succ_targets = 0
        self.__ast_failed_targets = 0
        self.__mpl_failed_targets = 0
        self.__failed_targets = 0
        self.__skiped_targets = 0

    def update_total_targets(self, num=-1):
        self.__total_targets = num
    
    def inc_succ_targets(self):
        self.__succ_targets += 1
    
    def inc_ast_failed_targets(self):
        self.__ast_failed_targets += 1
        self.__inc_failed_targets()
    
    def inc_mpl_failed_targets(self):
        self.__mpl_failed_targets += 1
        self.__inc_failed_targets()
    
    def __inc_failed_targets(self):
        self.__failed_targets += 1
    
    def inc_skiped_targets(self):
        self.__skiped_targets += 1
        
    def print_progress_bar(self):
        print("IR Generation Schedule: %d/%d, %d total targets, %d successfully, %d failed: AST: %d, MPL: %d, %d skipped\r" % (self.__total_targets, \
                    self.__succ_targets\
                    + self.__failed_targets\
                    + self.__skiped_targets,\
                    self.__total_targets,\
                    self.__succ_targets,\
                    self.__failed_targets,\
                    self.__ast_failed_targets,\
                    self.__mpl_failed_targets,\
                    self.__skiped_targets),\
                    end='', flush=True)
    
    def export_build_statistics(self):
        import os
        import simplejson
        export_file = os.path.join(self.__context.REPORT_DIR, "export.json")
        content = {
            "total targets": self.__total_targets,
            "succ targets": self.__succ_targets,
            "failed targets": self.__failed_targets,
            "skipped targets": self.__skiped_targets,
            "ast failed target": self.__ast_failed_targets,
            "mpl failed target": self.__mpl_failed_targets
        }
        
        with open(export_file, "w") as f:
            simplejson.dump(content, f, indent=4)
    
    def fail_targets(self):
        return self.__failed_targets