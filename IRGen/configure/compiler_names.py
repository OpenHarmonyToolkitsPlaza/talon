import os
import sys
from xml.etree import ElementTree as ET

def get_names(root, xpath):
    
    items = root.findall(xpath)
    names = []
    
    for item in items:
        names.append(item.text)
    
    return names


class CompilerName(object):
        
    gcc_names = set()
    clang_names = set()
    ld_names = set()
    ar_names = set()
    ranlib_names = set()
    strip_names = set()
    objcopy_names = set()
    ccache_names = set()
    
    @staticmethod
    def load_compiler_names(compiler_config_path):
        
        root = ET.parse(compiler_config_path).getroot()
        CompilerName.gcc_names = get_names(root, "build/gcc")
        CompilerName.clang_names = get_names(root, "build/clang")
        CompilerName.ld_names = get_names(root, "build/ld")
        CompilerName.ar_names = get_names(root, "build/ar")
        CompilerName.ranlib_names = get_names(root, "build/ranlib")
        CompilerName.strip_names = get_names(root, "build/strip")
        CompilerName.objcopy_names = get_names(root, "build/objcopy")
        CompilerName.ccache_names = get_names(root, "build/ccache")
        
if __name__ == "__main__":
    path = "/root/IRGen/configure/compiler_toolchain.xml"
    CompilerName.load_compiler_names(path)
    
    print(CompilerName.clang_names)