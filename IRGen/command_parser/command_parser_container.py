import os
import sys
import shlex
import shutil


from configure.compiler_configure import *
from command_parser.option_base import *



class CmdParserContainer(object):
    def __init__(self, context=None, compiler="", options=[], cwd="", cmdgen=None):
        self.context = context
        self.cwd = cwd
        self.compiler = compiler
        self.options = options
        self.cwd = cwd
        self.cmdgen = cmdgen
        
    def delete_options_by_list(self, remove_list=[]):
        new_option = []
        remove_index = 0
        remove_size = len(remove_list)
        for item in self.options:
            if (remove_size > remove_index) \
                    and remove_list[remove_index] == item:
                remove_index += 1
            else:
                new_option.append(item)
        self.options[:] = new_option
        
    def delete_options_by_name(self, name_list=[]):
        remove_list = []
        for option in self.options:
            if not option.option:
                continue
            if option.option in name_list:
                remove_list.append(option)
        
        self.delete_options_by_list(remove_list)
            
    
    def extend_options_by_list(self, extend_list=[]):
        self.options.extend(extend_list)
        
    def auto_check_output_options(self):
        pass
            
    def index_option_by_name(self, opt_name="", start_index=0, end_index=-1):
        
        end_index = len(self.options) if end_index == -1 else end_index
        
        for index in range(start_index, end_index):
            if self.options[index].option:
                if opt_name == self.options[index].option:
                    return  True
        return False
    
    def generate_option_by_params(self, opt_name="", params=[], type=PREFIX_SIMPLE):
        
        return Option(option=opt_name, parameter=params, type=type)
    
    
    def insert_option_by_index(self, option=None, index=-1):
        
        if option is None or index == -1:
            return
        
        if index > len(self.options):
            # 如果超过合适的插入范围，插入到最后一个位置
            index = len(self.options)
            
        self.options.insert(index, option)
    
    def append_compiler_option(self):
        
        option = Option(option="-c", type=PREFIX_SIMPLE)
        self.options.append(option)
    
    
    def get_input_options(self):
        raise Exception("This is an interface left for subclasses to implement")
    
    def get_output_options(self):
        raise Exception("This is an interface left for subclasses to implement")
    
    def update_output_option(self, new_output=""):
        if not new_output:
            return
         
        output_opt = self.get_output_options()
        output_opt[0].parameter = [new_output]
    
    def get_options_by_name(self, name_list=[]):
        
        match_options = []
        
        for option in self.options:
            if option.option in name_list:
                match_options.append(option)
        
        return match_options
