from command_parser.gcc_parse.gcc_option_filter import GCCFilter

class GenCommand(object):
    def __init__(self, context=None, cwd=""):
        self.cwd = cwd
        self.context = context
        self.command_filter = None

    def generate_original_command(self, compiler="", options=[]):
        
        if not compiler or not options:
            raise Exception("It occurs fails when generate_original_command")
        
        command_line = [compiler]
        
        for option in options:
            command_line.extend(option.get_option())
        
        return command_line
        