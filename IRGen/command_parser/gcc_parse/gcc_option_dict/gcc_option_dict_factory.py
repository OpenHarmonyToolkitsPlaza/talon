import os
import sys

sys.path.insert(0, "/root/IRGen")

from command_parser.gcc_parse.gcc_option_dict.gcc_option_dict_base import GCCOPtionDictBase
from command_parser.gcc_parse.gcc_option_dict.gcc_option_dict_vers import *


class GCCOptionDictFactory(object):
    
    version_dict = {
        "4.0.4": "4.0.4.xml",
        "4.1.2": "4.1.2.xml",
        "4.2.4": "4.2.4.xml",
        "4.3.6": "4.3.6.xml",
        "4.4.7": "4.4.7.xml",
        "4.5.4": "4.5.4.xml",
        "4.6.4": "4.6.4.xml",
        "4.7.4": "4.7.4.xml",
        "4.8.5": "4.8.5.xml",
        "4.9.4": "4.9.4.xml",
        "5.5.0": "5.5.0.xml",
        "6.5.0": "6.5.0.xml",
        "7.5.0": "7.5.0.xml",
        "8.5.0": "8.5.0.xml",
        "9.5.0": "9.5.0.xml",
        "10.4.0": "10.4.0.xml",
        "11.3.0": "11.3.0.xml",
        "12.2.0": "12.2.0.xml"
    }
    
    @staticmethod
    def get_option_dict(ver=""):
        # version_list = list(GCCOptionDictFactory.version_dict.keys()).sort()
        
        # correspond_v = ver
        # if version_list:
        #     for v in version_list:
        #         if v > ver:
        #             break
        #         correspond_v = v
        
        return GCCOptionDictFactory.version_dict.get(ver, GCCOPtionDictBase)()
        
        
if __name__ == "__main__":
    for key in GCCOptionDictFactory.version_dict:
        print(key)
        GCCOptionDictFactory.get_option_dict(key)