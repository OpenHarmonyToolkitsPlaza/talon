# -*- coding:utf-8 -*-

gcc_option_index = 100


class GCCOptionCategory(object):
    OVERALL = 0 + gcc_option_index
    C_LANGUAGE = 1 + gcc_option_index
    CPP_LANGUAGE = 2 + gcc_option_index
    OBJECTIVE_C = 3 + gcc_option_index
    LANGUAGE_INDEPENDENT = 4 + gcc_option_index
    WARNING = 5 + gcc_option_index
    DEBUGGING = 6 + gcc_option_index
    OPTIMIZATION = 7 + gcc_option_index
    PREPROCESSOR = 8 + gcc_option_index
    ASSEMBLER = 9 + gcc_option_index
    LINKER = 10 + gcc_option_index
    DIRECTORY = 11 + gcc_option_index
    MACHINE_DEPENDENT = 12 + gcc_option_index
    CODE_GENERATION = 13 + gcc_option_index
    INSTRUMENTATION = 14 + gcc_option_index
    DIAGNOSTIC = 15 + gcc_option_index
    DEVELOPER = 16 + gcc_option_index
    ANALYZER = 17 + gcc_option_index
    UNKNOWN_CATEGORY = 18 + gcc_option_index
    SOURCE_FILE = 19 + gcc_option_index
    UNKNOWN_OPTION = 20 + gcc_option_index


class GCCOPtionDictBase(object):
    
    def __init__(self):
        self.option_dict = self
        
        self.gcc_simple_dict = {
            "-Wno-invalid-offsetof": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wold-style-definition": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wnested-externs": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "--target-help": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-combine": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-S": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-pipe": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-E": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-v": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-c": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-###": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "--help": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-pass-exit-codes": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "--version": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-trigraphs": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-ffreestanding": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-fno-builtin": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-funsigned-char": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-fcond-mismatch": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-ansi": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-traditional": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-fhosted": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-fms-extensions": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fsigned-bitfields": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-traditional-cpp": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-no-integrated-cpp": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-fsigned-char": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-fno-asm": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-fconserve-space": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-weak": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fcheck-new": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fstats": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-nostdinc++": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-default-inline": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-access-control": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-ffor-scope": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-enforce-eh-specs": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fuse-cxa-atexit": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-threadsafe-statics": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-operator-names": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fvisibility-inlines-hidden": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-frepo": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-gnu-keywords": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fpermissive": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-const-strings": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-elide-constructors": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-implicit-inline-templates": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-implicit-templates": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-optional-diags": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-implement-inlines": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-rtti": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-nonansi-builtins": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fgnu-runtime": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-Wundeclared-selector": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-fobjc-exceptions": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-freplace-objc-classes": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-gen-decls": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-fno-nil-receivers": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-Wselector": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-fnext-runtime": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-fzero-link": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-Wno-protocol": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-Wpadded": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wvariadic-macros": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wnonnull": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wreturn-type": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wimplicit-function-declaration": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wfatal-errors": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wuninitialized": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wunused-variable": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wunused-function": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wformat": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Winvalid-pch": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-endif-labels": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wdisabled-optimization": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-div-by-zero": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fsyntax-only": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wformat-y2k": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-deprecated-declarations": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wmissing-braces": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wmissing-format-attribute": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wmissing-noreturn": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wextra": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wmissing-field-initializers": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-multichar": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wswitch-default": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-import": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-pedantic": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wundef": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wimplicit-int": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wcast-align": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wpacked": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Winline": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wunknown-pragmas": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Werror": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wall": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wredundant-decls": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wwrite-strings": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Waggregate-return": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wunused-parameter": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wparentheses": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-pedantic-errors": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wshadow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wchar-subscripts": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wlong-long": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wswitch-enum": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wtrigraphs": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wimport": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wsign-compare": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wcast-qual": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wfloat-equal": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wpointer-arith": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-w": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wunused-label": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wunused-value": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wsequence-point": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wimplicit": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wformat-nonliteral": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wunused": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wmain": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wcomment": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wstrict-aliasing": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wunreachable-code": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wsystem-headers": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wformat-security": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wswitch": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wconversion": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-format-extra-args": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wmissing-prototypes": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wbad-function-cast": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wmissing-declarations": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wdeclaration-after-statement": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wstrict-prototypes": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-pointer-sign": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wold-style-definition": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wtraditional": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wnested-externs": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-dumpspecs": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-feliminate-unused-debug-types": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gstabs+": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gvms": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-p": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fdump-unnumbered": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-print-search-dirs": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-Q": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-save-temps": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-ftree-based-profiling": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-ftest-coverage": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gxcoff+": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fvar-tracking": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-feliminate-unused-debug-symbols": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gcoff": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-pg": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-dumpmachine": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gxcoff": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-print-multi-directory": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-ftime-report": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-ggdb": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-feliminate-dwarf2-dups": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fmem-report": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-print-libgcc-file-name": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fprofile-arcs": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-dumpversion": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gdwarf-2": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-print-multi-lib": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gstabs": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-g": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-time": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fprefetch-loop-arrays": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fomit-frame-pointer": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-funswitch-loops": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fsignaling-nans": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-ccp": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fbranch-target-load-optimize2": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-loop-im": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fbtr-bb-exclusive": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fcaller-saves": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Os": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-floop-optimize2": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-dominator-opts": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-peephole": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fsched2-use-traces": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-finline-functions-called-once": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fprofile-use": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-function-cse": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fforce-addr": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-defer-pop": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fcse-follow-jumps": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fivopts": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fgcse-las": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fkeep-inline-functions": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fcx-limited-range": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-O3": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-loop-ivcanon": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fcse-skip-blocks": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fsched-spec-load-dangerous": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fbounds-check": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-falign-labels": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-inline": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fgcse": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ffloat-store": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-falign-jumps": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-falign-loops": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-freorder-blocks-and-partition": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fif-conversion2": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-freschedule-modulo-scheduled-loops": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fmove-loop-invariants": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-funsafe-math-optimizations": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-guess-branch-probability": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fkeep-static-consts": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fdelete-null-pointer-checks": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fweb": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-O2": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-finline-functions": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-freorder-blocks": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-floop-optimize": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ffast-math": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fsingle-precision-constant": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-vectorize": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftracer": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fmerge-all-constants": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-sched-interblock": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fmodulo-sched": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-funroll-all-loops": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fbranch-probabilities": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fsched2-use-superblocks": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ffunction-sections": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-dce": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fsched-spec-load": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-fre": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-foptimize-register-move": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fstrict-aliasing": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-loop-optimize": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-math-errno": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ffinite-math-only": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fschedule-insns": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-zero-initialized-in-bss": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fif-conversion": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-branch-count-reg": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fprofile-values": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fforce-mem": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-funroll-loops": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fgcse-lm": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fstrength-reduce": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-frerun-cse-after-loop": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-ch": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-freorder-functions": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fthread-jumps": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-foptimize-sibling-calls": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fsplit-ivs-in-unroller": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fexpensive-optimizations": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-sra": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fbranch-target-load-optimize": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fspeculative-prefetching": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fgcse-after-reload": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fpeel-loops": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-O0": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-trapping-math": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-copyrename": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fdelayed-branch": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-frename-registers": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fvpt": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-loop-linear": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-pre": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fgcse-sm": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fschedule-insns2": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fvariable-expansion-in-unroller": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-ter": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-frerun-loop-opt": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-falign-functions": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fmerge-constants": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-frounding-math": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-sched-spec": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-lrs": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-O": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fprofile-generate": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fcrossjumping": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-undef": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-MP": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-H": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-MM": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-M": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-C": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-MG": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-fworking-directory": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-nostdinc": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-P": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-remap": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-static": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-pie": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-shared": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-symbolic": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-nostdlib": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-shared-libgcc": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-nostartfiles": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-nodefaultlibs": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-s": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-I-": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-mfix-r4400": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m68020-40": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m68040": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ms": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgprel-ro": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-vliw-branch": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mam33": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-metrax4": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mone-byte-bool": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtomcat-stats": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mti": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-prolog-function": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-media": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-milp32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mwords-little-endian": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msize": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips1": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m4a-single": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msingle-exit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-double": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgnu-ld": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhard-float": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mwide-bitfields": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlinker-opt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfast-fix": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mshort": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fancy-math-387": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgpr-32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdp-isr-reload": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtarget-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvxworks": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mparallel-insns": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtpf-trace": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mn": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdalign": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbcopy": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhimem": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpcrel": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvms-return-codes": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcond-exec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfix-vr4120": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m1": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpush-args": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-cond-move": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmedia": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnosb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mapcs-frame": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvolatile-asm-stop": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdisable-callt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Qn": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlong-calls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mh": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mauto-pic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-arch_errors_fatal": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcond-move": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mthumb-interwork": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mstring": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-misize": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrptb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m32r2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m32081": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-muladd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m4-nofpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfloat64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-align-stringops": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mieee": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msplit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mconst16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mprototype": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-maix-struct-return": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-malloc-cc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mregparam": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-tablejump": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbitfield": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnobitfield": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-ac0": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgp32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mimpure-text": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mparallel-mpy": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpdebug": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbranch-likely": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m32-bit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmul-bug-workaround": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdiv": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-split": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m68020": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m45": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-mult-bug": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxgot": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mieee-conformant": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-faster-structs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpa-risc-1-0": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mint64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcirrus-fix-invalid-insns": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m4-single": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m32r": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mzero-extend": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmulti-cond-exec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m5200": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhardlit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minline-float-divide-min-latency": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdouble-float": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbranch-expensive": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-bit-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mthreads": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mregister-names": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mepsilon": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcall-sysv": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-gotplt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-malign-int": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fp-ret-in-387": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minline-all-stringops": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-EL": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mprefergot": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmangle-cpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mg": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mknuthdiv": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnew-mnemonics": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mv850": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbigtable": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmmx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-maltivec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m68881": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mauto-incdec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdisable-fpregs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msoft-float": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mabicalls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbig-switch": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvliw-branch": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcsync-anomaly": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlinked-fp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcc-init": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minline-float-divide-max-throughput": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlinux": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvis": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbranch-cheap": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-ep": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mregparm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips32r2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrelocatable-lib": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpoke-function-name": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mv8plus": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-munix-asm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mid-shared-library": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgp64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbwx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mwindiss": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnested-cond-exec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfast-indirect-calls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mint32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfmovd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-bind_at_load": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdec-asm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-strict-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-id-shared-library": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-sep-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m6812": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-scc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gfull": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbcopy-builtin": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-abshi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfix-vr4130": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnomacsave": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmult-bug": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mads": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msvr4-struct-return": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlow-64k": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmuladd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbig-endian": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-threads": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbig": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-pthread": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mloop-unsigned": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m4-single-only": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minline-int-divide-min-latency": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m4a-single-only": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-macc-8": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-app-regs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m4a": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpacked-stack": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msplit-addresses": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m4al": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-flush-func": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips4": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mconstant-gp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-specld-anomaly": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m32532": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlong64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-maout": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfpr-64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpaired-single": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msoft-quad-float": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m96bit-long-double": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mac0": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m10": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-malign-double": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minline-sqrt-max-throughput": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msmall-text": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mwarn-dynamicstack": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mzarch": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-force_cpusubtype_ALL": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-sched-prolog": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfp64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-cond-exec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m68020-60": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-dword": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmvcle": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhitachi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfix-r4000": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mieee-with-inexact": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcpu32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m32032": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mint8": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-multi-cond-exec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-malign-loops": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m210": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-am33": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-flush-trap": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrenesas": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m32rx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mv850e": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-soft-float": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpadstruct": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlittle": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-all_load": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Qy": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m68060": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnop-fun-dllimport": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrtd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-nolibdld": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnohimem": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-malpha-as": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msmall-exec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtext-section-literals": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-dwarf2-asm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minline-int-divide-max-throughput": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgnu-as": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlong32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmpyi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-renesas": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-sim": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ml": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msingle-pic-base": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvr4130-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlibfuncs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-sim2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-pic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfixed-cc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mearly-stop-bits": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmvme": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-sdata": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-membedded-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-toc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m3": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msim": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m68000": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpack": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-space-regs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mv850e1": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxl-compat": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-multilib-library-pic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpe": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msep-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdivide-traps": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnoregparam": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtpcs-leaf-frame": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfused-madd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-muninit-const-in-rodata": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips3": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbk": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mslow-bytes": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfp32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-side-effects": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msio": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mupdate": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m32332": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbase-addresses": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlibrary-pic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-malign-300": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhp-ld": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-maccumulate-outgoing-args": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcallee-super-interworking": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdisable-indexing": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfix-sb1": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfdpic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlittle-endian": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-melf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msvr3-shlib": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mspace": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtpcs-frame": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtoplevel-symbols": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m68030": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m4": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdynamic-no-pic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcheck-zero-division": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-musermode": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-interrupts": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m4byte-functions": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msym32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-crt0": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfloat32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mthumb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m2e": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mint16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-prologue-epilogue": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcall-prologues": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpower": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mexplicit-relocs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mscc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcaller-super-interworking": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtiny-stack": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmad": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlongcalls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-eflags": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmultiple": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msingle-float": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-momit-leaf-frame-pointer": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdouble": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfloat-vax": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-munix": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgpr-64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-G": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcallgraph-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gused": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbranch-predict": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mspecld-anomaly": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msmall-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-nested-cond-exec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mstack-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-memb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minmax": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrelax": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mjump-in-delay": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfpr-32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-unaligned-doubles": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m32381": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m4a-nofpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-malign-labels": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m6811": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgas": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmemcpy": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minline-sqrt-min-latency": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-malign-natural": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhard-quad-float": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mabshi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbuild-constants": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mabort-on-noreturn": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-pack": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-myellowknife": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-csync-anomaly": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-nocpp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdword": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-align-loops": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mportable-runtime": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdebug": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips3d": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgnu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minline-plt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-EB": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfp-exceptions": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlong-load-store": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m40": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrelocatable": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-bundle": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-dynamiclib": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mTLS": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m3e": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbackchain": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfull-toc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-melinux": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-macc-4": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcall-netbsd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mapp-regs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-funwind-tables": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-finhibit-size-directive": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fargument-alias": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fstack-check": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-finstrument-functions": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fwrapv": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fnon-call-exceptions": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fverbose-asm": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fshort-wchar": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fshared-data": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fno-common": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fpie": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fPIC": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fpic": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fleading-underscore": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fpcc-struct-return": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fexceptions": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-freg-struct-return": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fshort-double": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fasynchronous-unwind-tables": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-ftrapv": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fno-ident": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fshort-enums": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-Wabi": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wctor-dtor-privacy": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wnon-virtual-dtor": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wreorder": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Weffc++": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-deprecated": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wstrict-null-sentinel": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-non-template-friend": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wold-style-cast": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Woverloaded-virtual": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-pmf-conversions": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wsign-promo": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Winit-self": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wmissing-include-dirs": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fdump-translation-unit": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fdump-class-hierarchy": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-Wno-init-self": {
            "category": GCCOptionCategory.WARNING,
            "action": "keep"
            },
            "-Winvalid-offsetof": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-missing-include-dirs": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-padded": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-variadic-macros": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-nonnull": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-return-type": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-implicit-function-declaration": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-fatal-errors": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-uninitialized": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-unused-variable": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-unused-function": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-format": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-invalid-pch": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wendif-labels": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-disabled-optimization": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wdiv-by-zero": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-format-y2k": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wdeprecated-declarations": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-missing-braces": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-missing-format-attribute": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-missing-noreturn": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-extra": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-missing-field-initializers": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wmultichar": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-switch-default": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wimport": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-undef": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-implicit-int": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-cast-align": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-packed": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-inline": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-unknown-pragmas": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-error": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-all": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-redundant-decls": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-write-strings": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-aggregate-return": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-unused-parameter": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-parentheses": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-shadow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-char-subscripts": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-long-long": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-switch-enum": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-trigraphs": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-import": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-sign-compare": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-cast-qual": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-float-equal": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-pointer-arith": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-unused-label": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-unused-value": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-sequence-point": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-implicit": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-format-nonliteral": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-unused": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-main": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-comment": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-strict-aliasing": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-unreachable-code": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-system-headers": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-format-security": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-switch": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-conversion": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wformat-extra-args": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-missing-prototypes": {
                "category": "C-only Warning Options",
                "action": "keep"
            },
            "-Wno-bad-function-cast": {
                "category": "C-only Warning Options",
                "action": "keep"
            },
            "-Wno-missing-declarations": {
                "category": "C-only Warning Options",
                "action": "keep"
            },
            "-Wno-declaration-after-statement": {
                "category": "C-only Warning Options",
                "action": "keep"
            },
            "-Wno-strict-prototypes": {
                "category": "C-only Warning Options",
                "action": "keep"
            },
            "-Wpointer-sign": {
                "category": "C-only Warning Options",
                "action": "keep"
            },
            "-Wno-old-style-definition": {
                "category": "C-only Warning Options",
                "action": "keep"
            },
            "-Wno-traditional": {
                "category": "C-only Warning Options",
                "action": "keep"
            },
            "-Wno-nested-externs": {
                "category": "C-only Warning Options",
                "action": "keep"
            }
        }
        self.gcc_space_dict = {
            "-x": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-o": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-aux-info": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-fmudflap": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "--param": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-A": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-Xpreprocessor": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-include": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-MT": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-iwithprefix": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-iprefix": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-D": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-U": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-isystem": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-MQ": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-idirafter": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-MF": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-imacros": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-Xassembler": {
                "category": GCCOptionCategory.ASSEMBLER,
                "action": "keep"
            },
            "-u": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-Xlinker": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-I": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-iquote": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-V": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-b": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-bundle_loader": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-G": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            }
        }
        self.gcc_equal_dict = {
            "-std": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-fabi-version": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fconstant-string-class": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-fmessage-length": {
                "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
                "action": "keep"
            },
            "-fdiagnostics-show-location": {
                "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
                "action": "keep"
            },
            "-Wformat": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wstrict-aliasing": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fsched-verbose": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-print-file-name": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-frandom-seed": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-print-prog-name": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-ftree-vectorizer-verbose": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fsched-stalled-insns": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-finline-limit": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-specs": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-msoft-reg-count": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtext": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minsert-sched-nops": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfixed-range": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mspe": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfp-trap-mode": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmcu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmax-stack-frame": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrpts": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mstack-guard": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msched-costly-dep": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mshared-library-id": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msda": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mregparm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpic-register": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msdata": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtune": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-march": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtrap-precision": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mflush-func": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmodel": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfp-rounding-mode": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mwarn-framesize": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-melinux-stacksize": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfloat-gprs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-masm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpreferred-stack-boundary": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-misel": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmemory-latency": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfpmath": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbranch-cost": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mflush-trap": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mstructure-size-boundary": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-munix": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfloat-abi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtls-size": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mschedule": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtda": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-missue-rate": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mabi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minit-stack": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mprioritize-restricted-insns": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mzda": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fvisibility": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fstack-limit-register": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fpack-struct": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-ftls-model": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-Wno-format": {
            "category": "Warning Options",
            "action": "keep"
            },
            "-Wno-strict-aliasing": {
                "category": "Warning Options",
                "action": "keep"
            }
        }
        self.gcc_match_dict = {
            "-Wno-larger-than-": {
            "category": "Warning Options",
            "action": "keep"
            },
            "-D": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-ftemplate-depth-": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wlarger-than-": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-d": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-g": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-Wa,": {
                "category": GCCOptionCategory.ASSEMBLER,
                "action": "keep"
            },
            "-Wl,": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-l": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-B": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-L": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-I": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-iquote": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-YP,": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-F": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Ym,": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fcall-used-": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-ffixed-": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fcall-saved-": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            }
        }

    def init_option_dict(self):
        self.load_change_options()
        self.update_options_dict()
    
    def load_change_options(self):
        self.discard_options = {
            "gcc_simple_dict": [],
            "gcc_space_dict": [],
            "gcc_equal_dict": [],
            "gcc_match_dict": []
        }   
        
        self.append_options = {
            "gcc_simple_dict": {},
            "gcc_space_dict": {},
            "gcc_equal_dict": {},
            "gcc_match_dict": {}
        }
    
    def update_options_dict(self):
        self.update_single_category_options(self.discard_options["gcc_simple_dict"], self.append_options["gcc_simple_dict"], self.option_dict.get_simple_dict())
        self.update_single_category_options(self.discard_options["gcc_space_dict"], self.append_options["gcc_space_dict"], self.option_dict.get_space_dict())
        self.update_single_category_options(self.discard_options["gcc_equal_dict"], self.append_options["gcc_equal_dict"], self.option_dict.get_equal_dict())
        self.update_single_category_options(self.discard_options["gcc_match_dict"], self.append_options["gcc_match_dict"], self.option_dict.get_match_dict())
        
    def update_single_category_options(self, discard_options=[], update_options={}, option_dict={}):
        # for option in discard_options:
        #     option_dict.pop(option)
        
        option_dict.update(update_options)
    
    def get_simple_dict(self):
        return self.gcc_simple_dict

    def get_space_dict(self):
        return self.gcc_space_dict
    
    def get_equal_dict(self):
        return self.gcc_equal_dict
    
    def get_match_dict(self):
        return self.gcc_match_dict