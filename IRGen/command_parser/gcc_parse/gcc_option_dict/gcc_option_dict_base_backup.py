# -*- coding:utf-8 -*-

gcc_option_index = 100


class GCCOptionCategory(object):
    OVERALL = 0 + gcc_option_index
    C_LANGUAGE = 1 + gcc_option_index
    CPP_LANGUAGE = 2 + gcc_option_index
    OBJECTIVE_C = 3 + gcc_option_index
    LANGUAGE_INDEPENDENT = 4 + gcc_option_index
    WARNING = 5 + gcc_option_index
    DEBUGGING = 6 + gcc_option_index
    OPTIMIZATION = 7 + gcc_option_index
    PREPROCESSOR = 8 + gcc_option_index
    ASSEMBLER = 9 + gcc_option_index
    LINKER = 10 + gcc_option_index
    DIRECTORY = 11 + gcc_option_index
    MACHINE_DEPENDENT = 12 + gcc_option_index
    ARM = 13 + gcc_option_index
    X86 = 14 + gcc_option_index
    CODE_GENERATION = 15 + gcc_option_index
    UNKNOWN_CATEGORY = 16 + gcc_option_index
    SOURCE_FILE = 17 + gcc_option_index
    UNKNOWN_OPTION = 18 + gcc_option_index


class GCCOPtionDictBase(object):

    gcc_simple_dict = {
        "-": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "-###": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--all-warnings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "--ansi": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "--assemble": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--comments": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--comments-in-macros": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--compile": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--coverage": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "--debug": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "--dependencies": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--extra-warnings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "--help": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--include-barrier": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "--no-canonical-prefixes": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--no-integrated-cpp": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--no-line-commands": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--no-standard-includes": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--no-standard-libraries": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "--no-sysroot-suffix": {
            "action": "filter",
            "category": GCCOptionCategory.DIRECTORY,
            "support": False
        },
        "--no-warnings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "--optimize": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "--pass-exit-codes": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "--pedantic": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--pedantic-errors": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "--pie": {
            "action": "filter",
            "category": GCCOptionCategory.LINKER,
            "support": False
        },
        "--pipe": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--preprocess": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--print-libgcc-file-name": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "--print-missing-file-dependencies": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--print-multi-directory": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "--print-multi-lib": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "--print-multi-os-directory": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "--print-multiarch": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "--print-search-dirs": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "--print-sysroot": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "--print-sysroot-headers-suffix": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "--profile": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "--save-temps": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "--shared": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "--static": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "--symbolic": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "--target-help": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--time": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "--trace-includes": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--traditional": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "--traditional-cpp": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "--trigraphs": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "--user-dependencies": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--verbose": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--version": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "-C": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "-CC": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-E": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "-H": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-M": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-MD": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-MG": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-MM": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-MMD": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-MP": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-N": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-O": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-Ofast": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-Og": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-Os": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-P": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-Q": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-Qn": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-Qy": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-S": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "-W": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wa,": {
            "action": "keep",
            "category": GCCOptionCategory.ASSEMBLER,
            "support": True
        },
        "-Wabi": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wabi-tag": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Waddress": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Waggregate-return": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Waggressive-loop-optimizations": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wall": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Warray-bounds": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wassign-intercept": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-Wattributes": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wbad-function-cast": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wbool-compare": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wbuiltin-macro-redefined": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wc++-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wc++0x-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wc++11-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wc++14-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wc90-c99-compat": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wc99-c11-compat": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wcast-align": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wcast-qual": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wchar-subscripts": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wchkp": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wclobbered": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wcomment": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wcomments": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-Wconditionally-supported": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wconversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wconversion-null": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wcoverage-mismatch": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wcpp": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wctor-dtor-privacy": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wdate-time": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wdeclaration-after-statement": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wdelete-incomplete": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wdelete-non-virtual-dtor": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wdeprecated": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wdeprecated-declarations": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wdesignated-init": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wdisabled-optimization": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wdiscarded-array-qualifiers": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wdiscarded-qualifiers": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wdiv-by-zero": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wdouble-promotion": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Weffc++": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wempty-body": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wendif-labels": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wenum-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Werror": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Werror-implicit-function-declaration": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wextra": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wfatal-errors": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wfloat-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wfloat-equal": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wformat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wformat=0": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wformat=2": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wformat-contains-nul": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wformat-extra-args": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wformat-nonliteral": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wformat-security": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wformat-signedness": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wformat-y2k": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wformat-zero-length": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wfree-nonheap-object": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wignored-qualifiers": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wimplicit": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wimplicit-function-declaration": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wimplicit-int": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wimport": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wincompatible-pointer-types": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Winherited-variadic-ctor": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Winit-self": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Winline": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wint-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wint-to-pointer-cast": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Winvalid-memory-model": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Winvalid-offsetof": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Winvalid-pch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wjump-misses-init": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wl,": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-Wliteral-suffix": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wlogical-not-parentheses": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wlogical-op": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wlong-long": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wmain": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wmaybe-uninitialized": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wmemset-transposed-args": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wmissing-braces": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wmissing-declarations": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wmissing-field-initializers": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wmissing-format-attribute": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wmissing-include-dirs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wmissing-noreturn": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wmissing-parameter-type": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wmissing-prototypes": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wmudflap": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wmultichar": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wnarrowing": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wnested-externs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-#pragma-messages": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-#warnings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-CFString-literal": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-NSObject-attribute": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-abi": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-abi-tag": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wno-absolute-value": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-abstract-final-class": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-abstract-vbase-init": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-address": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-address-of-array-temporary": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-address-of-temporary": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-aggregate-return": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-aggressive-loop-optimizations": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-all": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-ambiguous-ellipsis": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-ambiguous-macro": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-ambiguous-member-template": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-analyzer-incompatible-plugin": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-anonymous-pack-parens": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-arc": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-arc-bridge-casts-disallowed-in-nonarc": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-arc-maybe-repeated-use-of-weak": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-arc-non-pod-memaccess": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-arc-performSelector-leaks": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-arc-repeated-use-of-weak": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-arc-retain-cycles": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-arc-unsafe-retained-assign": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-array-bounds": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-array-bounds-pointer-arithmetic": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-asm": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-asm-operand-widths": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-assign-enum": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-assign-intercept": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-Wno-assume": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-at-protocol": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-atomic-memory-ordering": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-atomic-properties": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-atomic-property-with-user-defined-accessor": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-attributes": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-auto-import": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-auto-storage-class": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-auto-var-id": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-availability": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-backend-plugin": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-backslash-newline-escape": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-bad-array-new-length": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-bad-function-cast": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-bind-to-temporary-copy": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-bitfield-constant-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-bitwise-op-parentheses": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-bool-compare": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-bool-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-bool-conversions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-bridge-cast": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-builtin-macro-redefined": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-builtin-memcpy-chk-size": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-builtin-requires-header": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++0x-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++0x-extensions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++0x-narrowing": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++11-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++11-compat-deprecated-writable-strings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++11-compat-pedantic": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++11-compat-reserved-user-defined-literal": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++11-extensions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++11-extra-semi": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++11-long-long": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++11-narrowing": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++14-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++14-compat-pedantic": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++14-extensions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++1y-extensions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++1z-extensions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++98-c++11-c++14-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++98-c++11-c++14-compat-pedantic": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++98-c++11-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++98-c++11-compat-pedantic": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++98-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++98-compat-bind-to-temporary-copy": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++98-compat-local-type-template-args": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++98-compat-pedantic": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c++98-compat-unnamed-type-template-args": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c11-extensions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c90-c99-compat": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-c99-c11-compat": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-c99-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-c99-extensions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-cast-align": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-cast-of-sel-type": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-cast-qual": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-char-align": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-char-subscripts": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-class-varargs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-clobbered": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-comment": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-comments": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-compare-distinct-pointer-types": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-complex-component-init": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-conditional-type-mismatch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-conditional-uninitialized": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-conditionally-supported": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-config-macros": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-constant-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-constant-logical-operand": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-constexpr-not-const": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-consumed": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-conversion-null": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wno-coverage-mismatch": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-covered-switch-default": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-cpp": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-cstring-format-directive": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-ctor-dtor-privacy": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wno-cuda-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-custom-atomic-properties": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-dangling-else": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-dangling-field": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-dangling-initializer-list": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-date-time": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-dealloc-in-category": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-debug-compression-unavailable": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-declaration-after-statement": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-delegating-ctor-cycles": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-delete-incomplete": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-delete-non-virtual-dtor": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wno-deprecated": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-deprecated-declarations": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-deprecated-implementations": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-deprecated-increment-bool": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-deprecated-objc-isa-usage": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-deprecated-objc-pointer-introspection": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-deprecated-objc-pointer-introspection-performSelector": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-deprecated-register": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-deprecated-writable-strings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-designated-init": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-direct-ivar-access": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-disabled-macro-expansion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-disabled-optimization": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-discard-qual": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-discarded-array-qualifiers": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-discarded-qualifiers": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-distributed-object-modifiers": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-div-by-zero": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-division-by-zero": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-dll-attribute-on-redeclaration": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-dllimport-static-field-def": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-documentation": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-documentation-deprecated-sync": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-documentation-html": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-documentation-pedantic": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-documentation-unknown-command": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-dollar-in-identifier-extension": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-double-promotion": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-duplicate-decl-specifier": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-duplicate-enum": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-duplicate-method-arg": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-duplicate-method-match": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-dynamic-class-memaccess": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-effc++": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wno-embedded-directive": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-empty-body": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-empty-translation-unit": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-encode-type": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-endif-labels": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-enum-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-enum-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-enum-too-large": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-error": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-exit-time-destructors": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-explicit-initialize-call": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-explicit-ownership-type": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-extended-offsetof": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-extern-c-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-extern-initializer": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-extra": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-extra-qualification": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-extra-semi": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-extra-tokens": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-fallback": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-fatal-errors": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-flexible-array-extensions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-float-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-float-equal": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-format": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-format=2": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-format-contains-nul": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-format-extra-args": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-format-invalid-specifier": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-format-non-iso": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-format-nonliteral": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-format-pedantic": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-format-security": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-format-signedness": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-format-y2k": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-format-zero-length": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-four-char-constants": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-free-nonheap-object": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-function-def-in-objc-container": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-future-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gcc-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-global-constructors": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-alignof-expression": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-anonymous-struct": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-array-member-paren-init": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-binary-literal": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-case-range": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-complex-integer": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-compound-literal-initializer": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-conditional-omitted-operand": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-designator": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-empty-initializer": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-empty-struct": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-flexible-array-initializer": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-flexible-array-union-member": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-folding-constant": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-imaginary-constant": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-label-as-value": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-redeclared-enum": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-statement-expression": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-static-float-init": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-string-literal-operator-template": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-union-cast": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-variable-sized-type-not-at-end": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-zero-line-directive": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-gnu-zero-variadic-macro-arguments": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-header-guard": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-header-hygiene": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-idiomatic-parentheses": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-ignored-attributes": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-ignored-optimization-argument": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-ignored-pragmas": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-ignored-qualifiers": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-implicit": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-implicit-atomic-properties": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-implicit-conversion-floating-point-to-bool": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-implicit-exception-spec-mismatch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-implicit-fallthrough": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-implicit-fallthrough-per-function": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-implicit-function-declaration": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-implicit-int": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-implicit-retain-self": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-implicitly-unsigned-literal": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-import": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-import-preprocessor-directive-pedantic": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-incompatible-library-redeclaration": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-incompatible-ms-struct": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-incompatible-pointer-types": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-incompatible-pointer-types-discards-qualifiers": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-incompatible-property-type": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-incomplete-implementation": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-incomplete-module": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-incomplete-umbrella": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-inconsistent-dllimport": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-inconsistent-missing-override": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-infinite-recursion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-inherited-variadic-ctor": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-init-self": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-initializer-overrides": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-inline": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-inline-asm": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-inline-new-delete": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-int-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-int-conversions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-int-to-pointer-cast": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-int-to-void-pointer-cast": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-integer-overflow": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-invalid-command-line-argument": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-invalid-constexpr": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-invalid-iboutlet": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-invalid-initializer-from-system-header": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-invalid-noreturn": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-invalid-offsetof": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-invalid-pch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-invalid-pp-token": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-invalid-source-encoding": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-invalid-token-paste": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-jump-misses-init": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-keyword-compat": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-keyword-macro": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-knr-promoted-parameter": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-language-extension-token": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-large-by-value-copy": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-literal-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-literal-range": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-literal-suffix": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wno-local-type-template-args": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-logical-not-parentheses": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-logical-op": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-logical-op-parentheses": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-long-long": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-loop-analysis": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-macro-redefined": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-main": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-main-return-type": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-malformed-warning-check": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-maybe-uninitialized": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-memset-transposed-args": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-memsize-comparison": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-method-signatures": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-microsoft": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-microsoft-exists": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-mismatched-parameter-types": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-mismatched-return-types": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-mismatched-tags": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-braces": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-declarations": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-field-initializers": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-format-attribute": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-include-dirs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-method-return-type": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-noreturn": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-parameter-type": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-missing-prototype-for-cc": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-prototypes": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-selector-name": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-sysroot": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-missing-variable-declarations": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-module-build": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-module-conflict": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-most": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-msvc-include": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-multichar": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-multiple-move-vbase": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-narrowing": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wno-nested-anon-types": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-nested-externs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-new-returns-null": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-newline-eof": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-noexcept": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wno-non-gcc": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-non-literal-null-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-non-modular-include-in-framework-module": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-non-modular-include-in-module": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-non-pod-varargs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-non-template-friend": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wno-non-virtual-dtor": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wno-nonnull": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-nonportable-cfstrings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-nonportable-vector-initialization": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-normalized": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-null-arithmetic": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-null-character": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-null-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-null-dereference": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-autosynthesis-property-ivar-name-match": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-cocoa-api": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-designated-initializers": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-forward-class-redefinition": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-interface-ivars": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-literal-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-literal-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-method-access": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-missing-property-synthesis": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-missing-super-calls": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-multiple-method-names": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-noncopy-retain-block-property": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-nonunified-exceptions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-property-implementation": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-property-implicit-mismatch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-property-matches-cocoa-ownership-rule": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-property-no-attribute": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-property-synthesis": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-protocol-method-implementation": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-protocol-property-synthesis": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-readonly-with-setter-property": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-redundant-api-use": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-redundant-literal-use": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-root-class": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-string-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-objc-string-concatenation": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-odr": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-old-style-cast": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wno-old-style-declaration": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-old-style-definition": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-openmp-clauses": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-openmp-loop-form": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-openmp-simd": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-out-of-line-declaration": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-over-aligned": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-overflow": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-overlength-strings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-overloaded-shift-op-parentheses": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-overloaded-virtual": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wno-override-init": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-overriding-method-mismatch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-overriding-t-option": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-packed": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-packed-bitfield-compat": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-padded": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-parentheses": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-parentheses-equality": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-pass": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-pass-analysis": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-pass-failed": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-pass-missed": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-pedantic": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-pedantic-ms-format": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-pmf-conversions": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wno-pointer-arith": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-pointer-bool-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-pointer-sign": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-pointer-to-int-cast": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-pointer-type-mismatch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-potentially-evaluated-expression": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-pragmas": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-predefined-identifier-outside-function": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-private-extern": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-profile-instr-out-of-date": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-profile-instr-unprofiled": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-property-access-dot-syntax": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-property-attribute-mismatch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-protocol": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-Wno-protocol-property-synthesis-ambiguity": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-psabi": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-readonly-iboutlet-property": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-receiver-expr": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-receiver-forward-class": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-receiver-is-weak": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-redeclared-class-member": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-redundant-decls": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-reinterpret-base-class": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-remark-backend-plugin": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-reorder": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wno-requires-super-attribute": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-reserved-id-macro": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-reserved-user-defined-literal": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-retained-language-linkage": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-return-local-addr": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-return-stack-address": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-return-type": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-return-type-c-linkage": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-sanitize-address": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-section": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-selector": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-Wno-selector-type-mismatch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-self-assign": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-self-assign-field": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-self-move": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-semicolon-before-method-body": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-sentinel": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-sequence-point": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-serialized-diagnostics": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-shadow": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-shadow-ivar": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-shift-count-negative": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-shift-count-overflow": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-shift-op-parentheses": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-shift-overflow": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-shift-sign-overflow": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-shorten-64-to-32": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-sign-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-sign-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-sign-promo": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wno-sizeof-array-argument": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-sizeof-array-decay": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-sizeof-pointer-memaccess": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-sometimes-uninitialized": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-source-uses-openmp": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-stack-protector": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-static-float-init": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-static-in-inline": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-static-inline-explicit-instantiation": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-static-local-in-inline": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-static-self-init": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-strict-aliasing": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-strict-null-sentinel": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wno-strict-overflow": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-strict-prototypes": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-strict-selector-match": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-Wno-string-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-string-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-string-plus-char": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-string-plus-int": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-strlcpy-strlcat-size": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-strncat-size": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-suggest-final-methods": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-suggest-final-types": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-suggest-override": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-super-class-method-mismatch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-switch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-switch-bool": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-switch-default": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-switch-enum": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-sync-fetch-and-nand-semantics-changed": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-sync-nand": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-synth": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-system-headers": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-tautological-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-tautological-constant-out-of-range-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-tautological-overlap-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-tautological-pointer-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-tautological-undefined-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-tentative-definition-incomplete-type": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-thread-safety": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-thread-safety-analysis": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-thread-safety-attributes": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-thread-safety-beta": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-thread-safety-negative": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-thread-safety-precise": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-thread-safety-reference": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-thread-safety-verbose": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-traditional": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-traditional-conversion": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-trampolines": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-trigraphs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-type-limits": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-type-safety": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-typedef-redefinition": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-typename-missing": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unavailable-declarations": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-undeclared-selector": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-Wno-undef": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-undefined-bool-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-undefined-inline": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-undefined-internal": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-undefined-reinterpret-cast": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unevaluated-expression": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unicode": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unicode-whitespace": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-uninitialized": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unknown-attributes": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unknown-escape-sequence": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unknown-pragmas": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unknown-warning": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-unknown-warning-option": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unnamed-type-template-args": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unneeded-internal-declaration": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unneeded-member-function": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unreachable-code": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unreachable-code-aggressive": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unreachable-code-break": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unreachable-code-loop-increment": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unreachable-code-return": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unsafe-loop-optimizations": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-unsequenced": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unsuffixed-float-constants": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-unsupported-dll-base-class-template": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unsupported-friend": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unsupported-visibility": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-argument": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-but-set-parameter": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-unused-but-set-variable": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-unused-command-line-argument": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-comparison": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-const-variable": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-exception-parameter": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-function": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-getter-return-value": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-label": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-local-typedef": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-local-typedefs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-macros": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-member-function": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-parameter": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-private-field": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-property-ivar": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-result": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-value": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-variable": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-unused-volatile-lvalue": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-used-but-marked-unused": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-useless-cast": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-user-defined-literals": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-varargs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-variadic-macros": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-vector-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-vector-conversions": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-vector-operation-performance": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-vexing-parse": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-virtual-move-assign": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-visibility": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-vla": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-vla-extension": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-void-ptr-dereference": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-volatile-register-var": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-weak-template-vtables": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-weak-vtables": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-writable-strings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-write-strings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-zero-as-null-pointer-constant": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-zero-length-array": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wnoexcept": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wnon-template-friend": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wnon-virtual-dtor": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wnonnull": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wnormalized": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wodr": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wold-style-cast": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wold-style-declaration": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wold-style-definition": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wopenmp-simd": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Woverflow": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Woverlength-strings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Woverloaded-virtual": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Woverride-init": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wp,": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-Wpacked": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wpacked-bitfield-compat": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wpadded": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wparentheses": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wpedantic": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wpedantic-ms-format": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wpmf-conversions": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wpointer-arith": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wpointer-sign": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wpointer-to-int-cast": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wpragmas": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wproperty-assign-default": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wprotocol": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-Wpsabi": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wredundant-decls": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wreorder": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wreturn-local-addr": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wreturn-type": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wselector": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-Wsequence-point": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wshadow": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wshadow-ivar": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wshift-count-negative": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wshift-count-overflow": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wsign-compare": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wsign-conversion": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wsign-promo": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-Wsized-deallocation": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wsizeof-array-argument": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wsizeof-pointer-memaccess": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wstack-protector": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wstrict-aliasing": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wstrict-null-sentinel": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wstrict-overflow": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wstrict-prototypes": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wstrict-selector-match": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-Wsuggest-final-methods": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wsuggest-final-types": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wsuggest-override": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wswitch": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wswitch-bool": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wswitch-default": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wswitch-enum": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wsync-nand": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wsynth": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wsystem-headers": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wtraditional": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wtraditional-conversion": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wtrampolines": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wtrigraphs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wtype-limits": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wundeclared-selector": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-Wundef": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wuninitialized": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wunknown-pragmas": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wunreachable-code": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wunsafe-loop-optimizations": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wunsuffixed-float-constants": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wunused": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wunused-argument": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wunused-but-set-parameter": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wunused-but-set-variable": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wunused-function": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wunused-label": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wunused-local-typedefs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wunused-macros": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-Wunused-parameter": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wunused-result": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wunused-value": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wunused-variable": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wuseless-cast": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wvarargs": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wvariadic-macros": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wvector-operation-performance": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wvirtual-move-assign": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wvla": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wvolatile-register-var": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wwrite-strings": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wzero-as-null-pointer-constant": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Z": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-ansi": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-c": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "-coverage": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-dA": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-dD": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-dH": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-dI": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-dM": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-dN": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-dP": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-dU": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-da": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-dp": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-dumpmachine": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-dumpspecs": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-dumpversion": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-dx": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-export-dynamic": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-fPIC": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fPIE": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-faccess-control": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-faggressive-loop-optimizations": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-falign-functions": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-falign-jumps": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-falign-labels": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-falign-loops": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fall-virtual": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fallow-parameterless-variadic-functions": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-falt-external-templates": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fargument-alias": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fargument-noalias": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fargument-noalias-anything": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fargument-noalias-global": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fasm": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fassociative-math": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fasynchronous-unwind-tables": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fauto-inc-dec": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fauto-profile": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fbounds-check": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fbranch-count-reg": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fbranch-probabilities": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fbranch-target-load-optimize": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fbranch-target-load-optimize2": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fbtr-bb-exclusive": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fbuilding-libgcc": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fbuiltin": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fcall-saved-reg": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fcall-used-reg": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fcaller-saves": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fcanonical-system-headers": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fcheck-data-deps": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fcheck-new": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fcheck-pointer-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-check-incomplete-type": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-check-read": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-check-write": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-first-field-has-own-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-instrument-calls": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-instrument-marked-only": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-narrow-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-narrow-to-innermost-array": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-optimize": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-store-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-treat-zero-dynamic-size-as-infinite": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-use-fast-string-functions": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-use-nochk-string-functions": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-use-static-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-use-static-const-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-use-wrappers": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fchkp-zero-input-bounds-for-main": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fcilkplus": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fcombine-stack-adjustments": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fcommon": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fcompare-debug": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fcompare-debug-second": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fcompare-elim": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fcond-mismatch": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fconserve-space": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fconserve-stack": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fcprop-registers": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fcrossjumping": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fcse-follow-jumps": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fcse-skip-blocks": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fcx-fortran-rules": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fcx-limited-range": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fdata-sections": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fdbg-cnt-list": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fdce": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fdebug-cpp": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "-fdebug-types-section": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fdeclone-ctor-dtor": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fdeduce-init-list": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fdefault-inline": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-fdefer-pop": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fdelayed-branch": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fdelete-dead-exceptions": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fdelete-null-pointer-checks": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fdevirtualize": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fdevirtualize-at-ltrans": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fdevirtualize-speculatively": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fdiagnostics-color": {
            "action": "keep",
            "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
            "support": True
        },
        "-fdiagnostics-show-caret": {
            "action": "filter",
            "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
            "support": False
        },
        "-fdiagnostics-show-option": {
            "action": "keep",
            "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
            "support": True
        },
        "-fdirectives-only": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "-fdollars-in-identifiers": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-fdse": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fdump-ada-spec": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-fdump-ada-spec-slim": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-fdump-final-insns": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fdump-noaddr": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fdump-passes": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fdump-unnumbered": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fdump-unnumbered-links": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fdwarf2-cfi-asm": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fearly-inlining": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-felide-constructors": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-feliminate-dwarf2-dups": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-feliminate-unused-debug-symbols": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-feliminate-unused-debug-types": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-femit-class-debug-always": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-femit-struct-debug-baseonly": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-femit-struct-debug-reduced": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fenable-kind-pass": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fenforce-eh-specs": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fenum-int-equiv": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fexceptions": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fexpensive-optimizations": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fext-numeric-literals": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fextended-identifiers": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-fextern-tls-init": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fexternal-templates": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-ffast-math": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-ffat-lto-objects": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-ffinite-math-only": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-ffixed-reg": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-ffloat-store": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-ffor-scope": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fforce-addr": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-fforward-propagate": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ffreestanding": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-ffriend-injection": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-ffunction-cse": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ffunction-sections": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fgcse": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fgcse-after-reload": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fgcse-las": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fgcse-lm": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fgcse-sm": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fgnu-keywords": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fgnu-runtime": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-fgnu-tm": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fgnu-unique": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fgnu89-inline": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fgraphite": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fgraphite-identity": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fguess-branch-probability": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fguiding-decls": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fhandle-exceptions": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fhelp": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-fhoist-adjacent-loads": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fhonor-std": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fhosted": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fhuge-objects": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fident": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fif-conversion": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fif-conversion2": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fimplement-inlines": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fimplicit-inline-templates": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fimplicit-templates": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-findirect-inlining": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-finhibit-size-directive": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-finline": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-finline-atomics": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-finline-functions": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-finline-functions-called-once": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-finline-small-functions": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-finstrument-functions": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fipa-cp": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fipa-cp-alignment": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fipa-cp-clone": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fipa-icf": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fipa-icf-functions": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fipa-icf-variables": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fipa-matrix-reorg": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fipa-profile": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fipa-pta": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fipa-pure-const": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fipa-ra": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fipa-reference": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fipa-sra": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fipa-struct-reorg": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fira-hoist-pressure": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fira-loop-pressure": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fira-share-save-slots": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fira-share-spill-slots": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fisolate-erroneous-paths-attribute": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fisolate-erroneous-paths-dereference": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fivopts": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fjump-tables": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fkeep-inline-dllexport": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fkeep-inline-functions": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fkeep-static-consts": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-flabels-ok": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-flax-vector-conversions": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fleading-underscore": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-flifetime-dse": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-flive-range-shrinkage": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-flocal-ivars": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-floop-block": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-floop-flatten": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-floop-interchange": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-floop-nest-optimize": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-floop-optimize": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-floop-parallelize-all": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-floop-strip-mine": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-floop-unroll-and-jam": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-flra-remat": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-flto": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-flto-odr-type-merging": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-flto-report": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-flto-report-wpa": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fmath-errno": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fmem-report": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fmem-report-wpa": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fmerge-all-constants": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fmerge-constants": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fmerge-debug-strings": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fmodulo-sched": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fmodulo-sched-allow-regmoves": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fmove-loop-invariants": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fms-extensions": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fmudflap": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-fmudflapir": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fmudflapth": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-fnew-abi": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fnext-runtime": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-fnil-receivers": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fno-PIC": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-PIE": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-access-control": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-aggressive-loop-optimizations": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-align-functions": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-align-jumps": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-align-labels": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-align-loops": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-allow-parameterless-variadic-functions": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fno-asm": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fno-associative-math": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-asynchronous-unwind-tables": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-auto-inc-dec": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-auto-profile": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-bounds-check": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-branch-count-reg": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-branch-probabilities": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-branch-target-load-optimize": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-branch-target-load-optimize2": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-btr-bb-exclusive": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-builtin": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fno-builtin-function": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fno-caller-saves": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-canonical-system-headers": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "-fno-check-data-deps": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-check-new": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-check-pointer-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-check-incomplete-type": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-check-read": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-check-write": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-instrument-calls": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-instrument-marked-only": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-narrow-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-optimize": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-store-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-treat-zero-dynamic-size-as-infinite": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-use-fast-string-functions": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-use-nochk-string-functions": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-use-static-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-use-static-const-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-chkp-use-wrappers": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-combine-stack-adjustments": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-common": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-compare-debug": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-compare-elim": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-cond-mismatch": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fno-conserve-stack": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-cprop-registers": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-crossjumping": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-cse-follow-jumps": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-cse-skip-blocks": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-cx-fortran-rules": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-cx-limited-range": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-data-sections": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-dbg-cnt-list": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-dce": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-debug-cpp": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "-fno-debug-types-section": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fno-defer-pop": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-delayed-branch": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-delete-dead-exceptions": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-delete-null-pointer-checks": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-devirtualize": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-devirtualize-at-ltrans": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-devirtualize-speculatively": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-diagnostics-color": {
            "action": "keep",
            "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
            "support": True
        },
        "-fno-diagnostics-show-caret": {
            "action": "filter",
            "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
            "support": False
        },
        "-fno-diagnostics-show-option": {
            "action": "keep",
            "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
            "support": True
        },
        "-fno-dse": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-dwarf2-cfi-asm": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fno-early-inlining": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-elide-constructors": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-eliminate-dwarf2-dups": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-eliminate-unused-debug-symbols": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fno-eliminate-unused-debug-types": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fno-emit-class-debug-always": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-emit-struct-debug-baseonly": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-emit-struct-debug-reduced": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-enforce-eh-specs": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-exceptions": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-expensive-optimizations": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-ext-numeric-literals": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-extern-tls-init": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-fast-math": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-fat-lto-objects": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-finite-math-only": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-float-store": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-for-scope": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-forward-propagate": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-freestanding": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fno-friend-injection": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-function-cse": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-function-sections": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-gcse": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-gcse-after-reload": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-gcse-las": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-gcse-lm": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-gcse-sm": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-gnu-keywords": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-gnu-unique": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-gnu89-inline": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fno-graphite-identity": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-guess-branch-probability": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-hoist-adjacent-loads": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-hosted": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fno-ident": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-if-conversion": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-if-conversion2": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-implement-inlines": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-implicit-inline-templates": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-implicit-templates": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-indirect-inlining": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-inhibit-size-directive": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-inline": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-inline-functions": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-inline-functions-called-once": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-inline-small-functions": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-instrument-functions": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-ipa-cp": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-ipa-cp-alignment": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ipa-cp-clone": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ipa-icf": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ipa-profile": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ipa-pta": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ipa-pure-const": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ipa-ra": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ipa-reference": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ipa-sra": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ira-hoist-pressure": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ira-loop-pressure": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ira-share-save-slots": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ira-share-spill-slots": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-isolate-erroneous-paths-attribute": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-isolate-erroneous-paths-dereference": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ivopts": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-jump-tables": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-keep-inline-functions": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-keep-static-consts": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-lax-vector-conversions": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fno-leading-underscore": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-lifetime-dse": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-live-range-shrinkage": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-local-ivars": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fno-loop-block": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-loop-interchange": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-loop-nest-optimize": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-loop-parallelize-all": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-loop-strip-mine": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-loop-unroll-and-jam": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-lra-remat": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-lto": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-lto-report": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-lto-report-wpa": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-math-errno": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-mem-report": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-mem-report-wpa": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-merge-all-constants": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-merge-constants": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-merge-debug-strings": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-modulo-sched": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-modulo-sched-allow-regmoves": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-move-loop-invariants": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ms-extensions": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fno-nil-receivers": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fno-non-call-exceptions": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-nonansi-builtins": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-nothrow-opt": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-objc-call-cxx-cdtors": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fno-objc-direct-dispatch": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fno-objc-exceptions": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-fno-objc-gc": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fno-objc-nilcheck": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fno-omit-frame-pointer": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-openacc": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fno-openmp": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fno-openmp-simd": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fno-operator-names": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-opt-info": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-optimize-sibling-calls": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-optional-diags": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-pack-struct": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-partial-inlining": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-pcc-struct-return": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-peel-loops": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-peephole": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-peephole2": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-permissive": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-pic": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-pie": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-plan9-extensions": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fno-post-ipa-mem-report": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-pre-ipa-mem-report": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-predictive-commoning": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-prefetch-loop-arrays": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-pretty-templates": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-profile-arcs": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fno-profile-correction": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-profile-generate": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-profile-reorder-functions": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-profile-report": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-profile-use": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-profile-values": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-reciprocal-math": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-record-gcc-switches": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-ree": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-reg-struct-return": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-rename-registers": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-reorder-blocks": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-reorder-blocks-and-partition": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-reorder-functions": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-replace-objc-classes": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fno-repo": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-rerun-cse-after-loop": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-reschedule-modulo-scheduled-loops": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-rounding-math": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-rtti": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-sanitize-recover": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fno-sanitize-undefined-trap-on-error": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fno-sched-critical-path-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-dep-count-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-group-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-interblock": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-last-insn-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-pressure": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-rank-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-spec": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-spec-insn-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-spec-load": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-spec-load-dangerous": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-stalled-insns": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched-stalled-insns-dep": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sched2-use-superblocks": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-schedule-fusion": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-schedule-insns": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-schedule-insns2": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-section-anchors": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sel-sched-pipelining": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-sel-sched-pipelining-outer-loops": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-selective-scheduling": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-selective-scheduling2": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-semantic-interposition": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-short-double": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-short-enums": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-short-wchar": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-show-column": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-fno-shrink-wrap": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-signaling-nans": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-signed-bitfields": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fno-signed-char": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fno-signed-zeros": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-single-precision-constant": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-sized-deallocation": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-split-ivs-in-unroller": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-split-stack": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-split-wide-types": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-ssa-phiopt": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-stack-check": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-stack-limit": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-stack-protector": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-stats": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-stdarg-opt": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-strict-aliasing": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-strict-overflow": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-strict-volatile-bitfields": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-sync-libcalls": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-syntax-only": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-fno-test-coverage": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-thread-jumps": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-threadsafe-statics": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-time-report": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-toplevel-reorder": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tracer": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-trapping-math": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-trapv": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fno-tree-bit-ccp": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-builtin-call-dce": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-ccp": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-ch": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-coalesce-vars": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-copy-prop": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-copyrename": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-dce": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-tree-dominator-opts": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-dse": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-forwprop": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-fre": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-loop-distribute-patterns": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-loop-distribution": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-loop-if-convert": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-loop-if-convert-stores": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-loop-im": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-loop-ivcanon": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-loop-linear": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-loop-optimize": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-loop-vectorize": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-partial-pre": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-phiprop": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-pre": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-pta": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-reassoc": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-sink": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-slsr": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-sra": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-switch-conversion": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-tail-merge": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-tree-ter": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-tree-vectorize": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-tree-vrp": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-unit-at-a-time": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-unroll-all-loops": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-unroll-loops": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-unsafe-loop-optimizations": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-unsafe-math-optimizations": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-unsigned-bitfields": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fno-unsigned-char": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fno-unswitch-loops": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-unwind-tables": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-use-cxa-atexit": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fno-use-cxa-get-exception-ptr": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-use-linker-plugin": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-var-tracking": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fno-var-tracking-assignments": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-var-tracking-assignments-toggle": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-variable-expansion-in-unroller": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-vect-cost-model": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-verbose-asm": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-visibility-inlines-hidden": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-visibility-ms-compat": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-vpt": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-vtv-counts": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-vtv-debug": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-weak": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fno-web": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-whole-program": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-working-directory": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-fno-wpa": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fno-wrapv": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fno-zero-initialized-in-bss": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fno-zero-link": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fnon-call-exceptions": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fnonansi-builtins": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fnonnull-objects": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fnothrow-opt": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fobjc-call-cxx-cdtors": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-fobjc-direct-dispatch": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fobjc-exceptions": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-fobjc-gc": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-fobjc-nilcheck": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fobjc-sjlj-exceptions": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fomit-frame-pointer": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fopenacc": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fopenmp": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fopenmp-simd": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-foperator-names": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fopt-info": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-foptimize-register-move": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-foptimize-sibling-calls": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-foptimize-strlen": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-foptional-diags": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fpack-struct": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fpartial-inlining": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fpcc-struct-return": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fpch-deps": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "-fpch-preprocess": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-fpeel-loops": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fpeephole": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fpeephole2": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fpermissive": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fpic": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fpie": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fplan9-extensions": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-fpost-ipa-mem-report": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fpre-ipa-mem-report": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fpredictive-commoning": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fprefetch-loop-arrays": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fpreprocessed": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "-fpretty-templates": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fprofile": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-fprofile-arcs": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fprofile-correction": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fprofile-generate": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fprofile-reorder-functions": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fprofile-report": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fprofile-use": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fprofile-values": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-freciprocal-math": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-frecord-gcc-switches": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-free": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-freg-struct-return": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fregmove": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-frename-registers": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-freorder-blocks": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-freorder-blocks-and-partition": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-freorder-functions": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-freplace-objc-classes": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-frepo": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-freport-bug": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-frerun-cse-after-loop": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-frerun-loop-opt": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-freschedule-modulo-scheduled-loops": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-frounding-math": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-frtti": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fsanitize-recover": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fsanitize-undefined-trap-on-error": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fsched-critical-path-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-dep-count-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-group-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-interblock": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fsched-last-insn-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-pressure": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-rank-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-spec": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-spec-insn-heuristic": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-spec-load": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-spec-load-dangerous": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-stalled-insns": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-stalled-insns-dep": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched2-use-superblocks": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched2-use-traces": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fschedule-fusion": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fschedule-insns": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fschedule-insns2": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fsection-anchors": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsee": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-fsel-sched-pipelining": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsel-sched-pipelining-outer-loops": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsel-sched-reschedule-pipelined": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fselective-scheduling": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fselective-scheduling2": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsemantic-interposition": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fshort-double": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fshort-enums": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fshort-wchar": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fshow-column": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-fshrink-wrap": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsignaling-nans": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fsigned-bitfields": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fsigned-char": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fsigned-zeros": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fsingle-precision-constant": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fsized-deallocation": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fsplit-ivs-in-unroller": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsplit-stack": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fsplit-wide-types": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsquangle": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fssa-phiopt": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fstack-check": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fstack-protector": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fstack-protector-all": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fstack-protector-explicit": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fstack-protector-strong": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fstack-usage": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fstats": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fstdarg-opt": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fstrength-reduce": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-fstrict-aliasing": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fstrict-enums": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fstrict-overflow": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fstrict-prototype": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fstrict-volatile-bitfields": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fsync-libcalls": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fsyntax-only": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-ftarget-help": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-ftest-coverage": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fthis-is-variable": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fthread-jumps": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fthreadsafe-statics": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-ftime-report": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-ftoplevel-reorder": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftracer": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-ftrack-macro-expansion": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "-ftrack-macro-expansion=": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "-ftrapping-math": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-ftrapv": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-ftree-bit-ccp": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-builtin-call-dce": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-ccp": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-ch": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-coalesce-inlined-vars": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-coalesce-vars": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-copy-prop": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-copyrename": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-cselim": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-ftree-dce": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-ftree-dominator-opts": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-dse": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-forwprop": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-fre": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-loop-distribute-patterns": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-loop-distribution": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-loop-if-convert": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-loop-if-convert-stores": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-loop-im": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-loop-ivcanon": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-loop-linear": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-loop-optimize": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-loop-vectorize": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-lrs": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-ftree-partial-pre": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-phiprop": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-pre": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-pta": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-reassoc": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-salias": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-ftree-scev-cprop": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-ftree-sink": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-slp-vectorize": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-ftree-slsr": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-sra": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-store-ccp": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-ftree-store-copy-prop": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-ftree-switch-conversion": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-tail-merge": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-ter": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-ftree-vect-loop-version": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-ftree-vectorize": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-ftree-vrp": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-funit-at-a-time": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-funroll-all-loops": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-funroll-loops": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-funsafe-loop-optimizations": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-funsafe-math-optimizations": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-funsigned-bitfields": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-funsigned-char": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-funswitch-loops": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-funwind-tables": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fuse-cxa-atexit": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fuse-cxa-get-exception-ptr": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fuse-linker-plugin": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fvar-tracking": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fvar-tracking-assignments": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fvar-tracking-assignments-toggle": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fvar-tracking-uninit": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fvariable-expansion-in-unroller": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fvect-cost-model": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fverbose-asm": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fversion": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-fvisibility-inlines-hidden": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fvisibility-ms-compat": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fvpt": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fvtable-gc": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fvtable-thunks": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fvtv-counts": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fvtv-debug": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fweak": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fweb": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fwhole-program": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fworking-directory": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "-fwpa": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fwrapv": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fxref": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fzee": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fzero-initialized-in-bss": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fzero-link": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-g": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-g0": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-g1": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-g2": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-g3": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-gcoff": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-gdwarf": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-gdwarf-2": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-gdwarf-4": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-gdwarf-version": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-gen-decls": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-ggdb": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-ggnu-pubnames": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-gno-pubnames": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-gno-record-gcc-switches": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-gno-split-dwarf": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-gno-strict-dwarf": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-gpubnames": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-grecord-gcc-switches": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-gsplit-dwarf": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-gstabs": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-gstabs+": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-gstrict-dwarf": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-gtoggle": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-gvms": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-gxcoff": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-gxcoff+": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-gz": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-lang-asm": {
            "action": "keep",
            "category": GCCOptionCategory.ASSEMBLER,
            "support": True
        },
        "-lgcc": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-lgcov": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-lobjc": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-m128bit-long-double": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-m16": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-m32": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-m3dnow": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-m3dnowa": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-m64": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-m80387": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-m8bit-idiv": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-m96bit-long-double": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mabm": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mabort-on-noreturn": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-maccumulate-outgoing-args": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-madx": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-maes": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-malign-double": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-malign-stringops": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mapcs": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mapcs-float": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mapcs-frame": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mapcs-reentrant": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mapcs-stack-check": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-marm": {
            "action": "keep",
            "category": GCCOptionCategory.ARM,
            "support": True
        },
        "-masm-syntax-unified": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mavx": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mavx2": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mavx256-split-unaligned-load": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mavx256-split-unaligned-store": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mavx512bw": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mavx512cd": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mavx512dq": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mavx512er": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mavx512f": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mavx512ifma": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mavx512pf": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mavx512vbmi": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mavx512vl": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mbig-endian": {
            "action": "keep",
            "category": GCCOptionCategory.ARM,
            "support": True
        },
        "-mbmi": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mbmi2": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mcallee-super-interworking": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mcaller-super-interworking": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mcld": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mclflushopt": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mclwb": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mcrc32": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mcx16": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mdispatch-scheduler": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mdump-tune-features": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mf16c": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mfancy-math-387": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mfentry": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mfix-cortex-m3-ldrd": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mfma": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mfma4": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mforce-drap": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mfp-ret-in-387": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mfsgsbase": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mfxsr": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mhard-float": {
            "action": "keep",
            "category": GCCOptionCategory.MACHINE_DEPENDENT,
            "support": True
        },
        "-mhle": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mieee-fp": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-minline-all-stringops": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-minline-stringops-dynamically": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mintel-syntax": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mlittle-endian": {
            "action": "keep",
            "category": GCCOptionCategory.ARM,
            "support": True
        },
        "-mlong-calls": {
            "action": "keep",
            "category": GCCOptionCategory.ARM,
            "support": True
        },
        "-mlong-double-128": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mlong-double-64": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mlong-double-80": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mlwp": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mlzcnt": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mmmx": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mmovbe": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mmpx": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mms-bitfields": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mmwaitx": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mneon-for-64bits": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mnew-generic-costs": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mno-align-stringops": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mno-apcs-float": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mno-apcs-frame": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mno-apcs-reentrant": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mno-apcs-stack-check": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mno-default": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mno-fancy-math-387": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mno-fp-ret-in-387": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mno-long-calls": {
            "action": "keep",
            "category": GCCOptionCategory.ARM,
            "support": True
        },
        "-mno-push-args": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mno-red-zone": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mno-sched-prolog": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mno-single-pic-base": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mno-sse4": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mno-thumb-interwork": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mno-tls-direct-seg-refs": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mno-unaligned-access": {
            "action": "keep",
            "category": GCCOptionCategory.ARM,
            "support": True
        },
        "-mnop-mcount": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mold-rtx-costs": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-momit-leaf-frame-pointer": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mpc32": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mpc64": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mpc80": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mpclmul": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mpcommit": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mpic-data-is-text-relative": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mpoke-function-name": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mpopcnt": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mprefer-avx128": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mprefetchwt1": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mprfchw": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mprint-tune-info": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mpush-args": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mrdrnd": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mrdseed": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mrecip": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mrecord-mcount": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mred-zone": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mrestrict-it": {
            "action": "keep",
            "category": GCCOptionCategory.ARM,
            "support": True
        },
        "-mrtd": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mrtm": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-msahf": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-msched-prolog": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-msha": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-msingle-pic-base": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mskip-rax-setup": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mslow-flash-data": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-msoft-float": {
            "action": "keep",
            "category": GCCOptionCategory.MACHINE_DEPENDENT,
            "support": True
        },
        "-msse": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-msse2": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-msse2avx": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-msse3": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-msse4": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-msse4.1": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-msse4.2": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-msse4a": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-msse5": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-msseregparm": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mssse3": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mstack-arg-probe": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mstackrealign": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mtbm": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mthumb": {
            "action": "keep",
            "category": GCCOptionCategory.ARM,
            "support": True
        },
        "-mthumb-interwork": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mtls-direct-seg-refs": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mtpcs-frame": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mtpcs-leaf-frame": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-munaligned-access": {
            "action": "keep",
            "category": GCCOptionCategory.ARM,
            "support": True
        },
        "-mvect8-ret-in-mem": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mvectorize-with-neon-double": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mvectorize-with-neon-quad": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mvzeroupper": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mword-relocations": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mx32": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mxop": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mxsave": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mxsavec": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mxsaveopt": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mxsaves": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-n": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-no-canonical-prefixes": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-no-integrated-cpp": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-nodefaultlibs": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-nostartfiles": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-nostdinc": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-nostdinc++": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-nostdlib": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-p": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-pass-exit-codes": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-pedantic": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-pedantic-errors": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-pg": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-pie": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-pipe": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "-print-libgcc-file-name": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-print-multi-directory": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-print-multi-lib": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-print-multi-os-directory": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-print-multiarch": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-print-objc-runtime-info": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-print-search-dirs": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-print-sysroot": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-print-sysroot-headers-suffix": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-pthread": {
            "action": "keep",
            "category": GCCOptionCategory.MACHINE_DEPENDENT,
            "support": True
        },
        "-r": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-rdynamic": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-remap": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-s": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-save-temps": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-shared": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-shared-libgcc": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-static": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-static-libasan": {
            "action": "filter",
            "category": GCCOptionCategory.LINKER,
            "support": False
        },
        "-static-libgcc": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-static-libgfortran": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-static-libgo": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-static-liblsan": {
            "action": "filter",
            "category": GCCOptionCategory.LINKER,
            "support": False
        },
        "-static-libmpx": {
            "action": "filter",
            "category": GCCOptionCategory.LINKER,
            "support": False
        },
        "-static-libmpxwrappers": {
            "action": "filter",
            "category": GCCOptionCategory.LINKER,
            "support": False
        },
        "-static-libstdc++": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-static-libtsan": {
            "action": "filter",
            "category": GCCOptionCategory.LINKER,
            "support": False
        },
        "-static-libubsan": {
            "action": "filter",
            "category": GCCOptionCategory.LINKER,
            "support": False
        },
        "-symbolic": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-t": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-time": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-traditional": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-traditional-cpp": {
            "action": "filter",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": False
        },
        "-trigraphs": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-undef": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-v": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "-version": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-w": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
    }



    gcc_space_dict = {
        "--assert": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--define-macro": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--dump": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "--dumpbase": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "--dumpdir": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "--entry": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--for-assembler": {
            "action": "filter",
            "category": GCCOptionCategory.ASSEMBLER,
            "support": False
        },
        "--for-linker": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "--force-link": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "--imacros": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include-directory": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "--include-directory-after": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include-prefix": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include-with-prefix": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include-with-prefix-after": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include-with-prefix-before": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--language": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--library-directory": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "--output": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--param": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "--prefix": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "--print-file-name": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "--print-prog-name": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "--specs": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "--sysroot": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "--undefine-macro": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--write-dependencies": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "--write-user-dependencies": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-A": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-B": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "-D": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-F": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-I": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "-L": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "-MF": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-MQ": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-MT": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-T": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-Tbss": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-Tdata": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-Ttext": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-U": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-Xassembler": {
            "action": "keep",
            "category": GCCOptionCategory.ASSEMBLER,
            "support": True
        },
        "-Xlinker": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-Xpreprocessor": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-aux-info": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-auxbase": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-auxbase-strip": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-dumpbase": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-dumpdir": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-e": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-h": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-idirafter": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-imacros": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-imultiarch": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-imultilib": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-include": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-iprefix": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-iquote": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "-isysroot": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-isystem": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-iwithprefix": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-iwithprefixbefore": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-l": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-o": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "-specs": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "-u": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-wrapper": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-x": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "-z": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
    }



    gcc_equal_dict = {
        "--assert": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--define-macro": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--dump": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "--entry": {
            "action": "filter",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": False
        },
        "--for-assembler": {
            "action": "filter",
            "category": GCCOptionCategory.ASSEMBLER,
            "support": False
        },
        "--for-linker": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "--force-link": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "--help": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "--imacros": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include-directory": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "--include-directory-after": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include-prefix": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include-with-prefix": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include-with-prefix-after": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--include-with-prefix-before": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--language": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--library-directory": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "--output": {
            "action": "keep",
            "category": GCCOptionCategory.OVERALL,
            "support": True
        },
        "--output-pch": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "--param": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "--prefix": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "--print-file-name": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "--print-prog-name": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "--specs": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "--sysroot": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "--undefine-macro": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-B": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "-I": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "-L": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "-Tbss": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-Tdata": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-Ttext": {
            "action": "keep",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": True
        },
        "-Wabi": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Warray-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Werror": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wformat": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wframe-larger-than": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wlarger-than": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-abi": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-Wno-array-bounds": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-error": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-format": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-frame-larger-than": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wno-larger-than": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-normalized": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-stack-usage": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-strict-aliasing": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-strict-overflow": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wno-suggest-attribute": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wnormalized": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wstack-usage": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wstrict-aliasing": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-Wstrict-overflow": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-Wsuggest-attribute": {
            "action": "filter",
            "category": GCCOptionCategory.WARNING,
            "support": False
        },
        "-aux-info": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-fabi-compat-version": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fabi-version": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fada-spec-parent": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-falign-functions": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-falign-jumps": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-falign-labels": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-falign-loops": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fasan-shadow-offset": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fauto-profile": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fcompare-debug": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fconstant-string-class": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fconstexpr-depth": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-fdbg-cnt": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fdebug-prefix-map": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fdiagnostics-color": {
            "action": "filter",
            "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
            "support": False
        },
        "-fdiagnostics-show-location": {
            "action": "keep",
            "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
            "support": True
        },
        "-fdump-final-insns": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fdump-go-spec": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-femit-struct-debug-detailed": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fenable-kind-pass": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fexcess-precision": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fexec-charset": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-ffp-contract": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-fhelp": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-finline-limit": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-finput-charset": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-finstrument-functions-exclude-file-list": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-finstrument-functions-exclude-function-list": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fira-algorithm": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fira-region": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fira-verbose": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fivar-visibility": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-flto": {
            "action": "keep",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": True
        },
        "-flto-compression-level": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-flto-partition": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fmax-errors": {
            "action": "keep",
            "category": GCCOptionCategory.WARNING,
            "support": True
        },
        "-fmessage-length": {
            "action": "keep",
            "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
            "support": True
        },
        "-fno-constant-string-class": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-fno-emit-struct-debug-detailed": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fno-sanitize": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fno-sanitize-recover": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fobjc-abi-version": {
            "action": "keep",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": True
        },
        "-fobjc-std": {
            "action": "filter",
            "category": GCCOptionCategory.OBJECTIVE_C,
            "support": False
        },
        "-foffload": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-foffload-abi": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fpack-struct": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fplugin": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-fplugin-arg-name": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-fplugin-arg-name-key": {
            "action": "filter",
            "category": GCCOptionCategory.OVERALL,
            "support": False
        },
        "-fprofile-dir": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fprofile-generate": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fprofile-use": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-frandom-seed": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fsanitize": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fsanitize-recover": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-fsched-stalled-insns": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-stalled-insns-dep": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fsched-verbose": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-fstack-check": {
            "action": "filter",
            "category": GCCOptionCategory.SOURCE_FILE,
            "support": False
        },
        "-fstack-limit-register": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fstack-limit-symbol": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-fstack-reuse": {
            "action": "filter",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": False
        },
        "-ftabstop": {
            "action": "keep",
            "category": GCCOptionCategory.PREPROCESSOR,
            "support": True
        },
        "-ftemplate-backtrace-limit": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-ftemplate-depth": {
            "action": "keep",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": True
        },
        "-ftls-model": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-ftrack-macro-expansion": {
            "action": "filter",
            "category": GCCOptionCategory.LINKER,
            "support": False
        },
        "-ftree-parallelize-loops": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-ftree-vectorizer-verbose": {
            "action": "filter",
            "category": GCCOptionCategory.UNKNOWN_CATEGORY,
            "support": False
        },
        "-fuse-ld": {
            "action": "keep",
            "category": GCCOptionCategory.LINKER,
            "support": True
        },
        "-fvect-cost-model": {
            "action": "filter",
            "category": GCCOptionCategory.OPTIMIZATION,
            "support": False
        },
        "-fvisibility": {
            "action": "keep",
            "category": GCCOptionCategory.CODE_GENERATION,
            "support": True
        },
        "-fvtable-verify": {
            "action": "filter",
            "category": GCCOptionCategory.CPP_LANGUAGE,
            "support": False
        },
        "-fwide-exec-charset": {
            "action": "filter",
            "category": GCCOptionCategory.LINKER,
            "support": False
        },
        "-gz": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-iplugindir": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "-iquote": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "-mabi": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-maddress-mode": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-malign-data": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-malign-functions": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-malign-jumps": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-malign-loops": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-march": {
            "action": "keep",
            "category": GCCOptionCategory.MACHINE_DEPENDENT,
            "support": True
        },
        "-masm": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mbranch-cost": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mcmodel": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mcpu": {
            "action": "keep",
            "category": GCCOptionCategory.MACHINE_DEPENDENT,
            "support": True
        },
        "-mfloat-abi": {
            "action": "keep",
            "category": GCCOptionCategory.ARM,
            "support": True
        },
        "-mfp16-format": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mfpmath": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mfpu": {
            "action": "keep",
            "category": GCCOptionCategory.ARM,
            "support": True
        },
        "-mincoming-stack-boundary": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mlarge-data-threshold": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mmemcpy-strategy": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mmemset-strategy": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mpic-register": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mpreferred-stack-boundary": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mrecip": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mregparm": {
            "action": "keep",
            "category": GCCOptionCategory.X86,
            "support": True
        },
        "-mstack-protector-guard": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mstringop-strategy": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mstructure-size-boundary": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mtls-dialect": {
            "action": "filter",
            "category": GCCOptionCategory.MACHINE_DEPENDENT,
            "support": False
        },
        "-mtp": {
            "action": "filter",
            "category": GCCOptionCategory.ARM,
            "support": False
        },
        "-mtune": {
            "action": "keep",
            "category": GCCOptionCategory.MACHINE_DEPENDENT,
            "support": True
        },
        "-mtune-ctrl": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-mveclibabi": {
            "action": "filter",
            "category": GCCOptionCategory.X86,
            "support": False
        },
        "-print-file-name": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-print-prog-name": {
            "action": "keep",
            "category": GCCOptionCategory.DEBUGGING,
            "support": True
        },
        "-save-temps": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
        "-specs": {
            "action": "keep",
            "category": GCCOptionCategory.DIRECTORY,
            "support": True
        },
        "-std": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "--std": {
            "action": "keep",
            "category": GCCOptionCategory.C_LANGUAGE,
            "support": True
        },
        "-time": {
            "action": "filter",
            "category": GCCOptionCategory.DEBUGGING,
            "support": False
        },
    }



    gcc_match_dict = {
            "-z":
            {
                "action": "keep",
                "category": GCCOptionCategory.LINKER,
                "support": True
            },
            "-x":
            {
                "action": "keep",
                "category": GCCOptionCategory.OVERALL,
                "support": True
            },
            "-u":
            {
                "action": "keep",
                "category": GCCOptionCategory.LINKER,
                "support": True
            },
            "-o":
            {
                "action": "keep",
                "category": GCCOptionCategory.OVERALL,
                "support": True
            },
            "-l":
            {
                "action": "keep",
                "category": GCCOptionCategory.LINKER,
                "support": True
            },
            "-iwithprefixbefore":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-iwithprefix":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-isystem":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-isysroot":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-iquote":
            {
                "action": "keep",
                "category": GCCOptionCategory.DIRECTORY,
                "support": True
            },
            "-iprefix":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-include":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-imultilib":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-imultiarch":
            {
                "action": "keep",
                "category": GCCOptionCategory.UNKNOWN_CATEGORY,
                "support": True
            },
            "-imacros":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-idirafter":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-h":
            {
                "action": "filter",
                "category": GCCOptionCategory.UNKNOWN_CATEGORY,
                "support": False
            },
            "-gxcoff+":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-gxcoff":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-gvms":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-gstabs+":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-gstabs":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-ggdb":
            {
                "action": "keep",
                "category": GCCOptionCategory.DEBUGGING,
                "support": True
            },
            "-gdwarf-":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-gdwarf":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-gcoff":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-ftrack-macro-expansion":
            {
                "action": "filter",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": False
            },
            "-ftemplate-depth-":
            {
                "action": "keep",
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "support": True
            },
            "-fplugin-arg-":
            {
                "action": "filter",
                "category": GCCOptionCategory.OVERALL,
                "support": False
            },
            "-fopt-info-":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-fno-opt-info-":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-fno-dump-":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-fno-builtin-":
            {
                "action": "keep",
                "category": GCCOptionCategory.C_LANGUAGE,
                "support": True
            },
            "-fname-mangling-version-":
            {
                "action": "filter",
                "category": GCCOptionCategory.UNKNOWN_CATEGORY,
                "support": False
            },
            "-finline-limit-":
            {
                "action": "filter",
                "category": GCCOptionCategory.OPTIMIZATION,
                "support": False
            },
            "-ffixed-":
            {
                "action": "filter",
                "category": GCCOptionCategory.CODE_GENERATION,
                "support": False
            },
            "-fenable-":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-fdump-":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-fdisable-":
            {
                "action": "filter",
                "category": GCCOptionCategory.DEBUGGING,
                "support": False
            },
            "-fcall-used-":
            {
                "action": "filter",
                "category": GCCOptionCategory.CODE_GENERATION,
                "support": False
            },
            "-fcall-saved-":
            {
                "action": "filter",
                "category": GCCOptionCategory.CODE_GENERATION,
                "support": False
            },
            "-fbuiltin-":
            {
                "action": "filter",
                "category": GCCOptionCategory.C_LANGUAGE,
                "support": False
            },
            "-e":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-d":
            {
                "action": "keep",
                "category": GCCOptionCategory.UNKNOWN_CATEGORY,
                "support": True
            },
            "-Wp,":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-Wlarger-than-":
            {
                "action": "keep",
                "category": GCCOptionCategory.WARNING,
                "support": True
            },
            "-Wl,":
            {
                "action": "keep",
                "category": GCCOptionCategory.LINKER,
                "support": True
            },
            "-Wa,":
            {
                "action": "keep",
                "category": GCCOptionCategory.ASSEMBLER,
                "support": True
            },
            "-U":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-T":
            {
                "action": "keep",
                "category": GCCOptionCategory.LINKER,
                "support": True
            },
            "-R":
            {
                "action": "keep",
                "category": GCCOptionCategory.UNKNOWN_CATEGORY,
                "support": True
            },
            "-O":
            {
                "action": "keep",
                "category": GCCOptionCategory.OPTIMIZATION,
                "support": True
            },
            "-MT":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-MQ":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-MF":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-L":
            {
                "action": "keep",
                "category": GCCOptionCategory.DIRECTORY,
                "support": True
            },
            "-I":
            {
                "action": "keep",
                "category": GCCOptionCategory.DIRECTORY,
                "support": True
            },
            "-F":
            {
                "action": "keep",
                "category": GCCOptionCategory.UNKNOWN_CATEGORY,
                "support": True
            },
            "-D":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            },
            "-B":
            {
                "action": "keep",
                "category": GCCOptionCategory.DIRECTORY,
                "support": True
            },
            "-A":
            {
                "action": "keep",
                "category": GCCOptionCategory.PREPROCESSOR,
                "support": True
            }
    }