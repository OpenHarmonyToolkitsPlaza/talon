from command_parser.gcc_parse.gcc_option_dict.gcc_option_dict_base import GCCOPtionDictBase
from command_parser.gcc_parse.gcc_option_dict.gcc_option_dict_base import GCCOptionCategory


class GCCOptionDict_4_0_4(GCCOPtionDictBase):
    
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = option_dict
        self.init_option_dict()
        
    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()
    
    def load_change_options(self):
        
        self.discard_options = {
            "gcc_simple_dict": [],
            "gcc_space_dict": [],
            "gcc_equal_dict": [],
            "gcc_match_dict": []
        }   
        
        self.append_options = {
            "gcc_simple_dict": {},
            "gcc_space_dict": {},
            "gcc_equal_dict": {},
            "gcc_match_dict": {}
        }
        
class GCCOptionDict_4_1_2(GCCOPtionDictBase):
    
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_4_0_4(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        
        self.append_options = {
            "gcc_simple_dict": {
                "-mbacc": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-msecure-plt": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mindexed-addressing": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-ftree-copy-prop": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fstack-protector-all": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mlong-double-64": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-ftree-sink": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-Wstrict-selector-match": {
                    "category": GCCOptionCategory.OBJECTIVE_C,
                    "action": "keep"
                },
                "-mdsp": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fobjc-direct-dispatch": {
                    "category": GCCOptionCategory.OBJECTIVE_C,
                    "action": "keep"
                },
                "-fobjc-gc": {
                    "category": GCCOptionCategory.OBJECTIVE_C,
                    "action": "keep"
                },
                "-Wassign-intercept": {
                    "category": GCCOptionCategory.OBJECTIVE_C,
                    "action": "keep"
                },
                "-mno-optimize-membar": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-rdynamic": {
                    "category": GCCOptionCategory.LINKER,
                    "action": "keep"
                },
                "-minvalid-symbols": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fearly-inlining": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-misel": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wno-pragmas": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-Wunsafe-loop-optimizations": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-ftree-store-copy-prop": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mreturn-pointer-on-d0": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mvrsave": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-ffriend-injection": {
                    "category": GCCOptionCategory.CPP_LANGUAGE,
                    "action": "keep"
                },
                "-msseregparm": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mbss-plt": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fno-jump-tables": {
                    "category": GCCOptionCategory.CODE_GENERATION,
                    "action": "keep"
                },
                "-fstack-protector": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-ftree-salias": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mspe": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wno-attributes": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-moptimize-membar": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-ftree-store-ccp": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-madjust-unroll": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wpointer-sign": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-mpt-fixed": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wstack-protector": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-Wc++-compat": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-Wvolatile-register-var": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-fobjc-call-cxx-cdtors": {
                    "category": GCCOptionCategory.OBJECTIVE_C,
                    "action": "keep"
                },
                "-mmac": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fwhole-program": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mswdiv": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-funsafe-loop-optimizations": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-ftree-vect-loop-version": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-Wno-int-to-pointer-cast": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-Wno-pointer-to-int-cast": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-Wpragmas": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-unsafe-loop-optimizations": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wattributes": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-pointer-sign": {
            "category": "C-only Warning Options",
            "action": "keep"
        },
        "-Wno-stack-protector": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-c++-compat": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-volatile-register-var": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wint-to-pointer-cast": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wpointer-to-int-cast": {
            "category": "Warning Options",
            "action": "keep"
        }
            },
            "gcc_space_dict": {
                "-isysroot": {
                    "category": GCCOptionCategory.PREPROCESSOR,
                    "action": "keep"
                }
            },
            "gcc_equal_dict": {
                "-mdiv": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fsched-stalled-insns-dep": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mmacosx-version-min": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mtp": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mdivsi3_libfunc": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-memregs": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "--sysroot": {
                    "category": GCCOptionCategory.DIRECTORY,
                    "action": "keep"
                },
                "-mgettrcost": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-multcost": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                }
            },
            "gcc_match_dict": {}
        }

        self.discard_options = {
            "gcc_simple_dict": [
            "-msb",
            "-m32032",
            "-mnoregparam",
            "-fforce-mem",
            "-m32381",
            "-mnohimem",
            "-fspeculative-prefetching",
            "-m32081",
            "-m32532",
            "-mnosb",
            "-mregparam",
            "-ftree-based-profiling",
            "-mhimem",
            "-mint64",
            "-m32332"
            ],
            "gcc_space_dict": [],
            "gcc_equal_dict": [],
            "gcc_match_dict": []
        }

    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()
    
class GCCOptionDict_4_2_4(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_4_1_2(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        
        self.append_options = {
            "gcc_simple_dict": {
                "-Wno-address": {
                    "category": "Warning Options",
                    "action": "keep"
                },
                "-Woverflow": {
                    "category": "Warning Options",
                    "action": "keep"
                },
                "-Wno-overlength-strings": {
                    "category": "Warning Options",
                    "action": "keep"
                },
                "-Wno-strict-overflow": {
                    "category": "Warning Options",
                    "action": "keep"
                },
                "-fdiagnostics-show-option": {
                    "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
                    "action": "keep"
                },
                "-mno-sched-prefer-non-control-spec-insns": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mkernel": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mmulhw": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-muclibc": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mno-sched-count-spec-in-critical-path": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Waddress": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-fdump-noaddr": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-mnhwloop": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mno-sched-spec-verbose": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-msched-in-control-spec": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fsection-anchors": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fsched-stalled-insns": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-Wno-overflow": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-msched-br-in-data-spec": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mel": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-ftree-vrp": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-Woverlength-strings": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-msched-ldc": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fstrict-overflow": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fcprop-registers": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-msched-ar-in-data-spec": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fno-toplevel-reorder": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mscore5": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-meb": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fsched-stalled-insns-dep": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mscore7": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fgnu89-inline": {
                    "category": GCCOptionCategory.C_LANGUAGE,
                    "action": "keep"
                },
                "-mno-sched-control-spec": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fopenmp": {
                    "category": GCCOptionCategory.C_LANGUAGE,
                    "action": "keep"
                },
                "-mno-sched-control-ldc": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mshared": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-femit-class-debug-always": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-funit-at-a-time": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fipa-pta": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-muls": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-msched-ar-data-spec": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mscore5u": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fsee": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mno-sched-br-data-spec": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mstackrealign": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mcfv4e": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wstrict-overflow": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-mno-sched-prefer-non-data-spec-insns": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mdlmzb": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mscore7d": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-frtl-abstract-sequences": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                }
            },
            "gcc_space_dict": {
                "-imultilib": {
                    "category": GCCOptionCategory.PREPROCESSOR,
                    "action": "keep"
                }
            },
            "gcc_equal_dict": {
                "-Werror": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-Wno-error": {
                "category": "Warning Options",
                "action": "keep"
                }
            },
            "gcc_match_dict": {}
        }

        self.discard_options = {
            "gcc_simple_dict": [
                "-frerun-loop-opt",
                "-floop-optimize",
                "-fstrength-reduce",
                "-floop-optimize2",
                "-fno-const-strings",
                "-fshared-data"
            ],
            "gcc_space_dict": [],
            "gcc_equal_dict": [
                "-fsched-stalled-insns",
                "-fsched-stalled-insns-dep"
            ],
            "gcc_match_dict": []
        }

    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()


class GCCOptionDict_4_3_6(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_4_2_4(option_dict)
        self.init_option_dict()
        
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
                "-fcheck-data-deps": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mleaf-id-shared-library": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wtype-limits": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-femit-struct-debug-reduced": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-mfast-fp": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mno-short": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fpredictive-commoning": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fauto-inc-dec": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fforward-propagate": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fno-signed-zeros": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fdbg-cnt-list": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-mflip-mips16": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fvisibility-ms-compat": {
                    "category": GCCOptionCategory.CPP_LANGUAGE,
                    "action": "keep"
                },
                "-fipa-pure-const": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fpost-ipa-mem-report": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-mbranch-hints": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mgpopt": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mcld": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wcoverage-mismatch": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-msmartmips": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-femit-struct-debug-detailed": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-Bstatic": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fno-merge-debug-strings": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-non-static": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mstdmain": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-minline-ic_invalidate": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-m5206e": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-ftree-reassoc": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-Wclobbered": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-fipa-reference": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-msahf": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-ftree-dse": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mpc32": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wsign-conversion": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-fassociative-math": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-finline-small-functions": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-Wvla": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-mcx16": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-frecord-gcc-switches": {
                    "category": GCCOptionCategory.CODE_GENERATION,
                    "action": "keep"
                },
                "-print-sysroot-headers-suffix": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-fipa-matrix-reorg": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-femit-struct-debug-baseonly": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-fipa-cp": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mno-rtd": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-flax-vector-conversions": {
                    "category": GCCOptionCategory.C_LANGUAGE,
                    "action": "keep"
                },
                "-Wlogical-op": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-fsplit-wide-types": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mno-leaf-id-shared-library": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wold-style-declaration": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-msafe-dma": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-minterlink-mips16": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mstack-check-l1": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-m5407": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mextern-sdata": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wmissing-parameter-type": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-Warray-bounds": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-mlocal-sdata": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fipa-struct-reorg": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fvect-cost-model": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-Wempty-body": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-mdmx": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-m5307": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mpaired": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wtraditional-conversion": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-fmodulo-sched-allow-regmoves": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-msmall-mem": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-freciprocal-math": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fdce": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mrecip": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fdse": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mhard-dfp": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mrtp": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fpre-ipa-mem-report": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-mmt": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Xbind-now": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Xbind-lazy": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mdspr2": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mllsc": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-m528x": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mwarn-reloc": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wignored-qualifiers": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-Wc++0x-compat": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-Wno-type-limits": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-coverage-mismatch": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-clobbered": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-sign-conversion": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-vla": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-logical-op": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-old-style-declaration": {
            "category": "C and Objective-C-only Warning Options",
            "action": "keep"
        },
        "-Wno-missing-parameter-type": {
            "category": "C and Objective-C-only Warning Options",
            "action": "keep"
        },
        "-Wno-array-bounds": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-empty-body": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-traditional-conversion": {
            "category": "C and Objective-C-only Warning Options",
            "action": "keep"
        },
        "-Wno-ignored-qualifiers": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-c++0x-compat": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-pointer-sign": {
            "category": "C and Objective-C-only Warning Options",
            "action": "keep"
        }
            },
            "gcc_space_dict": {
            },
            "gcc_equal_dict": {
                "-femit-struct-debug-detailed": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "--help": {
                    "category": GCCOptionCategory.OVERALL,
                    "action": "keep"
                },
                "-ftree-parallelize-loops": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fdbg-cnt": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-fdebug-prefix-map": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-finstrument-functions-exclude-file-list": {
                    "category": GCCOptionCategory.CODE_GENERATION,
                    "action": "keep"
                },
                "-finstrument-functions-exclude-function-list": {
                    "category": GCCOptionCategory.CODE_GENERATION,
                    "action": "keep"
                },
                "-mcode-readable": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mveclibabi": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                }
            },
            "gcc_match_dict": {
                "-iframework": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                }
            }
        }
    
        self.discard_options = {
            "gcc_simple_dict": [
                "-mmpyi",
                "-m68881",
                "-mti",
                "-ftree-lrs",
                "-mdp-isr-reload",
                "-mfast-fix",
                "-mbk",
                "-ftree-store-copy-prop",
                "-msvr3-shlib",
                "-mdb",
                "-mloop-unsigned",
                "-fforce-addr",
                "-mregparm",
                "-mparallel-mpy",
                "-mrptb",
                "-mparallel-insns"
            ],
            "gcc_space_dict": [
            ],
            "gcc_equal_dict": [
                "-mrpts"
            ],
            "gcc_match_dict": []
        } 
 
    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()
        
class GCCOptionDict_4_4_7(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_4_3_6(option_dict)
        self.init_option_dict()
        
    def load_change_options(self):
        
        self.append_options = {
            "gcc_simple_dict": {
                "-Wenum-compare": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-fipa-cp-clone": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mgen-cell-microcode": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-floop-interchange": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mno-inefficient-warnings": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fselective-scheduling": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-msse2avx": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fno-ira-share-spill-slots": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-Wno-mudflap": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-mfix-r10000": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mcorea": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mcoreb": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wno-format-contains-nul": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-msimple-fpu": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-print-sysroot": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-msdram": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mserialize-volatile": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-msmall-model": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wno-deprecated": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-floop-block": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-msymbol-as-address": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wpacked-bitfield-compat": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-mfix-cortex-m3-ldrd": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-ftree-switch-conversion": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-ftree-builtin-call-dce": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-minline-stringops-dynamically": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mno-cygwin": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fsel-sched-pipelining-outer-loops": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mavoid-indexed-addresses": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mword-relocations": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-ftree-loop-distribution": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mcygwin": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mwarn-cell-microcode": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fprofile-correction": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mmulticore": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wno-builtin-macro-redefined": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-mno-lsim": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mwindows": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fno-ira-share-save-slots": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fconserve-stack": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-micplb": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fselective-scheduling2": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mplt": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mips64r2": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mwin32": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fira-coalesce": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-findirect-inlining": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mdll": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fcx-fortran-rules": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mbitops": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fsel-sched-pipelining": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-floop-strip-mine": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-mconsole": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mthread": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fno-dwarf2-cfi-asm": {
                    "category": GCCOptionCategory.DEBUGGING,
                    "action": "keep"
                },
                "-Wsync-nand": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-Wno-pedantic-ms-format": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-Wmudflap": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wformat-contains-nul": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wdeprecated": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-packed-bitfield-compat": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wbuiltin-macro-redefined": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-enum-compare": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-sync-nand": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wpedantic-ms-format": {
            "category": "Warning Options",
            "action": "keep"
        }
            },
            "gcc_space_dict": {
                "-T": {
                    "category": GCCOptionCategory.LINKER,
                    "action": "keep"
                },
            },
            "gcc_equal_dict": {
                "-mlarge-data-threshold": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mincoming-stack-boundary": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-Wlarger-than": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-mae": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fprofile-dir": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-Wframe-larger-than": {
                    "category": GCCOptionCategory.WARNING,
                    "action": "keep"
                },
                "-mstringop-strategy": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-mstack-increment": {
                    "category": GCCOptionCategory.MACHINE_DEPENDENT,
                    "action": "keep"
                },
                "-fira-region": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fira-algorithm": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-fira-verbose": {
                    "category": GCCOptionCategory.OPTIMIZATION,
                    "action": "keep"
                },
                "-Wno-larger-than": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-frame-larger-than": {
            "category": "Warning Options",
            "action": "keep"
        }
            },
            "gcc_match_dict": {
                "-Wp,": {
                    "category": GCCOptionCategory.PREPROCESSOR,
                    "action": "keep"
                }
            }
        }
        
        self.discard_options = {
            "gcc_simple_dict": [
                "-mbacc",
                "-maout",
                "-ftree-salias",
                "-melinux",
                "-mwindiss",
                "-ftree-store-ccp"
            ],
            "gcc_space_dict": [
            ],
            "gcc_equal_dict": [
                "-melinux-stacksize",
                "--help",
                "-minit-stack"
            ],
            "gcc_match_dict": [
                "-Wlarger-than-"
            ]
        }

    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()

class GCCOptionDict_4_5_4(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_4_4_7(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
            "-msign-extend-enabled": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmalloc64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msched-spec-ldc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msel-sched-dont-check-control-spec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mminmax": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsched-spec-insn-heuristic": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mcop": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m2a-nofpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fltrans": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fwpa": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fgraphite-identity": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mcop64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-no-canonical-prefixes": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-fcompare-debug-second": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fcompare-debug": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fno-pretty-templates": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-msimnovec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbarrel-shift-enabled": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsched-critical-path-heuristic": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-m2a-single-only": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmovbe": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcrc32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mas100-syntax": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mio-volatile": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-inline-sqrt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fira-loop-pressure": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wno-unused-result": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-ftree-forwprop": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-print-multi-os-directory": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-ftree-phiprop": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fsched-pressure": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mno-opts": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msave-acc-in-interrupts": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mivc2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-maddress-space-conversion": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdump-final-insns": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-municode": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrepeat": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-matomic-updates": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fwhopr": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-gno-strict-dwarf": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-inline-float-divide": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsched-rank-heuristic": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mall-opts": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gtoggle": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mcop32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmultiply-enabled": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msynci": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmcount-ra-address": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gstrict-dwarf": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mmult": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ftree-pta": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-msched-max-memory-insns-hard-limit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-floop-parallelize-all": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mdivide-enabled": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrelax-pic-calls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msched-stop-bits-after-every-cycle": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mabsdiff": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-maverage": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fenable-icf-debug": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-muser-enabled": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsched-group-heuristic": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-static-libstdc++": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-mleadz": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-flto": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fdump-unnumbered-links": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mbig-endian-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mclip": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msatur": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fvar-tracking-assignments-toggle": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fsched-dep-count-heuristic": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-m2a-single": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-inline-int-divide": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fvar-tracking-assignments": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mea32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fno-set-stack-executable": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m64bit-doubles": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m2a": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msched-fp-mem-deps-zero-cost": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fuse-linker-plugin": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-flto-report": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mdc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fipa-sra": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fsched-last-insn-heuristic": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wjump-misses-init": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wunsuffixed-float-constants": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wunused-result": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-enum-compare": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-unsuffixed-float-constants": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-jump-misses-init": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
        },
        "gcc_equal_dict": {
            "-mcache-size": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmax-constant-size": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fexcess-precision": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mint-register": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msched-max-memory-insns": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbased": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mconfig": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fplugin": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-mdebug-main": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fltrans-output-list": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-flto-compression-level": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-save-temps": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fcompare-debug": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-time": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fdump-final-insns": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mfp16-format": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ftemplate-depth": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mtiny": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            }
        },
        "gcc_match_dict": {
            "-fplugin-arg-": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-gdwarf-": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            }
        }
        }
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-msize",
            "-fsee",
            "-mno-sched-control-spec",
            "-mno-sched-br-data-spec",
            "-mno-sched-spec-verbose",
            "-gdwarf-2",
            "-mno-sched-prefer-non-control-spec-insns",
            "-mno-tablejump",
            "-mno-sched-count-spec-in-critical-path",
            "-Wunreachable-code",
            "-mno-sched-control-ldc",
            "-mno-dwarf2-asm",
            "-msched-ldc",
            "-mno-sched-prefer-non-data-spec-insns",
            "-fsched2-use-traces",
            "-frtl-abstract-sequences",
            "-mt"
        ],
        "gcc_space_dict": [
        ],
        "gcc_equal_dict": [],
        "gcc_match_dict": [
            "-ftemplate-depth-"
        ]
        }

    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()
        
class GCCOptionDict_4_6_4(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_4_5_4(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
            "-fpartial-inlining": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wunused-but-set-parameter": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mglibc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-floop-flatten": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mno-clearbss": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fcombine-stack-adjustments": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-diagnostics-show-option": {
                "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
                "action": "keep"
            },
            "-mxl-float-sqrt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-tno-android-ld": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdump-ada-spec": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-mv850e2v3": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Ofast": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mrecip-precision": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wunused-but-set-variable": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-cpp": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mxl-gp-opt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fcompare-elim": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mprefer-avx128": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fnothrow-opt": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-ftree-loop-if-convert-stores": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fstack-usage": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mfentry": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mv850es": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-int-to-pointer-cast": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mliw": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ftree-bit-ccp": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fdevirtualize": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-msmall-divides": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfix-at697f": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mandroid": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fplan9-extensions": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-mxl-barrel-shift": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mv850e2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fstrict-volatile-bitfields": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-ftree-loop-distribute-patterns": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-tno-android-cc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxl-soft-div": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fobjc-nilcheck": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-mforce-no-pic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxl-pattern-compare": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvect8-ret-in-mem": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wtrampolines": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mxl-multiply-high": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxl-soft-mul": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbionic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxl-stack-check": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m8bit-idiv": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsplit-stack": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-mavx256-split-unaligned-load": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-wrapper": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-mvzeroupper": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ftree-loop-if-convert": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mfriz": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fipa-profile": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mam34": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mam33-2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxl-float-convert": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wdouble-promotion": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wnoexcept": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-unused-but-set-parameter": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-unused-but-set-variable": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wcpp": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wint-to-pointer-cast": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-trampolines": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-implicit": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-double-promotion": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
        },
        "gcc_equal_dict": {
            "-mrecip": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fmax-errors": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-flto": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mcmodel": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-iplugindir": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-Wsuggest-attribute": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fdump-go-spec": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-fconstexpr-depth": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-ffp-contract": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-flto-partition": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fobjc-abi-version": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-fobjc-std": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-mblock-move-inline-limit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-suggest-attribute": {
            "category": "Warning Options",
            "action": "keep"
            }
        },
        "gcc_match_dict": {
            "-mxl-mode-": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            }
        }
        }
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-fargument-alias",
            "-fdiagnostics-show-option",
            "-mcygwin",
            "-fltrans",
            "-flto",
            "-fwhopr",
            "-fwpa",
            "-I-",
            "-msplit",
            "-Wmissing-noreturn",
            "-fira-coalesce",
            "-mno-cygwin",
            "-combine",
            "-mswdiv",
            "-mno-split"
        ],
        "gcc_space_dict": [
            "-V",
            "-b",
            "-fmudflap",
            "-iquote"
        ],
        "gcc_equal_dict": [
            "-fltrans-output-list",
            "--sysroot",
            "-specs"
        ],
        "gcc_match_dict": [
            "-L",
            "-iquote"
        ]
        }
  
    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()

class GCCOptionDict_4_7_4(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_4_6_4(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
                "-Wdelete-non-virtual-dtor": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wunused-local-typedefs": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mfix-24k": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcr16cplus": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcmove": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpointers-to-nested-functions": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mshort-calls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpid": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhalf-reg-file": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wvector-operation-performance": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-ftree-tail-merge": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-munaligned-access": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-grecord-gcc-switches": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mstrict-X": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gno-record-gcc-switches": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-msoft-atomic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpretend-cmove": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdebug-cpp": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-mflat": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wnonnull": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mcmpeqdi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ftrack-macro-expansion": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-mcbranchdi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbit-ops": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpopc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-I-": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-mprefer-short-insn-regs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-free-nonheap-object": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fshrink-wrap": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-msave-toc-indirect": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fallow-parameterless-variadic-functions": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-mfmaf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msetlb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvis2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdump-passes": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-msmall16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ffat-lto-objects": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wmaybe-uninitialized": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-maccumulate-args": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msplit-vecmove-early": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvis3": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-free": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wnarrowing": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wzero-as-null-pointer-constant": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wc++11-compat": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-vector-operation-performance": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-nonnull": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wfree-nonheap-object": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-maybe-uninitialized": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-unused-local-typedefs": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-zero-as-null-pointer-constant": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-c++11-compat": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
            "-iquote": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            }
        },
        "gcc_equal_dict": {
            "-mstack-offset": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wstack-usage": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-ftrack-macro-expansion": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-mmul": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-max-vect-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-specs": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-mnops": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "--sysroot": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-mfp-mode": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtls-dialect": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdata-model": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-stack-usage": {
                "category": "Warning Options",
                "action": "keep"
            }
        },
        "gcc_match_dict": {
            "-fenable-": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-L": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-m1reg-": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-iquote": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            }
        }
        }
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-fenable-icf-debug",
            "-m6812",
            "-mauto-incdec",
            "-minmax",
            "-fipa-struct-reorg",
            "-m6811",
            "-mmangle-cpu",
            "-Wc++0x-compat"
        ],
        "gcc_space_dict": [
        ],
        "gcc_equal_dict": [
            "-mtext",
            "-msoft-reg-count"
        ],
        "gcc_match_dict": []
        }

    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()
  
class GCCOptionDict_4_8_5(GCCOPtionDictBase):
    
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_4_7_4(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
                "-Wliteral-suffix": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mtas": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mexr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fno-eliminate-unused-debug-types": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-m5-compact": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mzdcbranch": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mquad-memory": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrh850-abi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-static-libasan": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-fno-gnu-unique": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-mfix-cortex-a53-835769": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fhoist-adjacent-loads": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fprofile-report": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftree-slsr": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fdebug-types-section": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mcrypto": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fira-hoist-pressure": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mfsrra": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "--no-sysroot-suffix": {
                "category": GCCOptionCategory.DIRECTORY,
                "action": "keep"
            },
            "-mfix-ut699": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-faggressive-loop-optimizations": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mloop": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-floop-nest-optimize": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mv850e3v5": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsync-libcalls": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fdelete-dead-exceptions": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-Waddr-space-convert": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgcc-abi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m5-64media": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mstrict-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfsca": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m5-32media-nofpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mquad-memory-atomic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wpedantic": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-static-libtsan": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-mno-float": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgeneral-regs-only": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpowerpc-gpopt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m5-64media-nofpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fopt-info": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-Wsizeof-pointer-memaccess": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mno-warn-multiple-fast-interrupts": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpower8-vector": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fmem-report-wpa": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-m5-compact-nofpu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-muser-mode": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Og": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-diagnostics-show-caret": {
                "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
                "action": "keep"
            },
            "-mlong-jumps": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxl-reorder": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcbcond": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m5-32media": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpower8-fusion": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ftree-partial-pre": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mcompat-align-parm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-return-local-addr": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mdirect-move": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmcu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-exr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ftree-coalesce-vars": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wuseless-cast": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-pedantic": {
            "category": "Warning Options",
            "action": "keep"
            },
            "-Wno-sizeof-pointer-memaccess": {
                "category": "Warning Options",
                "action": "keep"
            },
            "-Wreturn-local-addr": {
                "category": "Warning Options",
                "action": "keep"
            },
            "-Wno-useless-cast": {
                "category": "Warning Options",
                "action": "keep"
            }
        },
        "gcc_space_dict": {
            "-fext-numeric-literals": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
        },
        "gcc_equal_dict": {
            "-fsanitize": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mhotpatch": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-matomic-model": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-maltivec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fstack-reuse": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fuse-ld": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-ftemplate-backtrace-limit": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mpointer-size": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fada-spec-parent": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            }
        },
        "gcc_match_dict": {}
        }
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-mhitachi",
            "-mpower",
            "-malpha-as",
            "-floop-flatten",
            "-fconserve-space",
            "-msoft-atomic",
            "-mcirrus-fix-invalid-insns",
            "-feliminate-unused-debug-types",
            "-fipa-matrix-reorg",
            "-pedantic",
            "-madjust-unroll",
            "-mnew-mnemonics"
        ],
        "gcc_space_dict": [],
        "gcc_equal_dict": [
            "-Wformat"
        ],
        "gcc_match_dict": []
        }
   
   
    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()
           
class GCCOptionDict_4_9_4(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_4_8_5(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
            "-Wdelete-incomplete": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mea": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-cmov": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdpfp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fvtv-counts": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-m16-bit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-cache-volatile": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-mpy": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msimd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdevirtualize-speculatively": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mno-fast-sw-div": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgp-direct": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mslow-flash-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdpfp-compact": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wdate-time": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mdump-tune-features": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fisolate-erroneous-paths-dereference": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-masm-hex": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlarge": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-static-libubsan": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-fisolate-erroneous-paths-attribute": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mforce-fp-as-gp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mforbid-fp-as-gp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fprofile-reorder-functions": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mno-gp-direct": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhtm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-default": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mreduced-regs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbarrel-shifter": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrestrict-it": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mctor-dtor": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mswap": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-dpfp-lrsr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-static-liblsan": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-flive-range-shrinkage": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-minterlink-compressed": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbypass-cache": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mex9": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wfloat-conversion": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mv3push": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-meva": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mperf-ext": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-v3push": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfull-regs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmicromips": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-hw-mul": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-perf-ext": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnorm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmul32x16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fopenmp-simd": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-msmall": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mspfp-compact": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcmov": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mspfp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-minrt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-flto-report-wpa": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mimadd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfix-rm7000": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdpfp-fast": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmul64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfix-cortex-a53-843419": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mneon-for-64bits": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fvtv-debug": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fstack-protector-strong": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mclear-hwcap": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvirt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wopenmp-simd": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mspfp-fast": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ftree-loop-vectorize": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wconditionally-supported": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-date-time": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-float-conversion": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-openmp-simd": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-delete-incomplete": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-conditionally-supported": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
        },
        "gcc_equal_dict": {
            "-fvtable-verify": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mmemset-strategy": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-misr-vector-size": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnan": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdiagnostics-color": {
                "category": GCCOptionCategory.LANGUAGE_INDEPENDENT,
                "action": "keep"
            },
            "-mstack-protector-guard": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcache-block-size": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmemcpy-strategy": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fvect-cost-model": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mtune-ctrl": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcustom-fpu-cfg": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mabs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            }
        },
        "gcc_match_dict": {
            "-mcustom-": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            }
        }
        }
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-mcmpeqdi",
            "-mcbranchdi",
            "-fno-default-inline",
            "-fvect-cost-model",
            "-foptimize-register-move",
            "-ftree-vect-loop-version"
        ],
        "gcc_space_dict": [],
        "gcc_equal_dict": [
            "-ftree-vectorizer-verbose"
        ],
        "gcc_match_dict": []
        }
 
    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()
    
class GCCOptionDict_5_5_0(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_4_9_4(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
            "-Wno-int-conversion": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mrecord-mcount": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mminimal-toc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-segs_read_write_addr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wbool-compare": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fopenacc": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-mpa-risc-2-0": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-spe": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fipa-icf": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mno-bypass-cache": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-xgot": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-debug": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-upper-regs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-ieee": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-current_version": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxop": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxsaveopt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-micromips": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-use-nochk-string-functions": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-m32bit-doubles": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wsuggest-final-types": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mno-single-exit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fPIE": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-mlp64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-static-libmpxwrappers": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-fchkp-use-static-bounds": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-string": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-omit-leaf-frame-pointer": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlwp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-extern-sdata": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdwarf2-asm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-image_base": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-embedded-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-vrsave": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wnormalized": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mno-fsca": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fstdarg-opt": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mdivide-breaks": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-unexported_symbols_list": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-whyload": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-force_flat_namespace": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-upper-regs-df": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcr16c": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlong-double-128": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpopcntd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx256-split-unaligned-store": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmax": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wshift-count-negative": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-sectobjectsymbols": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-floop-unroll-and-jam": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-multiply_defined_unused": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fmaf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fno-local-ivars": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-msched-control-spec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-longcalls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fix-24k": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-umbrella": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fused-madd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmfpgpr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlzcnt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fsrra": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcache-volatile": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-nodevicelib": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcbranch-force-delay-slot": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-check-zero-division": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpclmul": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-static-libmpx": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-fno-for-scope": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mno-v8plus": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-user-mode": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msse3": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-float32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mabm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcix": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-pagezero_size": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mep": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-treat-zero-dynamic-size-as-infinite": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-Wsuggest-final-methods": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-munaligned-doubles": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gz": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-compat-align-parm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-sum-in-toc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-direct-move": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-multiply_defined": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-seglinkedit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-explicit-relocs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-undefined": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmpx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-popcntd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmfcrf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-bitfield": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxsave": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msched-br-data-spec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbit-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfma4": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fno-peephole2": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-noseglinkedit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfma": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mupper-regs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxsavec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mssse3": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-flra-remat": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-msv-mode": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsanitize-recover": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mmainkernel": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wswitch-bool": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mrdrnd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-abicalls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-keep_private_externs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-eva": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-seg1addr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-seg_addr_table_filename": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-vis": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-dynamic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsized-deallocation": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mno-packed-stack": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-pointers-to-nested-functions": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlarge-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-dylinker_install_name": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mprefetchwt1": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-cbcond": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhw-div": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpc64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-single_module": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-vis3": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfix": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfxsr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-client_name": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-merror-reloc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-vr4130-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-no_dead_strip_inits_and_terms": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-popcntb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msched-prefer-non-data-spec-insns": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdata-sections": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-msha": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mupper-regs-df": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-masm-syntax-unified": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fix-r4400": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-modd-spreg": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-twolevel_namespace": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxsaves": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpowerpc64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-check-incomplete-type": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fchkp-use-static-const-bounds": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-cmpb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-recip-precision": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wchkp": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mf16c": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-mips16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-whatsloaded": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fno-stack-limit": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-msse4a": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-sub_library": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msdata": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-hardlit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpa-risc-1-1": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-multi_module": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-atomic-updates": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-noall_load": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-local-sdata": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-install_name": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-gpopt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-smartmips": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-prebind_all_twolevel_modules": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-mvcle": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-backchain": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-dspr2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-sectorder": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-epsilon": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhw-mulx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx512f": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-serialize-volatile": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msse": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-noprebind": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpopcnt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mprolog-function": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnop-mcount": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fix-cortex-a53-835769": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-popc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfprnd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-powerpc-gpopt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-segs_read_only_addr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-interlink-compressed": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-int16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-callgraph-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips32r3": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-check-write": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fchkp-store-bounds": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-isel": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips32r6": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-use-fast-string-functions": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-power8-vector": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mupper-regs-sf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrmw": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-dlmzb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-marm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx512pf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdevirtualize-at-ltrans": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mno-mfpgpr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips64r6": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-dsp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrtm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-seg_addr_table": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-target-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-plt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-dependency-file": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-optimize": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-msched-prefer-non-control-spec-insns": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips64r3": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fssa-phiopt": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-msse4.1": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-paired-single": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-synci": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m68881": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-update": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fprnd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fix-r10000": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fipa-cp-alignment": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-msse4": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fp-exceptions": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mxpa": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Bdynamic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-uninit-const-in-rodata": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-int32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mesa": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfsgsbase": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-xpa": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fp-in-toc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-libfuncs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-branch-likely": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-filelist": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-mulhw": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-hw-mulx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips64r5": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-imadd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-virt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wsizeof-array-argument": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-metrax100": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx512cd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msse4.2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsanitize-undefined-trap-on-error": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-fix-r4000": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-flat": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-odd-spreg": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-O1": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-segaddr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mconst-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-maes": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-save-toc-indirect": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-instrument-marked-only": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-sym32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-slow-bytes": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-flat_namespace": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlarge-mem": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-private_bundle": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-wide-bitfields": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mips32r5": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbmi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpowerpc-gfxopt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-exported_symbols_list": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-mt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wmemset-transposed-args": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-sectcreate": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wlogical-not-parentheses": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mea64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msched-count-spec-in-critical-path": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-narrow-to-innermost-array": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-relax-pic-calls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtbm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-sectalign": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-llsc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-disable-callt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlong-double-80": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m8-bit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fix-cortex-a53-843419": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-zdcbranch": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfast-sw-div": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-funsigned-bitfields": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-sub_umbrella": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-mul-bug-workaround": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-shared": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-instrument-calls": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-tls-direct-seg-refs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcmpb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlarge-text": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fix-sb1": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fschedule-fusion": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mno-text-section-literals": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-headerpad_max_install_names": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpopcntb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-powerpc-gfxopt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m31": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mc68020": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-recip": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-prototype": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wsuggest-override": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-m3dnow": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-relocatable-lib": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-compatibility_version": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-read_only_relocs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-multiple": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-const16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-base-addresses": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-long-calls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-quad-memory": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-branch-predict": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-avoid-indexed-addresses": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-xl-compat": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-munsafe-dma": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wmisspelled-isr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmwaitx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-memcpy": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mc68000": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-upper-regs-sf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-crypto": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fix-rm7000": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fstack-protector-explicit": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mno-mad": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-small-exec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-mdmx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fix-vr4120": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m128bit-long-double": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-tpf-trace": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-check-read": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-nomultidefs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-address-space-conversion": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-dylib_file": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-first-field-has-own-bounds": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mmul.x": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-m340": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-hw-div": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-nofixprebinding": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fcheck-pointer-bounds": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-vis2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-htm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-prebind": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhw-mul": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mdata-align": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtoc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fp-regs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlittle-endian-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-4byte-functions": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-init": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-altivec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-mips3d": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-relocatable": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfpxx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-static-libgcc": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-mno-hard-dfp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfloat-gprs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fauto-profile": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mprint-tune-info": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-dead_strip": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-mfcrf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fipa-ra": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-malign-power": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-quad-memory-atomic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msse2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-friz": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-segprot": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-narrow-bounds": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mbmi2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-split-addresses": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpc80": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsemantic-interposition": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mavx512er": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-interlink-mips16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-float64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfloat-ieee": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchkp-use-wrappers": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-div": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wshift-count-overflow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mfaster-structs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-weak_reference_mismatches": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-shadow-ivar": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-designated-init": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wc99-c11-compat": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wc90-c99-compat": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wabi-tag": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wc++14-compat": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-discarded-qualifiers": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-discarded-array-qualifiers": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-bool-compare": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-suggest-final-types": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-normalized": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-shift-count-negative": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-suggest-final-methods": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-switch-bool": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-chkp": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-sizeof-array-argument": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-memset-transposed-args": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-logical-not-parentheses": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-suggest-override": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-shift-count-overflow": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wint-conversion": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wshadow-ivar": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wdesignated-init": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-c99-c11-compat": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-c90-c99-compat": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-c++14-compat": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wdiscarded-qualifiers": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wdiscarded-array-qualifiers": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
            
            "-z": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-iwithprefixbefore": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-l": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            }
        },
        "gcc_equal_dict": {
            "-falign-labels": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mgpopt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fprofile-use": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Warray-bounds": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wformat": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fasan-shadow-offset": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-malign-data": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-falign-loops": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "--help": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-fstack-limit-symbol": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-fsched-stalled-insns": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mstack-size": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fivar-visibility": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-fprofile-generate": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fsched-stalled-insns-dep": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fauto-profile": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-falign-functions": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-gz": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-falign-jumps": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wstrict-overflow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fsanitize-recover": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-Wno-array-bounds": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-format": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-strict-overflow": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_match_dict": {
            "-mno-custom-": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gcoff": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-ggdb": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gstabs": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fopt-info-": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fno-builtin-": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-gxcoff": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gvms": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            }
        }}
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-mforce-fp-as-gp",
            "-mforbid-fp-as-gp",
            "-mpid",
            "-mno-gp-direct",
            "-mbit-ops",
            "-mno-warn-multiple-fast-interrupts",
            "-mas100-syntax",
            "-mgp-direct",
            "-mbig-endian-data",
            "-mwords-little-endian",
            "-mex9",
            "-msave-acc-in-interrupts",
            "-mips32r2"
        ],
        "gcc_space_dict": [],
        "gcc_equal_dict": [
            "-mdata-model",
            "-mmax-constant-size",
            "-mint-register"
        ],
        "gcc_match_dict": []
        } 

    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()

class GCCOptionDict_6_5_0(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_5_5_0(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
 "gcc_simple_dict": {
            "-Woverride-init-side-effects": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fchecking": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            },
            "-mavx512vbmi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-float128-hardware": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mms-bitfields": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsplit-paths": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mavx512bw": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wtautological-compare": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mavx512dq": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-auto-litpools": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-funconstrained-commons": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wplacement-new": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-scalar-storage-order": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Winvalid-memory-model": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mlra": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx512ifma": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Whsa": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mallregs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-std-struct-return": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mzvector": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mwarn-mcu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-cdx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wnonnull-compare": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wframe-address": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mclflushopt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fkeep-static-functions": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mdiv-rem": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mauto-litpools": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-aggressive-loop-optimizations": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mfloat128-hardware": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-float128": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcdx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wnamespaces": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-moptimize": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-frame-header-opt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wshift-overflow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-gdwarf": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-Wvirtual-inheritance": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-plt": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "--coverage": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-mmitigate-rop": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wunused-const-variable": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wnull-dereference": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mno-bmx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-matomic": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mg13": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfloat128": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wduplicated-cond": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mindirect-branch-register": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx512vl": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mframe-header-opt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mreadonly-in-sdata": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmusl": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mstd-struct-return": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mg10": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mll64": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlow-precision-recip-sqrt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcode-density": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnodiv": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mg14": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fssa-backprop": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wshift-negative-value": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mbmx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcaller-copies": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wmultiple-inheritance": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wmisleading-indentation": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wsubobject-linkage": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wignored-attributes": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-tautological-compare": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-placement-new": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wscalar-storage-order": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-invalid-memory-model": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-hsa": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-nonnull-compare": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-frame-address": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Waggressive-loop-optimizations": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-shift-overflow": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-unused-const-variable": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-null-dereference": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-duplicated-cond": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-shift-negative-value": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-override-init-side-effects": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-misleading-indentation": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-subobject-linkage": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-ignored-attributes": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
            
            "-Wtemplates": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
        },
        "gcc_equal_dict": {
            "-mdata-region": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wshift-overflow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fsanitize-sections": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-mindirect-branch": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wunused-const-variable": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-msilicon-errata-warn": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfunction-return": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsso-struct": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-Wplacement-new": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-msilicon-errata": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmpy-option": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcompact-branches": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-freorder-blocks-algorithm": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mcode-region": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-shift-overflow": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-unused-const-variable": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-placement-new": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_match_dict": {
            "-fdump-rtl-": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            }
        }
    
        }
        self.discard_options = {
            "gcc_simple_dict": [
            "-m5-64media-nofpu",
            "-m5-compact",
            "-m5-32media",
            "-fcheck-data-deps",
            "-m5-64media",
            "-mindexed-addressing",
            "-fshort-double",
            "-ftree-copyrename",
            "-minvalid-symbols",
            "-m5-compact-nofpu",
            "-m5-32media-nofpu",
            "-mpt-fixed"
        ],
        "gcc_space_dict": [],
        "gcc_equal_dict": [
            "-mgettrcost"
        ],
        "gcc_match_dict": []
        }
 
    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()
  
class GCCOptionDict_7_5_0(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_6_5_0(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
            "-Wregister": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wnoexcept-type": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mno-vis4": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fno-fp-int-builtin-inexact": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mfix-gr712rc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdump-tree-all": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            },
            "-m80387": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-lra": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-builtin-declaration-mismatch": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mno-lxc1-sxc1": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fsmuld": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wimplicit-fallthrough": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mgnu-attribute": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fno-printf-return-value": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mno-upper-regs-di": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wduplicated-branches": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fnew-inheriting-ctors": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fdirectives-only": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-mload-store-pairs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mabsdata": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdiagnostics-parseable-fixits": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-fipa-vrp": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fpch-preprocess": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-fdiagnostics-generate-patch": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-gcolumn-info": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-save-restore": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-flimit-function-alignment": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mvis4b": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wrestrict": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fcode-hoisting": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-MD": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-Wint-in-bool-context": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mlong-jump-table-offsets": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-load-store-pairs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfix-ut700": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msave-mduc-in-interrupts": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wpointer-compare": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mpku": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvis4": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpure-code": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gno-column-info": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fshrink-wrap-separate": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wunused-macros": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mno-madd4": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fdiv": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wmemset-elt-size": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fno-show-column": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-Waligned-new": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mfdiv": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Walloc-zero": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-MMD": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-ftrampolines": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-Wexpansion-to-defined": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mno-gnu-attribute": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fno-canonical-system-headers": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-fpreprocessed": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-Wbool-operation": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wswitch-unreachable": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mno-low-precision-div": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcmse": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfract-convert-truncate": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wdangling-else": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-dumpfullversion": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            },
            "-msave-restore": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fstore-merging": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mlxc1-sxc1": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-faligned-new": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Walloca": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mno-vis4b": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-CC": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-mupper-regs-di": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mclzero": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdollars-in-identifiers": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-mfsmuld": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fipa-bit-cp": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-m3dnowa": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmadd4": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msubxc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ftime-report-details": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            },
            "-fextended-identifiers": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-fpch-deps": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-mno-subxc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wstringop-overflow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fnew-ttp-matching": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wbuiltin-declaration-mismatch": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-implicit-fallthrough": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-duplicated-branches": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-restrict": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-int-in-bool-context": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-pointer-compare": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-unused-macros": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-memset-elt-size": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-aligned-new": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-alloc-zero": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-expansion-to-defined": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-bool-operation": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-switch-unreachable": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-dangling-else": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-alloca": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-stringop-overflow": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
        },
        "gcc_equal_dict": {
            "-fchecking": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            },
            "-fwide-exec-charset": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-finput-charset": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-fpermitted-flt-eval-methods": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-Wstringop-overflow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-msmall-data-limit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mstack-protector-guard-reg": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Walloc-size-larger-than": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wimplicit-fallthrough": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wnormalized": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fconstexpr-loop-limit": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mstack-protector-guard-offset": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtp-regno": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fexec-charset": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-Wshadow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Walloca-larger-than": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wvla-larger-than": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-ftabstop": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-Wno-stringop-overflow": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-alloc-size-larger-than": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-implicit-fallthrough": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-normalized": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-shadow": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-alloca-larger-than": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-vla-larger-than": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_match_dict": {
            "-fdump-tree-": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            }
        }
    
        }
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-ftree-loop-if-convert-stores",
            "-fsplit-paths",
            "-fstack-protector",
            "-fhosted",
            "-funsafe-loop-optimizations",
            "-fipa-cp-alignment"
        ],
        "gcc_space_dict": [
        ],
        "gcc_equal_dict": [
            "--help"
        ],
        "gcc_match_dict": [
            "-I",
            "-iquote"
        ]
        }

    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()

class GCCOptionDict_8_5_0(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_7_5_0(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
           "gcc_simple_dict": {
               "-Wextra-semi": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wif-not-aligned": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wpacked-not-aligned": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wclass-memaccess": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-ginline-points": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gno-statement-frontiers": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gno-as-loc-support": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mbe8": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnopm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvaes": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpc-relative-literal-loads": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mforce-indirect-call": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcompress": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wcatch-value": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-gas-loc-support": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mlow-precision-sqrt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-static-pie": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-Wcast-function-type": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mclwb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mext-perf2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mext-perf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmain-is-OS_task": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gno-variable-location-views": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fdiagnostics-show-template-tree": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-mbe32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mshstk": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mjli-always": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-madx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mwbnoinvd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wstringop-truncation": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mgas-isr-prologues": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrf16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx5124vnniw": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mprfchw": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gstatement-frontiers": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-gno-internal-reset-location-views": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mmovdir64b": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlow-precision-div": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mft32b": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpconfig": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcall-sysv-eabi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gno-inline-points": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fno-elide-type": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-fdump-lang-all": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            },
            "-fstack-protector": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-mavx512vpopcntdq": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrdseed": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mverbose-cost-dump": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcall-sysv-noeabi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx512vbmi2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mext-string": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcall-eabi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-ext-perf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gvariable-location-views": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mvpclmulqdq": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mrdpid": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wsizeof-pointer-div": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mgfni": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcall-aixdesc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx512vnni": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx512bitalg": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcall-linux": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wmissing-attributes": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-moutline-atomics": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ginternal-reset-location-views": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-nodevicespecs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcall-ms2sysv-xlogues": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcall-openbsd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mavx5124fmaps": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-ext-perf2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmovdiri": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mflip-thumb": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wmultistatement-macros": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-msgx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fprofile-abs-path": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-mno-relax": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcall-freebsd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-ext-string": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-catch-value": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-cast-function-type": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-stringop-truncation": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-sizeof-pointer-div": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-missing-attributes": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-multistatement-macros": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-extra-semi": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-if-not-aligned": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-packed-not-aligned": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
        },
        "gcc_equal_dict": {
            "-mrgf-banked-regs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mr0rel-sec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mprefer-vector-width": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlpc-width": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wcatch-value": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mblock-compare-inline-loop-limit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mstring-compare-inline-limit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gvariable-location-views": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-ffile-prefix-map": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-mtraceback": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wcast-align": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mstack-protector-guard-symbol": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mgprel-sec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msign-return-address": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mirq-ctrl-saved": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fmacro-prefix-map": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-moverride": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mblock-compare-inline-limit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-catch-value": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-cast-align": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_match_dict": {
            "-fdump-lang-": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            }
        }
    }
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-gcoff",
            "-mupper-regs-sf",
            "-mno-string",
            "-mgen-cell-microcode",
            "-mno-upper-regs-sf",
            "-mupper-regs-df",
            "-fstrict-overflow",
            "-mperf-ext",
            "-mstring",
            "-mno-upper-regs",
            "-mno-upper-regs-di",
            "-mupper-regs",
            "-mupper-regs-di",
            "-mno-perf-ext",
            "-feliminate-dwarf2-dups",
            "-mno-fix-cortex-a53-835769",
            "-mwarn-cell-microcode",
            "-mno-upper-regs-df",
            "-mno-fix-cortex-a53-843419",
            "-mno-low-precision-div",
            "-mno-direct-move",
            "-mdirect-move"
        ],
        "gcc_space_dict": [
            "-fdump-class-hierarchy",
            "-fdump-translation-unit"
        ],
        "gcc_equal_dict": [],
        "gcc_match_dict": [
            "-gcoff"
        ]
        }

    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()

class GCCOptionDict_9_5_0(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_8_5_0(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
                "-Wc11-c2x-compat": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
                "-Wno-init-list-lifetime": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-class-conversion": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wdeprecated-copy": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wc++17-compat": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-terminate": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mgnu-asm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtrack-speculation": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mistack": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mloongson-ext2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdump-earlydebug": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            },
            "-mno-low64k": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msoft-mul": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msecurity": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-medsp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mlongcall": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfdivdu": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mccrt": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Waddress-of-packed-member": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mloongson-ext": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhard-div": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-pltseq": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msched-prolog": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mloongson-mmi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcache": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-longcall": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ftree-scev-cprop": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mstack-size": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-manchor": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fchar8_t": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-prio-ctor-dtor": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mhigh-registers": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mpushpop": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mwaitpkg": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gdescribe-dies": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mpltseq": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-loongson-mmi": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-ginv": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gsplit-dwarf": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mno-crc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfix-r5900": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msplit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fipa-reference-addressable": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mmanual-endbr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mvdsp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-loongson-ext2": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mtrust": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fno-diagnostics-show-line-numbers": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-mptwrite": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msfimm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mnewlib": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcldemote": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdump-debug": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            },
            "-mror": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mshftimm": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fsave-optimization-record": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fno-diagnostics-show-labels": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-mhard-mul": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-r": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-msext": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wmissing-profile": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mno-loongson-ext": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmp": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbranch-index": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msoft-div": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-nolibc": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-mhle": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mconstpool": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmultiple-stld": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fipa-stack-alignment": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wno-attribute-warning": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mpic-data-is-text-relative": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-msmart": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcrc": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mginv": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-melrw": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-fix-r5900": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-address-of-packed-member": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wprio-ctor-dtor": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-missing-profile": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wattribute-warning": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-c++17-compat": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-c11-c2x-compat": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
            "-e": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
        },
        "gcc_equal_dict": {
            "--entry": {
                "category": GCCOptionCategory.LINKER,
                "action": "keep"
            },
            "-mboard": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fprofile-exclude-files": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-fdiagnostics-format": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-mfentry-section": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fconstexpr-ops-limit": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wattribute-alias": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-minstrument-return": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fcf-protection": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-fprofile-update": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-mharden-sls": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mbranch-protection": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fopenacc-dim": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-fdiagnostics-minimum-margin-width": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-mfentry-name": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fprofile-filter-files": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-flive-patching": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wno-attribute-alias": {
            "category": "Warning Options",
            "action": "keep"
            }
        },
        "gcc_match_dict": {}
    }
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-mbranch-expensive",
            "-fchkp-use-fast-string-functions",
            "-fchkp-use-static-bounds",
            "-fchkp-instrument-marked-only",
            "-static-libmpx",
            "-fbounds-check",
            "-fchkp-narrow-bounds",
            "-fno-for-scope",
            "-mno-spe",
            "-mbcopy",
            "-mbranch-cheap",
            "-msimple-fpu",
            "-mfloat32",
            "-mno-float32",
            "-mno-rtd",
            "-mpaired",
            "-fchkp-treat-zero-dynamic-size-as-infinite",
            "-mfloat-gprs",
            "-mbcopy-builtin",
            "-ffriend-injection",
            "-fchkp-narrow-to-innermost-array",
            "-mmitigate-rop",
            "-mspe",
            "-fchkp-optimize",
            "-fchkp-check-read",
            "-fchkp-check-incomplete-type",
            "-mno-float64",
            "-mfloat64",
            "-fchkp-use-static-const-bounds",
            "-fchkp-use-wrappers",
            "-fchkp-use-nochk-string-functions",
            "-mabshi",
            "-mlow-64k",
            "-fchkp-first-field-has-own-bounds",
            "-fchkp-instrument-calls",
            "-fchkp-check-write",
            "-ffor-scope",
            "-fchkp-store-bounds",
            "-mno-abshi",
            "-fcheck-pointer-bounds",
            "-static-libmpxwrappers",
            "-mmpx"
        ],
        "gcc_space_dict": [],
        "gcc_equal_dict": [
            "-misel",
            "-maltivec",
            "-mfloat-gprs",
            "-mspe"
        ],
        "gcc_match_dict": []
        }

    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()

class GCCOptionDict_10_4_0(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_9_5_0(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
                "-Wcomma-subscript": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-incompatible-pointer-types": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wredundant-tags": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wsized-deallocation": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-subobject-linkage": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wmismatched-tags": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-delete-incomplete": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wenum-conversion": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-inaccessible-base": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-ignored-attributes": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-implicit-function-declaration": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-literal-suffix": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-override-init-side-effects": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-if-not-aligned": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-property-assign-default": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-Wc++20-compat": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-implicit-int": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wvolatile": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-format-extra-args": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-analyzer-stale-setjmp-buffer": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-mno-prefixed": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fanalyzer-fine-grained": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wformat-overflow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fanalyzer-state-merge": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-mrori": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-analyzer-possible-null-argument": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fdump-analyzer-exploded-nodes-3": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fdump-analyzer-exploded-nodes": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-lto-type-mismatch": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fanalyzer-verbose-state-changes": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wformat-security": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-attribute-alias": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-analyzer-exposure-through-output-file": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-analyzer-use-after-free": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fdump-analyzer-exploded-nodes-2": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-analyzer-double-fclose": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fdump-analyzer-callgraph": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-vla-larger-than": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mno-pcrel": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gno-as-locview-support": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fno-allocation-dce": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fcallgraph-info": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            },
            "-Wno-analyzer-possible-null-dereference": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-mprefixed": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-switch-bool": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fdump-analyzer-supergraph": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wzero-length-bounds": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wformat-signedness": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mtiny-printf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-analyzer-free-of-non-heap": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-odr": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-analyzer-use-of-uninitialized-value": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-ffinite-loops": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wno-analyzer-tainted-array-index": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-mfix-cmse-cve-2021-35465": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-analyzer-malloc-leak": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wrestrict": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fanalyzer-call-summaries": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-varargs": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-stringop-truncation": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-analyzer-unsafe-call-within-signal-handler": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fdump-analyzer-exploded-graph": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fdump-analyzer-stderr": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-mavx512bf16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fanalyzer-state-purge": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fdump-analyzer-state-purge": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wformat-y2k": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-shift-overflow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fcommon": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-Wformat-nonliteral": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fno-eliminate-unused-debug-symbols": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fanalyzer": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fprofile-partial-training": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Warith-conversion": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fsplit-wide-types-early": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wno-inherited-variadic-ctor": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-gas-locview-support": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mtpf-trace-skip": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fno-diagnostics-show-cwe": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-Wno-switch-unreachable": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-analyzer-double-free": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-hsa": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-pointer-compare": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fallow-store-data-races": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wno-coverage-mismatch": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wformat-truncation": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-virtual-move-assign": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-sizeof-array-argument": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fdump-analyzer": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-invalid-memory-model": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fanalyzer-verbose-edges": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-analyzer-null-dereference": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-address-of-packed-member": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fdiagnostics-show-path-depths": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-Wno-missing-profile": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-menqcmd": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-format-contains-nul": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mxbpf": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wstring-compare": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-placement-new": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-analyzer-use-of-pointer-in-stale-stack-frame": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-munordered-float": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-shift-count-negative": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mmma": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-analyzer-null-argument": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wanalyzer-too-complex": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-mno-tpf-trace-skip": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fanalyzer-transitivity": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-analyzer-file-leak": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-switch-outside-range": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-shift-count-overflow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wformat-extra-args": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-format-overflow": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wlto-type-mismatch": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-format-security": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wattribute-alias": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wvla-larger-than": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wswitch-bool": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-zero-length-bounds": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-format-signedness": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wodr": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-restrict": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wvarargs": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wstringop-truncation": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-format-y2k": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wshift-overflow": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-format-nonliteral": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-arith-conversion": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wswitch-unreachable": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Whsa": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wpointer-compare": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wcoverage-mismatch": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-format-truncation": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wsizeof-array-argument": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Winvalid-memory-model": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Waddress-of-packed-member": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wmissing-profile": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wformat-contains-nul": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-string-compare": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wshift-count-negative": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wswitch-outside-range": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wshift-count-overflow": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wincompatible-pointer-types": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-enum-conversion": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wignored-attributes": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wimplicit-function-declaration": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Woverride-init-side-effects": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wif-not-aligned": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-c++20-compat": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wimplicit-int": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
        },
        "gcc_equal_dict": {
            "-mlong-double": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fprofile-note": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-fcallgraph-info": {
                "category": GCCOptionCategory.DEVELOPER,
                "action": "keep"
            },
            "-mframe-limit": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fanalyzer-checker": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wformat-truncation": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fconstexpr-cache-depth": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mdouble": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mkernel": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fprofile-prefix-path": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-fdiagnostics-urls": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-fprofile-reproducible": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-Wformat-overflow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fanalyzer-verbosity": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fmax-include-depth": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-fdiagnostics-path-format": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-Wno-format-truncation": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-format-overflow": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_match_dict": {}
    }
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-matomic-updates",
            "-merror-reloc",
            "-mbranch-hints",
            "-msafe-dma",
            "-fbtr-bb-exclusive",
            
            "-munsafe-dma",
            "-mlarge-mem",
            "-mno-mfpgpr",
            "-feliminate-unused-debug-symbols",
            "-frepo",
            "-msmall-mem",
            "-mno-atomic-updates",
            "-mea64",
            "-mmfpgpr",
            "-fno-common",
            "-maddress-space-conversion",
            "-mstdmain",
            "-mcldemote",
            "-mea32",
            "-mno-address-space-conversion",
            "-fbranch-target-load-optimize2",
            "-fbranch-target-load-optimize",
            "-mwarn-reloc"
        ],
        "gcc_space_dict": [
        ],
        "gcc_equal_dict": [
            "-mcache-size"
        ],
        "gcc_match_dict": []
        }

    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()

class GCCOptionDict_11_3_0(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_10_4_0(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
            "-Wctad-maybe-unsupported": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
                "-Wno-exceptions": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wrange-loop-construct": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-mismatched-new-delete": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-vexing-parse": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wobjc-root-class": {
                "category": GCCOptionCategory.OBJECTIVE_C,
                "action": "keep"
            },
            "-Wno-deprecated-enum-enum-conversion": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-deprecated-enum-float-conversion": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mprivileged": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mamx-int8": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-analyzer-write-to-const": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-gdwarf64": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-Winvalid-imported-macros": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Mno-modules": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-mno-rop-protect": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-analyzer-mismatching-deallocation": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fno-analyzer-feasibility": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-mno-shorten-memrefs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fmodules-ts": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mamx-bf16": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mkl": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-analyzer-shift-count-overflow": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-gdwarf32": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-mavx512vp2intersect": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-privileged": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wenum-conversion": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fmodule-only": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fprofile-info-section": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-mmwait": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fmodule-header": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fno-module-lazy": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fipa-modref": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Wno-stringop-overread": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fdiagnostics-plain-output": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-flang-info-include-translate": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mindirect-branch-cs-prefix": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fmodule-implicit-inline": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-flang-info-module-cmi": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mno-block-ops-unaligned-vsx": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wtsan": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fno-bit-tests": {
                "category": GCCOptionCategory.CODE_GENERATION,
                "action": "keep"
            },
            "-Wsizeof-array-div": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mshorten-memrefs": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-muintr": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mhreset": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mserialize": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-flang-info-include-translate-not": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fdump-analyzer-json": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-analyzer-shift-count-negative": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fdump-analyzer-feasibility": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-stringop-overflow": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-analyzer-write-to-string-literal": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-flarge-source-files": {
                "category": GCCOptionCategory.PREPROCESSOR,
                "action": "keep"
            },
            "-mrop-protect": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mwidekl": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-enum-conversion": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wstringop-overread": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-tsan": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-sizeof-array-div": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wstringop-overflow": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
            "-dumpdir": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-dumpbase-ext": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
            "-dumpbase": {
                "category": GCCOptionCategory.OVERALL,
                "action": "keep"
            },
        },
        "gcc_equal_dict": {
            "-fdiagnostics-column-origin": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-flang-info-include-translate": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fmodule-mapper": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-fdiagnostics-column-unit": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-fzero-call-used-regs": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fprofile-info-section": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-fmodule-header": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-stdlib": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-flang-info-module-cmi": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-mmax-inline-shift": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            }
        },
        "gcc_match_dict": {}
    }
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-Wstringop-overflow",
            "-mlinux",
            "-Wno-hsa",
            "-mno-gotplt"
        ],
        "gcc_space_dict": [
        ],
        "gcc_equal_dict": [
            "-Wstringop-overflow"
        ],
        "gcc_match_dict": []
        }

    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()

class GCCOptionDict_12_2_0(GCCOPtionDictBase):
    def __init__(self, option_dict=GCCOPtionDictBase()):
        self.option_dict = GCCOptionDict_11_3_0(option_dict)
        self.init_option_dict()
    
    def load_change_options(self):
        self.append_options = {
            "gcc_simple_dict": {
            "-Wno-c++23-extensions": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-c++11-extensions": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-c++14-extensions": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wmismatched-new-delete": {
                "category": GCCOptionCategory.CPP_LANGUAGE,
                "action": "keep"
            },
            "-Wno-c++17-extensions": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-Wno-c++20-extensions": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mcond-move-int": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mfix-cortex-a57-aes-1742098": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fharden-conditional-branches": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-malu32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-analyzer-tainted-allocation-size": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-mfix-cortex-a72-aes-1655431": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fgnu-tm": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-mno-cond-move-float": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mco-re": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fhosted": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-mno-unaligned-access": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mcond-move-float": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mno-cond-move-int": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-gctf": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-fno-analyzer-state-purge": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wno-analyzer-tainted-size": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-mrelax-cmpxchg-loop": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mjmp32": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wdangling-pointer": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fno-analyzer-state-merge": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wopenacc-parallelism": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fdump-analyzer-exploded-paths": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Wtrivial-auto-var-init": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fmove-loop-stores": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-fdump-analyzer-untracked": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-mcldemote": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-Wno-analyzer-tainted-offset": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-Oz": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-Warray-compare": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fgimple": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-Winfinite-recursion": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-fharden-compares": {
                "category": GCCOptionCategory.INSTRUMENTATION,
                "action": "keep"
            },
            "-Wno-analyzer-tainted-divisor": {
                "category": GCCOptionCategory.ANALYZER,
                "action": "keep"
            },
            "-fipa-strict-aliasing": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-gbtf": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            },
            "-Wno-dangling-pointer": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-openacc-parallelism": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-trivial-auto-var-init": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-array-compare": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-infinite-recursion": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wcpp": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wc++23-extensions": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wc++11-extensions": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wc++14-extensions": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wc++17-extensions": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wc++20-extensions": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_space_dict": {
        },
        "gcc_equal_dict": {
            "-misa-spec": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-ftrivial-auto-var-init": {
                "category": GCCOptionCategory.OPTIMIZATION,
                "action": "keep"
            },
            "-mstore-max": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-mmove-max": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-foffload-options": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-Wbidi-chars": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-foffload": {
                "category": GCCOptionCategory.C_LANGUAGE,
                "action": "keep"
            },
            "-Wdangling-pointer": {
                "category": GCCOptionCategory.WARNING,
                "action": "keep"
            },
            "-mmax-inline-memcpy-size": {
                "category": GCCOptionCategory.MACHINE_DEPENDENT,
                "action": "keep"
            },
            "-fdiagnostics-escape-format": {
                "category": GCCOptionCategory.DIAGNOSTIC,
                "action": "keep"
            },
            "-Wno-bidi-chars": {
            "category": "Warning Options",
            "action": "keep"
        },
        "-Wno-dangling-pointer": {
            "category": "Warning Options",
            "action": "keep"
        }
        },
        "gcc_match_dict": {
            "-gctf": {
                "category": GCCOptionCategory.DEBUGGING,
                "action": "keep"
            }
        }
    }
        
        self.discard_options = {
            "gcc_simple_dict": [
            "-fanalyzer-state-merge",
            "-fanalyzer-state-purge",
            "-mthread"
        ],
        "gcc_space_dict": [
        ],
        "gcc_equal_dict": [],
        "gcc_match_dict": []
        }
        
    def get_simple_dict(self):
        return self.option_dict.get_simple_dict()

    def get_space_dict(self):
        return self.option_dict.get_space_dict()
    
    def get_equal_dict(self):
        return self.option_dict.get_equal_dict()
    
    def get_match_dict(self):
        return self.option_dict.get_match_dict()