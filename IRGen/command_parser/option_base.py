OPTION_PARAMS = 0x00FF

OPTION_PREFIX = 0xFF00

PARAMS_ONE = 0x0000   # ''
PARAMS_SPACE = 0x0001  # ' '
PARAMS_COLON = 0x0002  # ':'
PARAMS_COMMA = 0x0004  # ','
PARAMS_SCOLO = 0x0008  # ';'

PREFIX_SIMPLE = 0x0100  # ''
PREFIX_SPACE = 0x0200   # ' '
PREFIX_EQUAL = 0x0400   # '='
PREFIX_MATCH = 0x0800   # '*'

class Option(object):
    
    def __init__(self, option="", parameter=[], type=PREFIX_SIMPLE, category=-1, keep=True):
        
        self.option = option
        self.parameter = parameter
        self.type = type
        self.category = category
        self.keep = keep
        
    def get_option(self):
        
        param_type = self.type & OPTION_PARAMS
        
        if param_type == PARAMS_ONE:
            params = [''.join(self.parameter)]
        elif param_type == PARAMS_SPACE:
            params = [' '.join(self.parameter)]
        elif param_type == PARAMS_COLON:
            params = [':'.join(self.parameter)]
        elif param_type == PARAMS_COMMA:
            params = [','.join(self.parameter)]
        elif param_type == PARAMS_SCOLO:
            params = [';'.join(self.parameter)]
        else:
            params = []
        
        prefix_type = self.type & OPTION_PREFIX
        
        if prefix_type == PREFIX_SIMPLE:
            return [self.option]
        elif prefix_type == PREFIX_SPACE:
            return [self.option, params[0]]
        elif prefix_type == PREFIX_EQUAL:
            return ['='.join([self.option, params[0]])]
        elif prefix_type == PREFIX_MATCH:
            return [''.join([self.option, params[0]])]
        else:
            return [self.option]