import copy

from command_parser.command_generate import GenCommand
from command_parser.ranlib_parse.ranlib_option_filter import RanlibFilter
from command_parser.option_context import OptionContext

class RanlibGenCommand(GenCommand):
    
    def __init__(self, context=None, cwd=""):
        super().__init__(context, cwd)
        self.option_context = OptionContext()
        self.command_filter = RanlibFilter(context=self.context)