class FilterBase(object):
    
    def filter_func_callback(self, cwd='', compiler='', options=[], func_dict={}):

        for item in options:
            if not item.option:
                # Maybe cannot recognize or it's an input file.
                continue
            func = func_dict.get(item.option)
            if func is None:
                continue
            func(cwd, compiler, options, item)

    
    def generate_filter_context_callback(self, options=[], gen_dict={}):
        
        if not options or not gen_dict:
            return

        for item in options:
            func = gen_dict.get(item.option)
            if func is None:
                continue
            func(item.parameter)
            
    def filter_func_remove(self, options=[], remove_list=[]):
        if not options or not remove_list:
            return

        new_option = []
        remove_index = 0
        remove_size = len(remove_list)
        for item in options:
            if (remove_size > remove_index) \
                    and remove_list[remove_index] == item:
                remove_index += 1
            else:
                new_option.append(item)
        options[:] = new_option
        
    def filter_option_removal(self, cwd='', compiler='', options=[], option=None):
        if not option:
            raise Exception("filter option default input parameters error")

        option.keep = False
    
    def filter_option_default(self, cwd='', compiler='', options=[], option=None):
        
        filter_options = []
        
        for opt in options:
            if not opt.keep:
                filter_options.append(opt)
        
        self.remove_options_by_list(options=options, remove_list=filter_options)
        
        
    
    def remove_options_by_list(self, options=[], remove_list=[]):
        
        new_options = []
        
        for option in options:
            if option not in remove_list:
                new_options.append(option)
        
        options = new_options