import copy

from command_parser.command_generate import GenCommand
from command_parser.strip_parse.strip_option_filter import StripFilter
from command_parser.option_context import OptionContext

class StripGenCommand(GenCommand):
    def __init__(self, context=None, cwd=""):
        super().__init__(context=context, cwd=cwd)
        self.option_context = OptionContext()
        self.command_filter = StripFilter(context=self.context)
    