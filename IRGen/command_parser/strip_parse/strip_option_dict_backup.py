# -*- coding:utf-8 -*-

strip_option_index = 300


class Strip_OptionCategory(object):
    OVERALL = 0 + strip_option_index
    EXE_FILE = 1 + strip_option_index
    UNKNOWN_OPTION = 2 + strip_option_index

strip_category_list = [
    "Overall Options",
    "Executable File"
]




strip_simple_dict = {
    "--disable-deterministic-archives": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--discard-all": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--discard-locals": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--enable-deterministic-archives": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--help": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--info": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--keep-file-symbols": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--only-keep-debug": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--preserve-dates": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--strip-all": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--strip-debug": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--strip-dwo": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--strip-unneeded": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--verbose": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--version": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--wildcard": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-D": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-S": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-U": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-V": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-X": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-d": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-g": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-h": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-p": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-s": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-v": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-w": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-x": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
}



strip_space_dict = {
    "-F": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-I": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-K": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-N": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-O": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-R": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-o": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
}



strip_equal_dict = {
    "--input-target": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--keep-symbol": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--output-target": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--remove-section": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--strip-symbol": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "--target": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-F": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-I": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-K": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-N": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-O": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-R": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
}



strip_match_dict = {
    "-F": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-I": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-K": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-N": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-O": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-R": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
    "-o": {
        "action": "keep",
        "category": Strip_OptionCategory.OVERALL,
        "support": True
    },
}
