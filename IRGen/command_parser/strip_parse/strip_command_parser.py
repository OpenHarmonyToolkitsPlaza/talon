import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir))

from command_parser.command_parser_container import CmdParserContainer
from command_parser.strip_parse.strip_command_generator import StripGenCommand
from configure.compiler_configure import *
from utils.common_utils import *
from command_parser.option_parser import *
from command_parser.strip_parse.strip_option_dict import *

class StripParser(CmdParserContainer):
    
    def __init__(self, command=[], context=None, compiler="", options=[], cwd=""):
        self.command = command
        self.context = context
        self.compiler = compiler
        self.options = options
        self.cwd = cwd
        self.cmdgen =  StripGenCommand(context=context, cwd=self.cwd)
        self.option_dict = None
        
    def parse_command(self, command=[]):
        
        if not command:
            command = self.command
        
        self.compiler = command[0]
        options = command[1:]
        self.option_dict = OptionDict(option_dict_xml)
        self.options = self.build_options(parse_at_option(options))
        self.compiler = real_binary_path(self.compiler)
        return self.compiler, self.options
    
    
    def build_options(self, raw_options=[]):
        cmd = []
        i = 0
        while i < len(raw_options):
            raw_opt = raw_options[i]
            if raw_opt[0] != '-':
                option = Option('', [raw_opt], PREFIX_MATCH, OptionCategory.EXE)
                
            elif not raw_opt.startswith('--') and len(raw_opt) > 2:
                raw_options[0:1] = ['-' + ch for ch in raw_opt[1:]]
                continue
            else:
                option = get_simple_option(raw_opt, self.option_dict.get_simple_dict())
                if not option:
                    option = get_space_option(raw_opt, raw_options[i+1] if i+1 < len(raw_options) else '', self.option_dict.get_space_dict())
                    if option:
                        i += 1
                    else:
                        option = get_equal_option(raw_opt, self.option_dict.get_equal_dict())
                        if not option:
                            option = get_match_option(raw_opt, self.option_dict.get_match_dict())
                            if not option:
                                option = get_unknown_option(raw_opt, raw_options[i+1] if i+1 < len(raw_options) else '',OptionCategory=OptionCategory)
                                if option.type == PREFIX_SPACE:
                                    i += 1
                                logging.warning("unknown strip option" + str(vars(option)) + "Command:" + ' '.join(raw_options))
            i += 1
            cmd.append(option)
        return cmd

    def get_input_options(self):
        
        options = [option for option in self.options if not option.option]
        
        return options
    
    def get_output_options(self):
        
        options = [option for option in self.options if not option or option.option == '-o']
        
        out_options = []
        try:
            for option in options.reverse():
                if option.option == "-o":
                    out_options.append(option)
                    break
        except Exception as e:
            sys.exit()         
        out_options = out_options if out_options else options
        
        return out_options
    
    def get_original_command(self, compiler="", options=[]):
        return self.cmdgen.generate_original_command(compiler=self.compiler, options=self.options)