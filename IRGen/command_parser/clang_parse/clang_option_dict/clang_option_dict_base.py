clang_option_index = 400


class ClangOptionCategory(object):
    OVERALL = 0 + clang_option_index
    COMPILE = 1 + clang_option_index
    WARNING = 2 + clang_option_index
    DEBUGGING = 3 + clang_option_index
    OPTIMIZATION = 4 + clang_option_index
    LINKER = 5 + clang_option_index
    FORTRAN = 6 + clang_option_index
    NONETIME = 7 + clang_option_index
    UNKNOWN = 8 + clang_option_index

# clang 12 option dict
class ClangOptionDictBase():
    clang_simple_dict = {
        "-###": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-all_load": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-analyzer-checker-help-alpha": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-checker-help-developer": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-checker-help": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-checker-option-help-alpha": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-checker-option-help-developer": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-checker-option-help": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-config-help": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-disable-all-checks": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-disable-retry-exhausted": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-display-progress": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-list-enabled-checkers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--analyzer-no-default-checks": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-analyzer-opt-analyze-headers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-opt-analyze-nested-blocks": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--analyzer-output": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-analyzer-stats": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-viz-egraph-graphviz": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-werror": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--analyze": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-analyze": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ansi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--ansi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-arch_errors_fatal": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-arcmt-migrate-emit-errors": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ast-dump-all": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ast-dump-decl-types": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ast-dump-lookups": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ast-dump": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ast-list": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ast-print": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ast-view": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-A": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-bind_at_load": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-building-pch-with-obj": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-bundle": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-B": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-b": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-c-isystem": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-cc1as": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-cc1": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ccc-arcmt-check": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ccc-arcmt-modify": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ccc-print-bindings": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ccc-print-phases": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-CC": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-cfg-add-implicit-dtors": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-cfguard-no-checks": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-cfguard": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-cl-denorms-are-zero": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-cl-fast-relaxed-math": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-cl-finite-math-only": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-cl-fp32-correctly-rounded-divide-sqrt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-cl-kernel-arg-info": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-cl-mad-enable": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-cl-no-signed-zeros": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-cl-opt-disable": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-cl-single-precision-constant": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-cl-strict-aliasing": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-cl-uniform-work-group-size": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-cl-unsafe-math-optimizations": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-client_name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-code-completion-brief-comments": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-code-completion-macros": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-code-completion-patterns": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-code-completion-with-fixits": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-combine": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--combine": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-compatibility_version": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-compiler-options-dump": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-compress-debug-sections": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--compress-debug-sections": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--constant-cfstrings": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-coverage": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--coverage": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-cpp-precomp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-cpp": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "--cuda-compile-host-device": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--cuda-device-only": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--cuda-host-only": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--cuda-noopt-device-debug": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--cuda-path-ignore-env": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-current_version": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-cxx-isystem": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-C": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-c": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-dD": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-dead_strip": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-debug-forward-template-params": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-debug-info-macro": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--debug": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-detailed-preprocessing-record": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-disable-free": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-disable-lifetime-markers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-disable-llvm-optzns": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-disable-llvm-passes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-disable-llvm-verifier": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-disable-O0-optnone": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-disable-objc-default-synthesize-properties": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-disable-pragma-debug-crash": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-disable-red-zone": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-discard-value-names": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-dI": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-dM": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-dsym-dir": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-dump-coverage-mapping": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-dump-deserialized-decls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-dump-raw-tokens": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-dump-tokens": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-dumpmachine": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-dumpspecs": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-dumpversion": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-dwarf-explicit-import": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-dwarf-ext-refs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-dylinker_install_name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-dylinker": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-dynamiclib": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-dynamic": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-D": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-d": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-EB": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-EL": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-emit-ast": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-codegen-only": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-header-module": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-html": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-interface-stubs": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-llvm-bc": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-llvm-only": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-llvm-uselists": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-emit-llvm": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-merged-ifs": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-module-interface": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-module": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-obj": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-emit-pch": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--emit-static-lib": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-enable-trivial-auto-var-init-zero-knowing-it-will-be-removed-from-clang": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Eonly": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--extra-warnings": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-E": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-e": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-faapcs-bitfield-load": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-faapcs-bitfield-width": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-faccess-control": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-faddrsig": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-faggressive-function-elimination": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-falign-commons": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-falign-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-falign-jumps": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-falign-labels": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-falign-loops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-faligned-alloc-unavailable": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-faligned-allocation": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-faligned-new": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fall-intrinsics": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fallow-editor-placeholders": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fallow-half-arguments-and-returns": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fallow-pch-with-compiler-errors": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fallow-pcm-with-compiler-errors": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fallow-unsupported": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-faltivec": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fansi-escape-codes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fapple-kext": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fapple-link-rtlib": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fapple-pragma-pack": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fapplication-extension": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fapply-global-visibility-to-externs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fapprox-func": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fasm-blocks": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fasm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fassociative-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fassume-sane-operator-new": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fastcp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fastf": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fast": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fasynchronous-unwind-tables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fauto-profile-accurate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fauto-profile": {
            "category": ClangOptionCategory.OPTIMIZATION,
            "action": "keep"
        },
        "-fautolink": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fautomatic": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fbackslash": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fbacktrace": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fblocks-runtime-optional": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fblocks": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fborland-extensions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fbounds-check": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fbranch-count-reg": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fbuiltin-module-map": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fbuiltin": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fc++-static-destructors": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcall-saved-x10": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fcall-saved-x11": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fcall-saved-x12": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fcall-saved-x13": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fcall-saved-x14": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fcall-saved-x15": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fcall-saved-x18": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fcall-saved-x8": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fcall-saved-x9": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fcaller-saves": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fcaret-diagnostics": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcf-protection": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fchar8_t": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcheck-array-temporaries": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fcheck-new": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcolor-diagnostics": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcommon": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcompatibility-qualified-id-block-type-checking": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcomplete-member-pointers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconcepts-ts": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconst-strings": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconstant-cfstrings": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconvergent-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcoroutines-ts": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcoverage-mapping": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcray-pointer": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fcreate-profile": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcs-profile-generate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcuda-allow-variadic-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcuda-approx-transcendentals": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcuda-flush-denormals-to-zero": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fcuda-is-device": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcuda-short-ptr": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcxx-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcxx-modules": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fd-lines-as-code": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fd-lines-as-comments": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fdata-sections": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebug-info-for-profiling": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebug-macro": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebug-pass-arguments": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebug-pass-manager": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebug-pass-structure": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebug-ranges-base-address": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebug-types-section": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebugger-cast-result-to-id": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebugger-objc-literal": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebugger-support": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdeclare-opencl-builtins": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdeclspec": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdefault-double-8": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fdefault-inline": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fdefault-integer-8": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fdefault-real-8": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fdefer-pop": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fdelayed-template-parsing": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdelete-null-pointer-checks": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdeprecated-macro": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdevirtualize-speculatively": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fdevirtualize": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fdiagnostics-absolute-paths": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-color": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-fixit-info": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-parseable-fixits": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-print-source-range-info": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-show-hotness": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-show-note-include-stack": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-show-option": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-show-template-tree": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdigraphs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdirect-access-external-data": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdisable-module-hash": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiscard-value-names": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdollar-ok": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fdollars-in-identifiers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdouble-square-bracket-attributes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdump-fortran-optimized": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fdump-fortran-original": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fdump-parse-tree": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fdump-record-layouts-simple": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdump-record-layouts": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdump-vtable-layouts": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdwarf-directory-asm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdwarf-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdwarf2-cfi-asm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-felide-constructors": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-feliminate-unused-debug-symbols": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-feliminate-unused-debug-types": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fembed-bitcode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-femit-all-decls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-femulated-tls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fenable-matrix": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fencode-extended-block-signature": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fescaping-block-tail-calls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fexceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fexpensive-optimizations": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fexperimental-debug-variable-locations": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fexperimental-isel": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fexperimental-new-constant-interpreter": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fexperimental-new-pass-manager": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fexperimental-relative-c++-abi-vtables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fexperimental-strict-floating-point": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fextended-identifiers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fexternal-blas": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fexternc-nounwind": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ff2c": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-ffake-address-space-map": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffast-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffat-lto-objects": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ffine-grained-bitfield-accesses": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffinite-loops": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffinite-math-only": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffixed-form": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-ffixed-point": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffixed-r19": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ffixed-r9": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x10": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x11": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x12": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x13": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x14": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x15": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x16": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x17": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x18": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x19": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x1": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x20": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x21": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x22": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x23": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x24": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x25": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x26": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x27": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x28": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x29": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x30": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x31": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x3": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x4": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x5": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x6": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x7": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x8": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffixed-x9": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ffloat-store": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ffor-scope": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fforbid-guard-variables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fforce-dwarf-frame": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fforce-emit-vtables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fforce-enable-int128": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffree-form": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-ffreestanding": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffriend-injection": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffrontend-optimize": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-ffunction-attribute-list": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffunction-sections": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fgcse-after-reload": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fgcse-las": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fgcse-sm": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fgcse": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fglobal-isel": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fgnu-inline-asm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fgnu-keywords": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fgnu-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fgnu89-inline": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fgnu": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fgpu-allow-device-init": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fgpu-defer-diag": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fgpu-exclude-wrong-side-overloads": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fgpu-rdc": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fhalf-no-semantic-interposition": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fheinous-gnu-extensions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fhip-dump-offload-linker-script": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fhip-new-launch-api": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fhonor-infinites": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fhonor-infinities": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fhonor-nans": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fhosted": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fident": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fignore-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fimplement-inlines": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fimplicit-module-maps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fimplicit-modules": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fimplicit-none": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fimplicit-templates": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-finclude-default-header": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-findirect-virtual-calls": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-finit-local-zero": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-finline-functions-called-once": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-finline-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-finline-hint-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-finline-limit": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-finline-small-functions": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-finline": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-finstrument-function-entry-bare": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-finstrument-functions-after-inlining": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-finstrument-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-finteger-4-integer-8": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fintegrated-as": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fintegrated-cc1": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fintrinsic-modules-path": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fipa-cp": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fivopts": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fix-only-warnings": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fix-what-you-can": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fixit-recompile": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fixit-to-temporary": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fixit": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fjump-tables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fkeep-inline-functions": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fkeep-static-consts": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-flat_namespace": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-flax-vector-conversions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-flegacy-pass-manager": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-flimit-debug-info": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-flto-unit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-flto-visibility-public-std": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-flto": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmath-errno": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmax-identifier-length": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fmemory-profile": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmerge-all-constants": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmerge-constants": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fmerge-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodule-file-deps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodule-map-file-home-is-cwd": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodule-private": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fmodules-codegen": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-debuginfo": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-decluse": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-disable-diagnostic-validation": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fmodules-hash-content": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-local-submodule-visibility": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-search-all": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-strict-context-hash": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-strict-decluse": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-ts": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-validate-input-files-content": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-validate-once-per-build-session": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fmodules-validate-system-headers": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fmodules": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodulo-sched-allow-regmoves": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fmodulo-sched": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fms-compatibility": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fms-extensions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fms-volatile": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmudflapth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmudflap": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fnative-half-arguments-and-returns": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fnative-half-type": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fnested-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fnext-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-aapcs-bitfield-width": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fno-access-control": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-addrsig": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-aggressive-function-elimination": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-align-commons": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-align-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-align-jumps": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-align-labels": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-align-loops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-aligned-allocation": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-all-intrinsics": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-allow-editor-placeholders": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-altivec": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-apple-pragma-pack": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-application-extension": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-asm-blocks": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-asm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-associative-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-assume-sane-operator-new": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-asynchronous-unwind-tables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-auto-profile-accurate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-auto-profile": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-autolink": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-automatic": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-backslash": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-backtrace": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-bitfield-type-align": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-blocks": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-borland-extensions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-bounds-check": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-branch-count-reg": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-builtin": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-c++-static-destructors": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-caller-saves": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-caret-diagnostics": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-char8_t": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-check-array-temporaries": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-check-new": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-color-diagnostics": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-common": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-complete-member-pointers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-concept-satisfaction-caching": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-const-strings": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-constant-cfstrings": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-coroutines-ts": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-coverage-mapping": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-crash-diagnostics": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-cray-pointer": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-cuda-approx-transcendentals": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-cuda-flush-denormals-to-zero": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-cuda-host-device-constexpr": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-cuda-short-ptr": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-cxx-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-cxx-modules": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-d-lines-as-code": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-d-lines-as-comments": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-data-sections": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-debug-info-for-profiling": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-debug-macro": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-debug-pass-manager": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-debug-ranges-base-address": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-debug-types-section": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-declspec": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-default-double-8": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-default-inline": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-default-integer-8": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-default-real-8": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-defer-pop": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-delayed-template-parsing": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-delete-null-pointer-checks": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-deprecated-macro": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-devirtualize-speculatively": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-devirtualize": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-diagnostics-color": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-diagnostics-fixit-info": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-diagnostics-show-hotness": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-diagnostics-show-note-include-stack": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-diagnostics-show-option": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-diagnostics-use-presumed-location": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-digraphs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-direct-access-external-data": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-discard-value-names": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-dllexport-inlines": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-dollar-ok": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-dollars-in-identifiers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-double-square-bracket-attributes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-dump-fortran-optimized": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-dump-fortran-original": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-dump-parse-tree": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-dwarf-directory-asm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-dwarf2-cfi-asm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-elide-constructors": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-elide-type": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-eliminate-unused-debug-symbols": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-eliminate-unused-debug-types": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-emulated-tls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-escaping-block-tail-calls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-expensive-optimizations": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-experimental-isel": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-experimental-new-pass-manager": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-experimental-relative-c++-abi-vtables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-extended-identifiers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-external-blas": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-f2c": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-fast-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-fat-lto-objects": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-fine-grained-bitfield-accesses": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-finite-loops": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-finite-math-only": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-fixed-form": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-fixed-point": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-float-store": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-for-scope": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-force-dwarf-frame": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-force-emit-vtables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-force-enable-int128": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-free-form": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-friend-injection": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-frontend-optimize": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-function-attribute-list": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-function-sections": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-gcse-after-reload": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-gcse-las": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-gcse-sm": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-gcse": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-global-isel": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-gnu-inline-asm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-gnu-keywords": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-gnu89-inline": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-gnu": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-gpu-allow-device-init": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-gpu-defer-diag": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-gpu-exclude-wrong-side-overloads": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-gpu-rdc": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-hip-new-launch-api": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-honor-infinites": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fno-honor-infinities": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-honor-nans": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-ident": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-implement-inlines": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-implicit-module-maps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-implicit-modules": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-implicit-none": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-implicit-templates": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-init-local-zero": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-inline-functions-called-once": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-inline-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-inline-limit": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-inline-small-functions": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-inline": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-integer-4-integer-8": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-integrated-as": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-integrated-cc1": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-intrinsic-modules-path": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-ipa-cp": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-ivopts": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-jump-tables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-keep-inline-functions": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-keep-static-consts": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-lax-vector-conversions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-legacy-pass-manager": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-limit-debug-info": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-fno-lto-unit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-lto": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-math-builtin": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-math-errno": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-max-identifier-length": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-max-type-align": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-memory-profile": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-merge-all-constants": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-merge-constants": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-module-file-deps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-module-private": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-modules-decluse": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-modules-error-recovery": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-modules-global-index": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-modules-search-all": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-modules-validate-system-headers": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fno-modules": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-modulo-sched-allow-regmoves": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-modulo-sched": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-ms-compatibility": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-ms-extensions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-non-call-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-objc-arc-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-objc-arc": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-objc-convert-messages-to-runtime-calls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-objc-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-objc-infer-related-result-type": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-objc-legacy-dispatch": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-objc-nonfragile-abi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-objc-weak": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-omit-frame-pointer": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-openmp-cuda-force-full-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-openmp-cuda-mode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-openmp-cuda-parallel-target-regions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-openmp-optimistic-collapse": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-openmp-simd": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-openmp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-operator-names": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-optimize-sibling-calls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-pack-derived": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-pack-struct": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-padding-on-unsigned-fixed-point": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-pascal-strings": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-pch-codegen": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-pch-debuginfo": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-pch-instantiate-templates": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-pch-timestamp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-peel-loops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-permissive": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-PIC": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-pic": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-PIE": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-pie": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-plt": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-prebuilt-implicit-modules": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-prefetch-loop-arrays": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-preserve-as-comments": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-printf": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-profile-arcs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-profile-correction": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-profile-generate-sampling": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-profile-generate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-profile-instr-generate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-profile-instr-use": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-profile-reusedist": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-profile-sample-accurate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-profile-sample-use": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-profile-values": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-profile": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-protect-parens": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-pseudo-probe-for-profiling": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-range-check": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-real-4-real-10": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-real-4-real-16": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-real-4-real-8": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-real-8-real-10": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-real-8-real-16": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-real-8-real-4": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-realloc-lhs": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-reciprocal-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-record-command-line": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-recovery-ast-type": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-recovery-ast": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-recursive": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-register-global-dtors-with-atexit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-regs-graph": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-relaxed-template-template-args": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-rename-registers": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-reorder-blocks": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-repack-arrays": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-reroll-loops": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-rewrite-imports": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-rewrite-includes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-ripa": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-ropi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-rounding-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-rtlib-add-rpath": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-rtti-data": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-rtti": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-rwpi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-address-poison-custom-array-cookie": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-address-use-after-scope": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-address-use-odr-indicator": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-blacklist": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-cfi-canonical-jump-tables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-cfi-cross-dso": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-link-c++-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-link-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-memory-track-origins": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-memory-use-after-dtor": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-minimal-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-recover": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-stats": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-thread-atomics": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-thread-func-entry-exit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-thread-memory-access": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-trap": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-undefined-trap-on-error": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-save-optimization-record": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-schedule-insns2": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-schedule-insns": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-second-underscore": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-see": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-semantic-interposition": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-short-enums": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-short-wchar": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-show-column": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-show-source-location": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sign-zero": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-signaling-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-signaling-nans": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-signed-char": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-signed-wchar": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-signed-zeros": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-single-precision-constant": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-sized-deallocation": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-slp-vectorize-aggressive": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-slp-vectorize": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-spec-constr-count": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-spell-checking": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-split-dwarf-inlining": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-split-lto-unit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-split-machine-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-stack-arrays": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-stack-check": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-stack-clash-protection": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-stack-protector": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-stack-size-section": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-standalone-debug": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-strength-reduce": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-strict-aliasing": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-strict-enums": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-strict-float-cast-overflow": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-strict-modules-decluse": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-strict-overflow": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-strict-return": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-strict-vtable-pointers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-struct-path-tbaa": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sycl": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fno-temp-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-test-coverage": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-threadsafe-statics": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-tls-model": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-tracer": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-trapping-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-tree-dce": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-tree-salias": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-tree-slp-vectorize": {
            "category": ClangOptionCategory.OPTIMIZATION,
            "action": "keep"
        },
        "-fno-tree-ter": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-tree-vectorizer-verbose": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-tree-vectorize": {
            "category": ClangOptionCategory.OPTIMIZATION,
            "action": "keep"
        },
        "-fno-tree-vrp": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-trigraphs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-underscoring": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-unique-basic-block-section-names": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-unique-internal-linkage-names": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-unique-section-names": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-unit-at-a-time": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-unroll-all-loops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-unroll-loops": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-unsafe-loop-optimizations": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-unsafe-math-optimizations": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-unsigned-char": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-unswitch-loops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-unwind-tables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-use-cxa-atexit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-use-init-array": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-use-line-directives": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-use-linker-plugin": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-validate-pch": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-var-tracking": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-variable-expansion-in-unroller": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-vect-cost-model": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-vectorize": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-verbose-asm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-virtual-function-elimination": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-visibility-from-dllstorageclass": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-visibility-inlines-hidden-static-local-var": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-wchar": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-web": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-whole-file": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fno-whole-program-vtables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-whole-program": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fno-working-directory": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-wrapv": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-xl-pragma-pack": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-xray-always-emit-customevents": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-xray-always-emit-typedevents": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-xray-function-index": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-xray-ignore-loops": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-xray-instrument": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-zero-initialized-in-bss": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-zvector": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno_modules-validate-input-files-content": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno_pch-validate-input-files-content": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fnon-call-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fnoopenmp-relocatable-target": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fnoopenmp-use-tls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fnoxray-link-deps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-arc-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-arc": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-atdefs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-call-cxx-cdtors": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-convert-messages-to-runtime-calls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-gc-only": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-gc": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-infer-related-result-type": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-legacy-dispatch": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-link-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-new-property": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-nonfragile-abi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-runtime-has-weak": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-sender-dependent-dispatch": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-subscripting-legacy-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-weak": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fomit-frame-pointer": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-cuda-force-full-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-cuda-mode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-cuda-parallel-target-regions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-enable-irbuilder": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-is-device": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-optimistic-collapse": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-relocatable-target": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-simd": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-use-tls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-foptimize-sibling-calls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-force_cpusubtype_ALL": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-force_flat_namespace": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-forder-file-instrumentation": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpack-derived": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fpack-struct": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpadding-on-unsigned-fixed-point": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fparse-all-comments": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpascal-strings": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpass-by-value-is-noalias": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpcc-struct-return": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpch-codegen": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpch-debuginfo": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpch-instantiate-templates": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpch-preprocess": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpch-validate-input-files-content": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpeel-loops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fpermissive": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fPIC": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpic": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fPIE": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpie": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fplt": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprebuilt-implicit-modules": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprefetch-loop-arrays": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fpreserve-as-comments": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpreserve-vec3-type": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprintf": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-arcs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-correction": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fprofile-generate-sampling": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-generate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-instr-generate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-instr-use": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-reusedist": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-sample-accurate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-sample-use": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-use": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-values": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fprofile": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprotect-parens": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fpseudo-probe-for-profiling": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frange-check": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-freal-4-real-10": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-freal-4-real-16": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-freal-4-real-8": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-freal-8-real-10": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-freal-8-real-16": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-freal-8-real-4": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-frealloc-lhs": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-freciprocal-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frecord-command-line": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frecovery-ast-type": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frecovery-ast": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frecursive": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-freg-struct-return": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fregister-global-dtors-with-atexit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fregs-graph": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frelaxed-template-template-args": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frename-registers": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-freorder-blocks": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-frepack-arrays": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-freroll-loops": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fretain-comments-from-system-headers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frewrite-imports": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frewrite-includes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fripa": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fropi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frounding-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frtlib-add-rpath": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-frtti-data": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frtti": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frwpi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-address-globals-dead-stripping": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-address-poison-custom-array-cookie": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-address-use-after-scope": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-address-use-odr-indicator": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-cfi-canonical-jump-tables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-cfi-cross-dso": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-cfi-icall-generalize-pointers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-8bit-counters": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-indirect-calls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-inline-8bit-counters": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-inline-bool-flag": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-no-prune": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-pc-table": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-stack-depth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-trace-bb": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-trace-cmp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-trace-div": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-trace-gep": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-trace-pc-guard": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-trace-pc": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-link-c++-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-link-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-memory-track-origins": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-memory-use-after-dtor": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-minimal-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-recover": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-stats": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-thread-atomics": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-thread-func-entry-exit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-thread-memory-access": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-trap": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-undefined-trap-on-error": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsave-optimization-record": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsched-interblock": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fschedule-insns2": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fschedule-insns": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fsecond-underscore": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fsee": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fseh-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsemantic-interposition": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fshort-enums": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fshort-wchar": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fshow-column": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fshow-source-location": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsign-zero": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fsignaling-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsignaling-nans": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fsigned-bitfields": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsigned-char": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsigned-wchar": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsigned-zeros": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsingle-precision-constant": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fsized-deallocation": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsjlj-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fslp-vectorize-aggressive": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fslp-vectorize": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fspec-constr-count": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fspell-checking": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsplit-dwarf-inlining": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsplit-lto-unit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsplit-machine-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsplit-stack": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstack-arrays": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fstack-check": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstack-clash-protection": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstack-protector-all": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstack-protector-strong": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstack-protector": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstack-size-section": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstandalone-debug": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstrength-reduce": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fstrict-aliasing": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstrict-enums": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstrict-float-cast-overflow": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstrict-overflow": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstrict-return": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstrict-vtable-pointers": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fstruct-path-tbaa": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsycl-is-device": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsycl": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fsyntax-only": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fsystem-module": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fterminated-vtables": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ftest-coverage": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fthreadsafe-statics": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftime-report": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftime-trace": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftls-model": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftracer": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ftrapping-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftrapv": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftree-dce": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ftree-salias": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftree-slp-vectorize": {
            "category": ClangOptionCategory.OPTIMIZATION,
            "action": "keep"
        },
        "-ftree-ter": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ftree-vectorizer-verbose": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftree-vrp": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ftrigraphs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-funderscoring": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-funique-basic-block-section-names": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-funique-internal-linkage-names": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-funique-section-names": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-funit-at-a-time": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-funknown-anytype": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-funroll-all-loops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-funroll-loops": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-funsafe-loop-optimizations": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-funsafe-math-optimizations": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-funsigned-bitfields": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-funsigned-char": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-funswitch-loops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-funwind-tables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fuse-ctor-homing": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fuse-cxa-atexit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fuse-init-array": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fuse-line-directives": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fuse-linker-plugin": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fuse-register-sized-bitfield-access": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvalidate-ast-input-files-content": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvariable-expansion-in-unroller": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fvect-cost-model": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fvectorize": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fverbose-asm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvirtual-function-elimination": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvisibility-from-dllstorageclass": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvisibility-global-new-delete-hidden": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvisibility-inlines-hidden-static-local-var": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvisibility-inlines-hidden": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvisibility-ms-compat": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fwasm-exceptions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fweb": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fwhole-file": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fwhole-program-vtables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fwhole-program": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fwrapv": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fwritable-strings": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxl-pragma-pack": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-always-emit-customevents": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-always-emit-typedevents": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-always-instrument": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-attr-list": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-function-index": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-ignore-loops": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-instruction-threshold": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-instrumentation-bundle": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-instrument": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-link-deps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-modes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-never-instrument": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fzero-initialized-in-bss": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fzvector": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-F": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-g0": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-g1": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-g2": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-g3": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gcodeview-ghash": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-gcodeview": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-gcolumn-info": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gdwarf-2": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gdwarf-3": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gdwarf-4": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gdwarf-5": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gdwarf-aranges": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gdwarf32": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gdwarf64": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gdwarf": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gembed-source": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gen-reproducer": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-gfull": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-ggdb0": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-ggdb1": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-ggdb2": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-ggdb3": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-ggdb": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-ggnu-pubnames": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-ginline-line-tables": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gline-directives-only": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gline-tables-only": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-glldb": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gmlt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-gmodules": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gno-codeview-ghash": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-gno-column-info": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gno-embed-source": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gno-gnu-pubnames": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gno-inline-line-tables": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gno-pubnames": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gno-record-command-line": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gno-split-dwarf": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gno-strict-dwarf": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "--gpu-use-aux-triple-only": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-gpubnames": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-grecord-command-line": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gsce": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gsplit-dwarf": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gstrict-dwarf": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gtoggle": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gused": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gz": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-G": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-g": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "--help-hidden": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-help": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--help": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--hip-link": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-H": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-I-": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ibuiltininc": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-idirafter": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iframeworkwithsysroot": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iframework": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-imacros": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--imacros": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-barrier": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-include": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-index-header-map": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-init-only": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-integrated-as": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-interface-stub-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-internal-externc-isystem": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-internal-isystem": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-iprefix": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iquote": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-isysroot": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-isystem-after": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-isystem": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ivfsoverlay": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iwithprefixbefore": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iwithprefix": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iwithsysroot": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-I": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-J": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-keep_private_externs": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-L": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-l": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-m16": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-m32": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-m3dnowa": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-m3dnow": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-m64": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mabi=ieeelongdouble": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mabi=vec-default": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mabi=vec-extabi": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mabicalls": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-Mach": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-madx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-maes": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-maix-struct-return": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-malign-double": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-maltivec": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mamx-bf16": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mamx-int8": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mamx-tile": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-marm": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-massembler-fatal-warnings": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-massembler-no-warn": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-matomics": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512bf16": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512bitalg": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512bw": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512cd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512dq": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512er": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512f": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512ifma": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512pf": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512vbmi2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512vbmi": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512vl": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512vnni": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512vp2intersect": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx512vpopcntdq": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavxvnni": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mavx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mbackchain": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mbig-endian": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mbmi2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mbmi": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mbranch-likely": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mbranch-target-enforce": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mbranches-within-32B-boundaries": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mbulk-memory": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcheck-zero-division": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcldemote": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mclflushopt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mclwb": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mclzero": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcmodel=medany": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcmodel=medlow": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcmpb": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcmse": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcode-object-v3": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mconstant-cfstrings": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mconstructor-aliases": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mcrbits": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcrc": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcrypto": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcumode": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcx16": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mdirect-move": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mdisable-tail-calls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mdouble-float": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mdspr2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mdsp": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MD": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mefpu2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-membedded-data": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-menable-experimental-extensions": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-menable-no-infs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-menable-no-nans": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-menable-unsafe-fp-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-menqcmd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mexception-handling": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mexecute-only": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mextern-sdata": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mf16c": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfancy-math-387": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mfentry": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfix-and-continue": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mfix-cortex-a53-835769": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfloat128": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfma4": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfma": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfp32": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfp64": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfprnd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfpxx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfsgsbase": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfxsr": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MF": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mgeneral-regs-only": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mgfni": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mginv": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mglibc": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mglobal-merge": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mgpopt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MG": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mhard-float": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mhreset": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mhtm": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mhvx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-miamcu": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mieee-fp": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mieee-rnd-near": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mignore-xcoff-visibility": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--migrate": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-migrate": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mimplicit-float": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mincremental-linker-compatible": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-minline-all-stringops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-minvariant-function-descriptors": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-minvpcid": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips16": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips1": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips32r2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips32r3": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips32r5": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips32r6": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips32": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips3": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips4": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips5": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips64r2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips64r3": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips64r5": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips64r6": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mips64": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-misel": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MJ": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mkernel": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mkl": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mldc1-sdc1": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mlittle-endian": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mlocal-sdata": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mlong-calls": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mlong-double-128": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mlong-double-64": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mlong-double-80": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mlongcall": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mlvi-cfi": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mlvi-hardening": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mlwp": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mlzcnt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmadd4": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmark-bti-property": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MMD": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmemops": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmfocrf": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmicromips": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmma": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmmx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmovbe": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmovdir64b": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmovdiri": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmpx": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mms-bitfields": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmsa": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmultivalue": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmutable-globals": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmwaitx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MM": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-3dnowa": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-3dnow": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-abicalls": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-adx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-aes": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-altivec": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-amx-bf16": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-amx-int8": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-amx-tile": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-atomics": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512bf16": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512bitalg": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512bw": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512cd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512dq": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512er": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512f": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512ifma": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512pf": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512vbmi2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512vbmi": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512vl": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512vnni": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512vp2intersect": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx512vpopcntdq": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avxvnni": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-avx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-backchain": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-bmi2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-bmi": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-branch-likely": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-bulk-memory": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-check-zero-division": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-cldemote": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-clflushopt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-clwb": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-clzero": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-cmpb": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-code-object-v3": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-constant-cfstrings": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-crbits": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-crc": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-crypto": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-cumode": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-cx16": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-direct-move": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-dspr2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-dsp": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-embedded-data": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-enqcmd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-exception-handling": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-execute-only": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-extern-sdata": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-f16c": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-fix-cortex-a53-835769": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-float128": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-fma4": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-fma": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-fprnd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-fsgsbase": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-fxsr": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-gfni": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-ginv": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-global-merge": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-gpopt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-hreset": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-htm": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-hvx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-iamcu": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-implicit-float": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-incremental-linker-compatible": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-inline-all-stringops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mno-invariant-function-descriptors": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-invpcid": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-isel": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-kl": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-ldc1-sdc1": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-local-sdata": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-long-calls": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-longcall": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-lvi-cfi": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-lvi-hardening": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-lwp": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-lzcnt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-madd4": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-memops": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-mfocrf": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-micromips": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-mips16": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-mma": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-mmx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-movbe": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-movdir64b": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-movdiri": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-movt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-mpx": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mno-ms-bitfields": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-msa": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-mt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-multivalue": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-mutable-globals": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-mwaitx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-neg-immediates": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-nontrapping-fptoint": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-nvj": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-nvs": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-odd-spreg": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-omit-leaf-frame-pointer": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-outline-atomics": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mno-outline": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mno-packed-stack": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-packets": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-paired-vector-memops": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-pascal-strings": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-pclmul": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-pconfig": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-pcrel": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-pie-copy-relocations": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-pku": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-popcntd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-popcnt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-power10-vector": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-power8-vector": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-power9-vector": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-prefetchwt1": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-prfchw": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-ptwrite": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-rdpid": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-rdrnd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-rdseed": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-red-zone": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-reference-types": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-relax-all": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-relax-pic-calls": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-relax": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-restrict-it": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-retpoline-external-thunk": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-retpoline": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-rtd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-rtm": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-sahf": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-save-restore": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-serialize": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-seses": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-sgx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-sha": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-shstk": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-sign-ext": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-simd128": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-soft-float": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-speculative-load-hardening": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-spe": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-sse2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-sse3": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-sse4.1": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-sse4.2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-sse4a": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-sse4": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mno-sse": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-ssse3": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-stack-arg-probe": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-stackrealign": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-tail-call": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-tbm": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-thumb": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-tls-direct-seg-refs": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-tsxldtrk": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-uintr": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-unaligned-access": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-unimplemented-simd128": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-unsafe-fp-atomics": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-vaes": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-virt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-vpclmulqdq": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-vsx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-vx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-vzeroupper": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-waitpkg": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-warn-nonportable-cfstrings": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-wavefrontsize64": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-wbnoinvd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-widekl": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-x87": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-xgot": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-xop": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-xsavec": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-xsaveopt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-xsaves": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-xsave": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mnocrc": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mnoexecstack": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mnontrapping-fptoint": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mnop-mcount": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mnvj": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mnvs": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-modd-spreg": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-module-file-deps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-module-file-info": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-momit-leaf-frame-pointer": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-moutline-atomics": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-moutline": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mpacked-stack": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpackets": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpaired-vector-memops": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpascal-strings": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpclmul": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpconfig": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpcrel": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpie-copy-relocations": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpku": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpopcntd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpopcnt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpower10-vector": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpower8-vector": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpower9-vector": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mprefetchwt1": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mprfchw": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mptwrite": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MP": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mqdsp6-compat": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MQ": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mrdpid": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mrdrnd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mrdseed": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mreassociate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mrecip": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mrecord-mcount": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mred-zone": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mreference-types": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mrelax-all": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mrelax-pic-calls": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--mrelax-relocations": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mrelax": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mrestrict-it": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mretpoline-external-thunk": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mretpoline": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mrtd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mrtm": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msahf": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msave-restore": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msave-temp-labels": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-msecure-plt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mserialize": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mseses": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msgx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msha": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mshstk": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msign-ext": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msimd128": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msim": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msingle-float": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msoft-float": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mspeculative-load-hardening": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mspe": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msse2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msse3": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msse4.1": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msse4.2": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msse4a": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msse4": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-msse": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mssse3": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mstack-arg-probe": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mstackrealign": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mstrict-align": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-msvr4-struct-return": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mtail-call": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mtbm": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mthumb": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mtls-direct-seg-refs": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mtsxldtrk": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MT": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-muclibc": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-muintr": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-multi_module": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-munaligned-access": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-munimplemented-simd128": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-munsafe-fp-atomics": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-munwind-tables": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mv55": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mv5": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mv60": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mv62": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mv65": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mv66": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mv67t": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mv67": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mvaes": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mvirt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mvpclmulqdq": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mvsx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mvx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mvzeroupper": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MV": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mwaitpkg": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mwarn-nonportable-cfstrings": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mwavefrontsize64": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mwbnoinvd": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mwidekl": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mx32": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mx87": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mxgot": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mxop": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mxsavec": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mxsaveopt": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mxsaves": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mxsave": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-M": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-new-struct-path-tbaa": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-no-canonical-prefixes": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-no-code-completion-globals": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-no-code-completion-ns-level-decls": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-no-cpp-precomp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--no-cuda-noopt-device-debug": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--no-cuda-version-check": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-no-emit-llvm-uselists": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-no-finalize-removal": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-no-implicit-float": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-no-integrated-as": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-no-integrated-cpp": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--no-integrated-cpp": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-no-ns-alloc-error": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-no-pedantic": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--no-pedantic": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-no-pthread": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-no-struct-path-tbaa": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--no-undefined": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-no_dead_strip_inits_and_terms": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-nobuiltininc": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-nocpp": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-nodefaultlibs": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-nofixprebinding": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-nogpuinc": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-nogpulib": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-nolibc": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-nomultidefs": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-nopie": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-noprebind": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-noprofilelib": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-noseglinkedit": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-nostartfiles": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-nostdinc++": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-nostdinc": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-nostdlib++": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-nostdlibinc": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-nostdlib": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-nostdsysteminc": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-n": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-O0": {
            "category": ClangOptionCategory.OPTIMIZATION,
            "action": "keep"
        },
        "-O4": {
            "category": ClangOptionCategory.OPTIMIZATION,
            "action": "keep"
        },
        "-ObjC++": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-objc-isystem": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-atomic-property": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-all": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-annotation": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-designated-init": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-instancetype": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-literals": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-ns-macros": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-property-dot-syntax": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-property": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-protocol-conformance": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-readonly-property": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-readwrite-property": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-migrate-subscripting": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-ns-nonatomic-iosonly": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-returns-innerpointer-property": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcxx-isystem": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ObjC": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-object": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-O": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-o": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-pagezero_size": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-pass-exit-codes": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--pass-exit-codes": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-pch-through-hdrstop-create": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-pch-through-hdrstop-use": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-pedantic-errors": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--pedantic-errors": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-pedantic": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--pedantic": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-pg": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-pic-is-pie": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-pie": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-pipe": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--pipe": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-prebind_all_twolevel_modules": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-prebind": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--precompile": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-preload": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-dependency-directives-minimized-source": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-diagnostic-categories": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-effective-triple": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-effective-triple": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-ivar-layout": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-print-libgcc-file-name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-libgcc-file-name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-multi-directory": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-multi-directory": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-multi-lib": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-multi-lib": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-multi-os-directory": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-multi-os-directory": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-preamble": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-resource-dir": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-resource-dir": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-search-dirs": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-search-dirs": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-stats": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-print-supported-cpus": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--print-supported-cpus": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-print-target-triple": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-target-triple": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-targets": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-targets": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-private_bundle": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-pthreads": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-pthread": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-P": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-p": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-Qn": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Qunused-arguments": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-Qy": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Q": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-rdynamic": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-relaxed-aliasing": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-relocatable-pch": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--relocatable-pch": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-remap": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-rewrite-legacy-objc": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-rewrite-macros": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-rewrite-objc": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-rewrite-test": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-r": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-save-stats": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--save-stats": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-save-temps": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--save-temps": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-seg1addr": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-seglinkedit": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-setup-static-analyzer": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-shared-libgcc": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-shared-libsan": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-shared": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--shared": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-show-encoding": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--show-includes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-show-inst": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--signed-char": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-single_module": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-split-stacks": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-static-define": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-static-libgcc": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-static-libgfortran": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-static-libsan": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-static-libstdc++": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-static-openmp": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-static-pie": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-static": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--static": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-stdlib++-isystem": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-sub_library": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-sub_umbrella": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-sys-header-deps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-S": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-s": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--target-help": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-Tbss": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-Tdata": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-templight-dump": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-test-io": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-time": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-traditional-cpp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--traditional-cpp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-traditional": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--traditional": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-trim-egraph": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Ttext": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-twolevel_namespace_hints": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-twolevel_namespace": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-T": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-t": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-undefined": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-undef": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-unoptimized-cfg": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--unsigned-char": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-U": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-u": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-vectorize-loops": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-vectorize-slp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--verify-debug-info": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-verify-ignore-unexpected": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-verify-pch": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-verify": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--version": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-via-file-asm": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--via-file-asm": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-V": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-v": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Wall": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-WCL4": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-Wdeprecated": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-whatsloaded": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-whyload": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-Wlarge-by-value-copy": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-Wno-deprecated": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-Wno-rewrite-macros": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Wno-write-strings": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-working-directory": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Wwrite-strings": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-w": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-X": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-x": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-Z-reserved-lib-cckext": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-Z-reserved-lib-stdc++": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-Z-Xlinker-no-demangle": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-Z": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--all-warnings": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "--assemble": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--comments-in-macros": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--comments": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--compile": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-dA": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--dependencies": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--entry": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-fcuda-rdc": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fembed-bitcode-marker": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodule-maps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-aligned-new": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-cuda-rdc": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-module-maps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-profile-use": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-record-gcc-switches": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frecord-gcc-switches": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftree-vectorize": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-gno-record-gcc-switches": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-grecord-gcc-switches": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-m80387": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcpu=?": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mmfcrf": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-80387": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-mfcrf": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-pure-code": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-zvector": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mpure-code": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mtune=?": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mzvector": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--no-line-commands": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-no-pie": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--no-standard-includes": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--no-standard-libraries": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--no-warnings": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-nocudainc": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-nocudalib": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--optimize": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--preprocess": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--print-missing-file-dependencies": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--profile-blocks": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--profile": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-shared-libasan": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--trace-includes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-trigraphs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--trigraphs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--user-dependencies": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--verbose": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--write-dependencies": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--write-user-dependencies": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        }
    },
    clang_space_dict = {
        "-add-plugin": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-allowable_client": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-analyze-function": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-checker": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-config-compatibility-mode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-config": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-constraints": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-disable-checker": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-dump-egraph": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-inline-max-stack-depth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-inlining-mode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-max-loop": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-output": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-purge": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-store": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-arch_only": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-arch": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-arcmt-migrate-report-output": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ast-dump-filter": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ast-merge": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-aux-target-cpu": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-aux-target-feature": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-aux-triple": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-bundle_loader": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ccc-arcmt-migrate": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ccc-gcc-name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ccc-install-dir": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ccc-objcmt-migrate": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-chain-include": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-code-completion-at": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--config": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-coverage-data-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-coverage-notes-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-default-function-attr": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-defsym": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-dependency-dot": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-dependency-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-diagnostic-log-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-dwarf-debug-flags": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-dwarf-debug-producer": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--dyld-prefix": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-dylib_file": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-error-on-deserialized-decl": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-exception-model": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-exported_symbols_list": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fbracket-depth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcaret-diagnostics-max-lines": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconstant-string-class": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconstexpr-backtrace-limit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconstexpr-depth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconstexpr-steps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcuda-include-gpubinary": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebug-compilation-dir": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-format": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-show-category": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ferror-limit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-filelist": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-filetype": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmacro-backtrace-limit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodule-feature": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodule-implementation-of": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-user-build-path": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fopenmp-host-ir-file-path": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-foperator-arrow-depth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-force_load": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fprofile-remapping-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-framework": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-frewrite-map-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fspell-checking-limit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftabstop": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftemplate-backtrace-limit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftemplate-depth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftrapv-handler": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftype-visibility": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-function-alignment": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvisibility": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-gcc-toolchain": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-gen-cdb-fragment-path": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-header-include-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-image_base": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-imultilib": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-include-pch": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-init": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-install_name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-lazy_framework": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-lazy_library": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-load": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-main-file-name": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mdebug-pass": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-meabi": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfloat-abi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mfpmath": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mlimit-float-precision": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mlink-bitcode-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mlink-builtin-bitcode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mlink-cuda-bitcode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mllvm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-module-dependency-dir": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mregparm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mrelocation-model": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-msmall-data-limit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mt-migrate-directory": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mthread-model": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mtp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-multiply_defined_unused": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-multiply_defined": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--no-system-header-prefix": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-opt-record-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-opt-record-format": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-opt-record-passes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-output-asm-variant": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--output-class-directory": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--param": {
            "category": ClangOptionCategory.OPTIMIZATION,
            "action": "keep"
        },
        "-pic-level": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-plugin": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--print-file-name": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "--print-prog-name": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-read_only_relocs": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-record-command-line": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-remap-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-resource-dir": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--resource": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-rpath": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-seg_addr_table_filename": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-seg_addr_table": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-segs_read_only_addr": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-segs_read_write_addr": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-serialize-diagnostic-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-serialize-diagnostics": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--serialize-diagnostics": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-specs": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--specs": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-split-dwarf-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-split-dwarf-output": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-stack-protector-buffer-size": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-stack-protector": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--sysroot": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--system-header-prefix": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-target-abi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-target-cpu": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-target-feature": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-target-linker-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-triple": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-tune-cpu": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-umbrella": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-unexported_symbols_list": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-weak_framework": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-weak_library": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-weak_reference_mismatches": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-Xanalyzer": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-Xarch_device": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-Xarch_host": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-Xassembler": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Xclang": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Xcuda-fatbinary": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-Xcuda-ptxas": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-Xlinker": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-Xopenmp-target": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-Xpreprocessor": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Zlinker-input": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-z": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--assert": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "--bootclasspath": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--CLASSPATH": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--classpath": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--define-macro": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--encoding": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--extdirs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodule-name": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fnew-alignment": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--for-linker": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--force-link": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--include-directory-after": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-directory": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-prefix": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-with-prefix-after": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-with-prefix-before": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-with-prefix": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--language": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--library-directory": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--mhwdiv": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--output": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--prefix": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--rtlib": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--stdlib": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--std": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-target": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--undefine-macro": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        }
    },
    clang_equal_dict = {
        "-analyze-function": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-checker": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-config-compatibility-mode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-constraints": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-disable-checker": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-dump-egraph": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-inline-max-stack-depth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-inlining-mode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-output": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-purge": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-analyzer-store": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-arcmt-action": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ast-dump-all": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-ast-dump": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--autocomplete": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-cl-std": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-code-completion-at": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-compress-debug-sections": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--compress-debug-sections": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--config-system-dir": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--config-user-dir": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-coverage-data-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-coverage-notes-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-coverage-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--cuda-gpu-arch": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--cuda-include-ptx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--cuda-path": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-debug-info-kind": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--debug": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-debugger-tuning": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--dependent-lib": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--driver-mode": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-dwarf-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--dyld-prefix": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-error-on-deserialized-decl": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-exception-model": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-faddress-space-map-mangling": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-falign-functions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-falign-jumps": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-falign-labels": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-falign-loops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-faligned-new": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fauto-profile": {
            "category": ClangOptionCategory.OPTIMIZATION,
            "action": "keep"
        },
        "-fbasic-block-sections": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fbinutils-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fblas-matmul-limit": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fbootclasspath": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fbracket-depth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fbuild-session-file": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fbuild-session-timestamp": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fcf-protection": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcf-runtime-abi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcheck": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fclang-abi-compat": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fclasspath": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcoarray": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fcompile-resource": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconstant-string-class": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconstexpr-backtrace-limit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconstexpr-depth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconstexpr-steps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fconvert": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fcrash-diagnostics-dir": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fcs-profile-generate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebug-compilation-dir": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebug-default-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdebug-prefix-map": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdefault-calling-conv": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdenormal-fp-math-f32": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdenormal-fp-math": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdepfile-entry": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-color": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-format": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-hotness-threshold": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-show-category": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fdiagnostics-show-location": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fembed-bitcode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fencoding": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ferror-limit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fexcess-precision": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-fexec-charset": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fextdirs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffile-prefix-map": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffp-contract": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffp-exception-behavior": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffp-model": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffpe-trap": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fgnuc-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-finit-character": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-finit-integer": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-finit-logical": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-finit-real": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-finline-limit": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-finput-charset": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fixit": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-flax-vector-conversions": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-flimited-precision": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-flto-jobs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-flto": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmacro-backtrace-limit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmacro-prefix-map": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmax-array-constructor": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fmax-errors": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fmax-stack-var-size": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fmax-subrecord-length": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fmax-tokens": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmax-type-align": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmemory-profile": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmessage-length": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodule-file": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fmodule-format": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodule-map-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodule-name": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-cache-path": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fmodules-embed-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-ignore-macro": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmodules-prune-after": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fmodules-prune-interval": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fms-compatibility-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fms-memptr-rep": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fmsc-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fnew-alignment": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-abi-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-arc-cxxlib": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-dispatch-method": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-nonfragile-abi-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fobjc-runtime": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-cuda-blocks-per-sm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-cuda-number-of-sm": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-cuda-teams-reduction-recs-num": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-foperator-arrow-depth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-foptimization-record-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-foptimization-record-passes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-foutput-class-dir": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-foverride-record-layout": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpack-struct": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpass-plugin": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpatchable-function-entry-offset": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fpatchable-function-entry": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fplugin": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprebuilt-module-path": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fproc-stat-report": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-dir": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-exclude-files": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-filter-files": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-generate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-instr-generate": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-instr-use": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-instrument-path": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-instrument-use-path": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-instrument": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-list": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-prefix-map": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-remapping-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-sample-use": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-update": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fprofile-use": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frandom-seed": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-frecord-marker": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-frewrite-map-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-address-field-padding": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-blacklist": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-allowlist": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-blacklist": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-blocklist": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-type": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage-whitelist": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-hwaddress-abi": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-memory-track-origins": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-system-blacklist": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-undefined-strip-path-components": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsave-optimization-record": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fshow-overloads": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fspell-checking-limit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsymbol-partition": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftabstop": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftemplate-backtrace-limit": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftemplate-depth": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftest-module-file-extension": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fthin-link-bitcode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fthinlto-index": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftime-report": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftime-trace-granularity": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftls-model": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftrap-function": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftrapv-handler": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftrivial-auto-var-init-stop-after": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftrivial-auto-var-init": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fuse-ld": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fveclib": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvisibility-dllexport": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvisibility-externs-dllimport": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvisibility-externs-nodllstorageclass": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvisibility-nodllstorageclass": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fvisibility": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fwchar-type": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-function-groups": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-selected-function-group": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-G": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--gcc-toolchain": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--gpu-instrument-lib": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--gpu-max-threads-per-block": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-gsplit-dwarf": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gz": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "--hip-device-lib": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--hip-version": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--ld-path": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--libomptarget-nvptx-bc-path": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--linker-option": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mabi": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mabs": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-malign-branch-boundary": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-malign-functions": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-malign-jumps": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-malign-loops": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-march": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-masm": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mbranch-protection": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mcmodel": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcode-object-version": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcompact-branches": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mcpu": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mdouble": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mexec-model": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfloat-abi": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfpmath": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mfpu": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mframe-pointer": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-mharden-sls": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mhvx-length": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mhvx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mhwdiv": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mhwmult": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mimplicit-it": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mindirect-jump": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mios-simulator-version-min": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mios-version-min": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-miphoneos-version-min": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mlinker-version": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmacos-version-min": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmacosx-version-min": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mmcu": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mnan": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-moslib": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mpad-max-prefix-size": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mprefer-vector-width": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mregparm": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msign-return-address-key": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-msign-return-address": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msmall-data-limit": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msmall-data-threshold": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mstack-alignment": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mstack-probe-size": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mstack-protector-guard-offset": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mstack-protector-guard-reg": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mstack-protector-guard": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-msve-vector-bits": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mtls-size": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mtp": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mtune": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mtvos-simulator-version-min": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mtvos-version-min": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mwatchos-simulator-version-min": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-mwatchos-version-min": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--no-cuda-gpu-arch": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--no-cuda-include-ptx": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--no-offload-arch": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--no-system-header-prefix": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-white-list-dir-path": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcmt-whitelist-dir-path": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--offload-arch": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--output-class-directory": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--param": {
            "category": ClangOptionCategory.OPTIMIZATION,
            "action": "keep"
        },
        "-pch-through-header": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-preamble-bytes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-print-file-name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-file-name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-print-prog-name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--print-prog-name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--ptxas-path": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-resource-dir": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--resource": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--rocm-device-lib-path": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--rocm-path": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-Rpass-analysis": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Rpass-missed": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Rpass": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--rsp-quoting": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-rtlib": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--rtlib": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-save-stats": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--save-stats": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-save-temps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--save-temps": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-specs": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--specs": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-stats-file": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-std-default": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-std": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--std": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-stdlib": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--stdlib": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-sycl-std": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--sysroot": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--system-header-prefix": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-target-sdk-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--target": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-triple": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-unwindlib": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--unwindlib": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-vtordisp-mode": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--warn-": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-Wframe-larger-than": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Wlarge-by-value-copy": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Wlarger-than": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-working-directory": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--assert": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "--bootclasspath": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--CLASSPATH": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--classpath": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--define-macro": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--encoding": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--extdirs": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--for-linker": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--force-link": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--hip-device-lib-path": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "--imacros": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-directory-after": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-directory": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-prefix": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-with-prefix-after": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-with-prefix-before": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include-with-prefix": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--language": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--library-directory": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mappletvos-version-min": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mappletvsimulator-version-min": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--mhwdiv": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-miphonesimulator-version-min": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mwatchsimulator-version-min": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--optimize": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "--output": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--prefix": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--undefine-macro": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        }
    },
    clang_match_dict = {
        "-A-": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "--analyzer-output": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-A": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-a": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-B": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-b": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-c-isystem": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ccc-": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-cl-ext=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-client_name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-compatibility_version": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-current_version": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-cxx-isystem": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-dsym-dir": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-dylinker_install_name": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-D": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-d": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-e": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-fcomment-block-commands=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffixed-line-length-": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fforce-addr": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ffree-line-length-": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-fmodules-embed-all-files": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-builtin-": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-coverage=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-recover=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize-trap=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fno-sanitize=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fopenmp-targets=": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-fproc-stat-report": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-coverage=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-recover=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize-trap=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fsanitize=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-ftemplate-depth-": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-always-instrument": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-attr-list": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-instruction-threshold": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-instrumentation-bundle": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-modes": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-fxray-never-instrument": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-F": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-gcoff": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gstabs": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gvms": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-gxcoff": {
            "category": ClangOptionCategory.DEBUGGING,
            "action": "keep"
        },
        "-G": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-headerpad_max_install_names": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-idirafter": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iframeworkwithsysroot": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iframework": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-imacros": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--imacros": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-include": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "--include": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-interface-stub-version": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-internal-externc-isystem": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-internal-isystem": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-iprefix": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iquote": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-isysroot": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-isystem-after": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-isystem": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-ivfsoverlay": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iwithprefixbefore": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iwithprefix": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-iwithsysroot": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-I": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-J": {
            "category": ClangOptionCategory.FORTRAN,
            "action": "keep"
        },
        "-L": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-l": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-malign-branch=": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mconsole": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mdefault-build-attributes": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mdll": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mdynamic-no-pic": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MF": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MJ": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mno-default-build-attributes": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MQ": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mrecip=": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mthreads": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-MT": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-municode": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-mwindows": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-objc-isystem": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-objcxx-isystem": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Ofast": {
            "category": ClangOptionCategory.OPTIMIZATION,
            "action": "keep"
        },
        "-O": {
            "category": ClangOptionCategory.OPTIMIZATION,
            "action": "keep"
        },
        "-o": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-pagezero_size": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-plugin-arg-": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-R": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-seg1addr": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-segs_read_": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-stdlib++-isystem": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-sub_library": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-sub_umbrella": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-Tbss": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-Tdata": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-Ttext": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-T": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-undefined": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-U": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-u": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-verify-ignore-unexpected=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-verify=": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-V": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-Wa,": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--warn-": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-weak-l": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-Wl,": {
            "category": ClangOptionCategory.LINKER,
            "action": "keep"
        },
        "-Wlarger-than-": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-Wno-nonportable-cfstrings": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-Wnonportable-cfstrings": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-working-directory": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Wp,": {
            "category": ClangOptionCategory.COMPILE,
            "action": "keep"
        },
        "-Wundef-prefix=": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-W": {
            "category": ClangOptionCategory.WARNING,
            "action": "keep"
        },
        "-Xarch_": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-Xopenmp-target": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-X": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-x": {
            "category": ClangOptionCategory.OVERALL,
            "action": "keep"
        },
        "-y": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "-Z": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        },
        "--": {
            "category": ClangOptionCategory.NONETIME,
            "action": "keep"
        }
    }