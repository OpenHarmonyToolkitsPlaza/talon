from command_parser.clang_parse.clang_option_dict.clang_option_dict_base import ClangOptionDictBase
from configure.env_config import EnvConfig

class ClangOptionDictFactory(object):
    
    version_dict = {
        "4": "4.0.0.xml",
        "5": "5.0.0.xml",
        "6": "6.0.0.xml",
        "7": "7.0.0.xml",
        "8": "8.0.0.xml",
        "9": "9.0.0.xml",
        "10": "10.0.0.xml",
        "11": "11.0.0.xml",
        "12": "12.0.0.xml",
        "13": "13.0.0.xml",
        "14": "14.0.0.xml",
        "15": "15.0.0.xml"
    }
    
    @staticmethod
    def get_option_dict(ver=""):
        
        version_list = list(ClangOptionDictFactory.version_dict.keys()).sort()
        
        correspond_v = ver
        if version_list:
            for version in version_list:
                if version > ver:
                    break
                correspond_v = version
        
        return ClangOptionDictFactory.version_dict.get(correspond_v, ClangOptionDictBase)