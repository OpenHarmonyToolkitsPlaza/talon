import os
import sys
from enum import Enum, auto
from xml.etree import ElementTree

class OptionCategory(Enum):
    
    OVERALL = 1
    ELF = auto()
    I386 = auto()
    GOLD = auto()
    OBJECT_FILE = auto()
    UNKNOWN = auto()

option_dict_xml = os.path.join(os.path.dirname(__file__), "ld_option_dict/ld_option_dict.xml")

class OptionDict(object):
    
    
    def __init__(self, config_file):
        if not os.path.exists(config_file):
            raise Exception("There is not correspond configure file %s" % config_file)
        
        self.simple_option_dict = {}
        self.space_option_dict = {}
        self.equal_option_dict = {}
        self.match_option_dict = {}
        
        self.__load_option_dict(config_file)

    
    def __load_option_dict(self, config_file=""):
        
        option_dict_map = {
            "simple": self.simple_option_dict,
            "space": self.space_option_dict,
            "equal": self.equal_option_dict,
            "match": self.match_option_dict
        }
        
        category_map = {
            "overall" : OptionCategory.OVERALL,
            "elf" : OptionCategory.ELF,
            "i386" : OptionCategory.I386,
            "gold" : OptionCategory.GOLD,
            "object_file": OptionCategory.OBJECT_FILE,
            "unknown": OptionCategory.UNKNOWN
        }
        
        dict_tree = ElementTree.parse(config_file)
        root = dict_tree.getroot()
        for kind in root:
            for option in kind:
                option_dict = option_dict_map.get(kind.tag)
                category = category_map.get(option.tag, None)
                self.__append_single_option(option.text, category, option_dict)
        
    
    def __append_single_option(self, option="", category="", option_dict=""):
        
        item = {
            "category": category,
            "support": True if category != OptionCategory.UNKNOWN else False
        }
        
        option_dict[option] = item
        
    
    def get_simple_dict(self):
        return self.simple_option_dict


    def get_space_dict(self):
        return self.space_option_dict
    
    
    def get_equal_dict(self):
        return self.equal_option_dict
    
    
    def get_match_dict(self):
        return self.match_option_dict