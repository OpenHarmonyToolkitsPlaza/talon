# -*- coding:utf-8 -*-
"""
 Copyright (C), 2016-2017, Sourcebrella, Inc Ltd - All rights reserved.
 Unauthorized copying, using, modifying of this file, via any medium is strictly prohibited.
 Proprietary and confidential.

 Author: Jiaxing Wang<jiaxingwang@sbrella.com>
 File Description: This file is some table of ld's option category, LD_OptionCategory class record ld option
    category. ld_equal_dict used to recognize option -XXX=xxxx, ld_space_dict used to recognize option -XXX xxxx,
    ld_match_dict used to recognize option -Xxxx, ld_simple_dict used to recognize option -XXX.
 Creation Date: 2016-11-28
 Modification History:
    chrlis, add copyright header.
"""

ld_option_index = 200


class LD_OptionCategory(object):
    OVERALL = 0 + ld_option_index
    ELF = 1 + ld_option_index
    I386 = 2 + ld_option_index
    GOLD = 3 + ld_option_index
    OBJECT_FILE = 4 + ld_option_index
    UNKNOWN_OPTION = 5 + ld_option_index

ld_category_list = [
    "Overall Options",
    "ELF Options",
    "I386 Options",
    "Object File"
]


ld_simple_dict = {
    "-(": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-)": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Bdynamic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Bgroup": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--Bshareable": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Bstatic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Bsymbolic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Bsymbolic-functions": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--EB": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--EL": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Qy": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Ur": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--accept-unknown-input-arch": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--add-needed": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--add-stdcall-alias": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--allow-multiple-definition": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--allow-shlib-undefined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--apply-dynamic-relocs": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--as-needed": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--build-id": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--call_shared": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--check-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--compat-implib": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--copy-dt-needed-entries": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--cref": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--ctors-in-init-array": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--dc": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--default-imported-symver": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--default-symver": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--define-common": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--demangle": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--detect-odr-violations": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--disable-auto-image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--disable-auto-import": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--disable-large-address-aware": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--disable-long-section-names": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--disable-new-dtags": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--disable-runtime-pseudo-reloc": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--disable-stdcall-fixup": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--discard-all": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--discard-locals": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--discard-none": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--dll": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--dn": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--dp": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--dy": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--dynamic-list-cpp-new": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--dynamic-list-cpp-typeinfo": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--dynamic-list-data": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--dynamicbase": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--eh-frame-hdr": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--embedded-relocs": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--emit-relocs": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--enable-auto-image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--enable-auto-import": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--enable-extra-pe-debug": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--enable-extra-pep-debug": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--enable-long-section-names": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--enable-new-dtags": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--enable-runtime-pseudo-reloc": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--enable-stdcall-fixup": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--end-group": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--end-lib": {
        "action": "keep",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--error-unresolved-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--exclude-all-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--export-all-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--export-dynamic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--fatal-warnings": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--fix-arm1176": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--fix-cortex-a53-835769": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--fix-cortex-a53-843419": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--fix-cortex-a8": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--fix-v4bx": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--fix-v4bx-interworking": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--flto": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--force-exe-suffix": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--forceinteg": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--gc-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--gdb-index": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--gnu-unique": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--help": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--high-entropy-va": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--incremental": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--incremental-changed": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--incremental-full": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--incremental-startup-unchanged": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--incremental-unchanged": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--incremental-unknown": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--incremental-update": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--insert-timestamp": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--keep-files-mapped": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--kill-at": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--large-address-aware": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--ld-generated-unwind-info": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--leading-underscore": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--map-whole-files": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--merge-exidx-entries": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--mmap-output-file": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--nmagic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-accept-unknown-input-arch": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-allow-shlib-undefined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-as-needed": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-bind": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--no-check-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-copy-dt-needed-entries": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-define-common": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-demangle": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-dynamic-linker": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-export-dynamic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-fatal-warnings": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-gc-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-incremental": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--no-insert-timestamp": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--no-isolation": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--no-keep-memory": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-ld-generated-unwind-info": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--no-leading-underscore": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--no-map-whole-files": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-omagic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-print-gc-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-relax": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-seh": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--no-strip-discarded": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-undefined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-undefined-version": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-warn-mismatch": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-warn-search-mismatch": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--no-whole-archive": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--noinhibit-exec": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--non_shared": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--nostdlib": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--nxcompat": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--omagic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--p": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--pic-executable": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--pic-veneer": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--pie": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--plt-align": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--plt-static-chain": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--plt-thread-safe": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--pop-state": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--posix-fallocate": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--preread-archive-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--print-gc-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--print-icf-sections": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--print-map": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--print-memory-usage": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--print-output-format": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--print-sysroot": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--push-state": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--qmagic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--reduce-memory-overheads": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--relax": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--relocatable": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--rosegment": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--shared": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--sort-common": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--split-by-file": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--split-by-reloc": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--start-group": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--start-lib": {
        "action": "keep",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--static": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--stats": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--strip-all": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--strip-debug": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--strip-debug-gdb": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--strip-debug-non-line": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--strip-discarded": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--strip-lto-sections": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--support-old-code": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--target-help": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--text-reorder": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--threads": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--toc-optimize": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--toc-sort": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--trace": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--traditional-format": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--tsaware": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--undefined-version": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--unique": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--verbose": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--version": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--warn-alternate-em": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--warn-common": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--warn-constructors": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--warn-duplicate-exports": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--warn-execstack": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--warn-multiple-gp": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--warn-once": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--warn-search-mismatch": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--warn-section-align": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--warn-shared-textrel": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--warn-unresolved-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--wdmdriver": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--weak-unresolved-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--whole-archive": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Bdynamic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Bgroup": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-Bshareable": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Bstatic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Bsymbolic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Bsymbolic-functions": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-E": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-EB": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-EL": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-M": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-N": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Qy": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-S": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Ur": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-V": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-X": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-accept-unknown-input-arch": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-add-needed": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-add-stdcall-alias": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-allow-multiple-definition": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-allow-shlib-undefined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-apply-dynamic-relocs": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-as-needed": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-build-id": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-call_shared": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-check-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-compat-implib": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-copy-dt-needed-entries": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-cref": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-ctors-in-init-array": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-d": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-dc": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-default-imported-symver": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-default-symver": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-define-common": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-demangle": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-detect-odr-violations": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-disable-auto-image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-disable-auto-import": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-disable-large-address-aware": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-disable-long-section-names": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-disable-new-dtags": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-disable-runtime-pseudo-reloc": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-disable-stdcall-fixup": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-discard-all": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-discard-locals": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-discard-none": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-dll": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-dn": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-dp": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-dy": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-dynamic-list-cpp-new": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-dynamic-list-cpp-typeinfo": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-dynamic-list-data": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-dynamicbase": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-eh-frame-hdr": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-embedded-relocs": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-emit-relocs": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-enable-auto-image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-enable-auto-import": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-enable-extra-pe-debug": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-enable-extra-pep-debug": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-enable-long-section-names": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-enable-new-dtags": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-enable-runtime-pseudo-reloc": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-enable-stdcall-fixup": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-end-group": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-end-lib": {
        "action": "keep",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-error-unresolved-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-exclude-all-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-export-all-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-export-dynamic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-fatal-warnings": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-fix-arm1176": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-fix-cortex-a53-835769": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-fix-cortex-a53-843419": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-fix-cortex-a8": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-fix-v4bx": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-fix-v4bx-interworking": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-flto": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-force-exe-suffix": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-forceinteg": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-g": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-gc-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-gdb-index": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-gnu-unique": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-help": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-high-entropy-va": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-i": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-incremental": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-incremental-changed": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-incremental-full": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-incremental-startup-unchanged": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-incremental-unchanged": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-incremental-unknown": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-incremental-update": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-insert-timestamp": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-keep-files-mapped": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-kill-at": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-large-address-aware": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-ld-generated-unwind-info": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-leading-underscore": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-map-whole-files": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-merge-exidx-entries": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-mmap-output-file": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-n": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-nmagic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-accept-unknown-input-arch": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-allow-shlib-undefined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-as-needed": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-bind": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-no-check-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-copy-dt-needed-entries": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-define-common": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-demangle": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-dynamic-linker": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-export-dynamic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-fatal-warnings": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-gc-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-incremental": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-no-insert-timestamp": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-no-isolation": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-no-keep-memory": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-ld-generated-unwind-info": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-no-leading-underscore": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-no-map-whole-files": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-omagic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-print-gc-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-relax": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-seh": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-no-strip-discarded": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-undefined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-undefined-version": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-warn-mismatch": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-warn-search-mismatch": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-no-whole-archive": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-noinhibit-exec": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-non_shared": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-nostdlib": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-nxcompat": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-omagic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-p": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-pic-executable": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-pic-veneer": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-pie": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-plt-align": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-plt-static-chain": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-plt-thread-safe": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-pop-state": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-posix-fallocate": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-preread-archive-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-print-gc-sections": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-print-icf-sections": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-print-map": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-print-memory-usage": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-print-output-format": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-print-sysroot": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-push-state": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-q": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-qmagic": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-r": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-reduce-memory-overheads": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-relax": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-relocatable": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-rosegment": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-s": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-shared": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-sort-common": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-split-by-file": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-split-by-reloc": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-start-group": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-start-lib": {
        "action": "keep",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-static": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-stats": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-strip-all": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-strip-debug": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-strip-debug-gdb": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-strip-debug-non-line": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-strip-discarded": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-strip-lto-sections": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-support-old-code": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-t": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-target-help": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-text-reorder": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-threads": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-toc-optimize": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-toc-sort": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-trace": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-traditional-format": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-tsaware": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-undefined-version": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-unique": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-v": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-verbose": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-version": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-warn-alternate-em": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-warn-common": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-warn-constructors": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-warn-duplicate-exports": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-warn-execstack": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-warn-multiple-gp": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-warn-once": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-warn-search-mismatch": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-warn-section-align": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-warn-shared-textrel": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-warn-unresolved-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-wdmdriver": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-weak-unresolved-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-whole-archive": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-x": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
}



ld_space_dict = {
    "--Map": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Tbss": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Tdata": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Tldata-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Trodata-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Ttext": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Ttext-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--architecture": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--assert": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--audit": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--auxiliary": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--base_file": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--build-id-chunk-size-for-treehash": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--build-id-min-file-size-for-treehash": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--compress-debug-sections": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--dT": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--debug": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--default-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--defsym": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--depaudit": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--dll-search-prefix": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--dynamic-linker": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--dynamic-list": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--enable-auto-image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--entry": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--exclude-libs": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--exclude-modules-for-implib": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--exclude-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--export-dynamic-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--file-alignment": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--filter": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--fini": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--flto-partition": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--format": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--fuse-ld": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--gpsize": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--hash-bucket-empty-fraction": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--hash-size": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--hash-style": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--heap": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--icf": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--icf-iterations": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--ignore-unresolved-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--incremental-base": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--incremental-patch": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--init": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--just-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--keep-unique": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--library": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--library-path": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--major-image-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--major-os-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--major-subsystem-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--minor-image-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--minor-os-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--minor-subsystem-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--mri-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--oformat": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--orphan-handling": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--out-implib": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--output": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--output-def": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--plugin": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--plugin-opt": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--print-symbol-counts": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--require-defined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--retain-symbols-file": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--rosegment-gap": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--rpath": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--rpath-link": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--section-alignment": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--section-ordering-file": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--section-start": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--soname": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--sort-section": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--spare-dynamic-tags": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--split-stack-adjust-size": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--stack": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--stub-group-size": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--subsystem": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--sysroot": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--task-link": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--thread-count": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--thread-count-final": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--thread-count-initial": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--thread-count-middle": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--trace-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--undefined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--unresolved-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--version-exports-section": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--version-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--wrap": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-A": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-F": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-G": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-I": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-L": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Map": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-O": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-P": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-R": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-T": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": False
    },
    "-Tbss": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Tdata": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Tldata-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Trodata-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Ttext": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Ttext-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Y": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-a": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-architecture": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-assert": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-audit": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-auxiliary": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-b": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-base_file": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-build-id-chunk-size-for-treehash": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-build-id-min-file-size-for-treehash": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-c": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-compress-debug-sections": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-dT": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-debug": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-default-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-defsym": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-depaudit": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-dll-search-prefix": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-dynamic-linker": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-dynamic-list": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-e": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-enable-auto-image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-entry": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-exclude-libs": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-exclude-modules-for-implib": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-exclude-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-export-dynamic-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-f": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-file-alignment": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-filter": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-fini": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-flto-partition": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-format": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-fuse-ld": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-gpsize": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-h": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-hash-bucket-empty-fraction": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-hash-size": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-hash-style": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-heap": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-icf": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-icf-iterations": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-ignore-unresolved-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-incremental-base": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-incremental-patch": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-init": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-just-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-keep-unique": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-l": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-library": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-library-path": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-m": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-major-image-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-major-os-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-major-subsystem-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-minor-image-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-minor-os-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-minor-subsystem-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-mri-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-o": {
        "action": "keep",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-oformat": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-optimize": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-orphan-handling": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-out-implib": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-output": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-output-def": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-plugin": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-plugin-opt": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-print-symbol-counts": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-require-defined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-retain-symbols-file": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-rosegment-gap": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-rpath": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-rpath-link": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-section-alignment": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-section-ordering-file": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-section-start": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-soname": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-sort-section": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-spare-dynamic-tags": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-split-stack-adjust-size": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-stack": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-stub-group-size": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-subsystem": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-sysroot": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-task-link": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-thread-count": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-thread-count-final": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-thread-count-initial": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-thread-count-middle": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-trace-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-u": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-undefined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-unresolved-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-version-exports-section": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-version-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-wrap": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-y": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-z": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
}



ld_equal_dict = {
    "--Map": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Tbss": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Tdata": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Tldata-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Trodata-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Ttext": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--Ttext-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--architecture": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--assert": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--audit": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--auxiliary": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--build-id-chunk-size-for-treehash": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--build-id-min-file-size-for-treehash": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--compress-debug-sections": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--dT": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--debug": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--default-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--defsym": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--demangle": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--depaudit": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--dll-search-prefix": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--dynamic-linker": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--dynamic-list": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--enable-auto-image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--entry": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--exclude-libs": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--exclude-modules-for-implib": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--exclude-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--export-dynamic-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--file-alignment": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--filter": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--fini": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--flto-partition": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--format": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--fuse-ld": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--gpsize": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--hash-bucket-empty-fraction": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--hash-size": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--hash-style": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "--heap": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--icf": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--icf-iterations": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--ignore-unresolved-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--incremental-base": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--incremental-patch": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--init": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--just-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--keep-unique": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--library": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--library-path": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--major-image-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--major-os-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--major-subsystem-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--minor-image-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--minor-os-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--minor-subsystem-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--mri-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--oformat": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--orphan-handling": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--out-implib": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--output": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--output-def": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--plugin": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--plugin-opt": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--print-symbol-counts": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--require-defined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--retain-symbols-file": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--rosegment-gap": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--rpath": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--rpath-link": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--section-alignment": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--section-ordering-file": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--section-start": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--soname": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--sort-common": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--sort-section": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--spare-dynamic-tags": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--split-by-file": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--split-by-reloc": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--split-stack-adjust-size": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--stack": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--stub-group-size": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--subsystem": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--sysroot": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--task-link": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--thread-count": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--thread-count-final": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--thread-count-initial": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--thread-count-middle": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "--thumb-entry": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "--trace-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--undefined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--unique": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--unresolved-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--verbose": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--version-exports-section": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--version-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--wrap": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-A": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-F": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-I": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-L": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Map": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-O": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-P": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-R": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Tbss": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Tdata": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Tldata-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Trodata-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Ttext": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Ttext-segment": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-Y": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-architecture": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-assert": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-audit": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-auxiliary": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-build-id-chunk-size-for-treehash": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-build-id-min-file-size-for-treehash": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-compress-debug-sections": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-dT": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-debug": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-default-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-defsym": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-demangle": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-depaudit": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-dll-search-prefix": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-dynamic-linker": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-dynamic-list": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-enable-auto-image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-entry": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-exclude-libs": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-exclude-modules-for-implib": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-exclude-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-export-dynamic-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-file-alignment": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-filter": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-fini": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-flto-partition": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-format": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-fuse-ld": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-gpsize": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-hash-bucket-empty-fraction": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-hash-size": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-hash-style": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-heap": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-icf": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-icf-iterations": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-ignore-unresolved-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-image-base": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-incremental-base": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-incremental-patch": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-init": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-just-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-keep-unique": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-l": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-library": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-library-path": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-major-image-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-major-os-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-major-subsystem-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-minor-image-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-minor-os-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-minor-subsystem-version": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-mri-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-oformat": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-optimize": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-orphan-handling": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-out-implib": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-output": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-output-def": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-plugin": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-plugin-opt": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-print-symbol-counts": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-require-defined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-retain-symbols-file": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-rosegment-gap": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-rpath": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-rpath-link": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-section-alignment": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-section-ordering-file": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-section-start": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-soname": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-sort-common": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-sort-section": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-spare-dynamic-tags": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-split-by-file": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-split-by-reloc": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-split-stack-adjust-size": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-stack": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-stub-group-size": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-subsystem": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-sysroot": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-task-link": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-thread-count": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-thread-count-final": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-thread-count-initial": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-thread-count-middle": {
        "action": "filter",
        "category": LD_OptionCategory.GOLD,
        "support": True
    },
    "-thumb-entry": {
        "action": "filter",
        "category": LD_OptionCategory.I386,
        "support": True
    },
    "-trace-symbol": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-undefined": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-unique": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-unresolved-symbols": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-verbose": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-version-exports-section": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-version-script": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-wrap": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-y": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
}



ld_match_dict = {
    "--dT": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "--soname": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-A": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-F": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-G": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-I": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-L": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-O": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-P": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
    "-R": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-T": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": False
    },
    "-Y": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-a": {
        "action": "filter",  # Can not supported by ld.gold
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-b": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-c": {
        "action": "filter",  # Can not supported by ld.gold
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-dT": {
        "action": "filter",  # Can not supported by ld.gold
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-e": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-f": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-h": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-l": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-m": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-o": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-soname": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-u": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-y": {
        "action": "filter",
        "category": LD_OptionCategory.OVERALL,
        "support": True
    },
    "-z": {
        "action": "filter",
        "category": LD_OptionCategory.ELF,
        "support": True
    },
}
