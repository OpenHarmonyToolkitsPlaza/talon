from command_parser.option_filter_base import FilterBase
from command_parser.option_base import *

class ARFilter(FilterBase):
    def __init__(self, context=None):
        self.context = context
    
    def filter_options_default(self, compiler="", cwd="", options=[], option=None):
        
        filter_options = []
        
        for opt in options:
            if not opt.keep:
                filter_options.append(opt)
        
        self.remove_options_by_list(options=options, remove_list=filter_options) 