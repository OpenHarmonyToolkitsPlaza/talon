
class Singleton2(object):
    
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, "_instance"):
            cls._instance = super(Singleton, cls).__new__(cls, *args, **kwargs)
        return cls._instance
    
class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class MyClass1(metaclass=Singleton):
    def __init__(self):
        print("__init__")
    
if __name__ == "__main__":
    obj1 = MyClass1()
    obj2 = MyClass1()
    print(obj1)
    print(obj2)