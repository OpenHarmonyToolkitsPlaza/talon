import os
import sys
import argparse

from base.context import Context

class ArgParser(object):
    def __init__(self):
        self.__context = Context()
        self.__parser = argparse.ArgumentParser()
        self.__build_args = []
        self.add_args()
    
    def get_build_args(self):
        return self.__build_args
    
    def add_args(self):
        self.__parser.add_argument("--capture-only", dest="capture_only", action="store_true", default=False, 
                                   help="only capture the build process and store the information of building")
        
        self.__parser.add_argument("--irbuild-only", dest="irbuild_only", action="store_true", default=False,
                                   help='reuse the capture result before and just generate ir')

        self.__parser.add_argument("--clear-work-dirs", dest="clear_work_dirs", action="store_true",
                                   help="clear the work dirs")
        
        self.__parser.add_argument("--enable-graph-dump", dest="enable_graph_dump", action="store_true",
                                   help="enable dump the build graph")
        
        self.__parser.add_argument("--build-pool-size", dest="build_pool_size", type=int,
                                   help="specify the ir generate pool size")
        
        self.__parser.add_argument("--specify-maple-clang", dest="specify_maple_clang", type=str,
                                   help="specify maple front clang bin path")
        
        self.__parser.add_argument("--specify-maple-clangcpp", dest="specify_maple_clangcpp", type=str,
                                   help="specify maple front clang++ bin path")
        
        self.__parser.add_argument("--specify-maple-bin", dest="specify_maple_bin", type=str,
                                    help="specify maple ir generate tool path")
        
        self.__parser.add_argument("--specify-maple-gcc", dest="specify_maple_gcc", type=str,
                                    help="specify maple-gcc")
        
        self.__parser.add_argument("--specify-maple-maple", dest="specify_maple_maple", type=str,
                                    help="specify maple-maple")
        
        self.__parser.add_argument("--specify-maple-root", dest="specify_maple_root", type=str,help="specify maple-root")

        self.__parser.add_argument("--ir-cat-mode", dest="ir_cat_mode", action="store_true",
                                   help="using the maple ir cat mode")
        
        self.__parser.add_argument("--enable-log-print", dest="enable_log_print", action="store_true",
                                   help="enable print log into terminal")

        self.__parser.add_argument("--enable-debug-mode", dest="enable_debug_mode", action="store_true",
                                   help="Enable debug mode to print detail message")
        
        self.__parser.add_argument("--enable-whole-process", dest="enable_whole_process",         action="store_true", help="Enable tool to complete the whole build process while replaying")
    
    def parse_args(self):
        args = sys.argv
        index = args.index("--")
        self.__build_args = args[index+1:]
        args = args[1:index]
        args_parser = self.__parser.parse_args(args)
        
        if args_parser.capture_only:
            self.__context.CAPTURE_ONLY = True
        
        if args_parser.irbuild_only:
            self.__context.IRBUILD_ONLY = True
            
        if args_parser.clear_work_dirs:
            self.__context.CLEAR_WORK_DIRS = True
        
        if args_parser.enable_graph_dump:
            self.__context.ENABLE_BUILD_GRAPH_DUMP = True
        
        if args_parser.build_pool_size:
            self.__context.BUILD_POOL_SIZE = args_parser.build_pool_size
        
        if args_parser.specify_maple_clang:
            self.__context.MAPLE_CLANG = args_parser.specify_maple_clang
        
        if args_parser.specify_maple_clangcpp:
            self.__context.MAPLE_CLANGCPP = args_parser.specify_maple_clangcpp
        
        if args_parser.specify_maple_bin:
            self.__context.MAPLE_BIN = args_parser.specify_maple_bin

        if args_parser.specify_maple_gcc:
            self.__context.MAPLE_GCC = args_parser.specify_maple_gcc

        if args_parser.specify_maple_maple:
            self.__context.MAPLE_MAPLE = args_parser.specify_maple_maple

        if args_parser.specify_maple_root:
            self.__context.MAPLE_ROOT = args_parser.specify_maple_root

        if args_parser.ir_cat_mode:
            self.__context.IR_CAT_MODE = True
        
        if args_parser.enable_debug_mode:
            self.__context.ENABLE_DEBUG_MODE = True

        if args_parser.enable_whole_process:
            self.__context.ENABLE_WHOLE_PROCESS = True