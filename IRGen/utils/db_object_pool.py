from database.rdb_client import *

class DBObjectPool(object):
    
    # compilers capture db instances
    __gcc_db_inst = None
    __clang_db_inst = None
    __ld_db_inst = None
    __ar_db_inst = None
    __ranlib_db_inst = None
    __strip_db_inst = None
    
    # filecache db instance
    __filecache_db_inst = None
     
    __capture_db_socket = ""
    __filecache_db_socket = ""
    
    @staticmethod
    def set_sockets(cap_sock="", cap_port="0", cache_sock="", cache_port="0"):
        # default we use unix_socket_path to connect with redis server
        DBObjectPool.__capture_db_socket = cap_sock
        DBObjectPool.__filecache_db_socket = cache_sock
        
    @staticmethod
    def _get_gcc_db_inst():
        if not DBObjectPool.__gcc_db_inst:
            DBObjectPool.__gcc_db_inst = GCCCmdDBHandler(unix_socket=DBObjectPool.__capture_db_socket)
        return DBObjectPool.__gcc_db_inst
    
    @staticmethod
    def _get_clang_db_inst():
        if not DBObjectPool.__clang_db_inst:
            DBObjectPool.__clang_db_inst = ClangCmdDBHandler(unix_socket=DBObjectPool.__capture_db_socket)
        return DBObjectPool.__clang_db_inst
    
    @staticmethod
    def _get_ld_db_inst():
        if not DBObjectPool.__ld_db_inst:
            DBObjectPool.__ld_db_inst = LDCmdDBHandler(unix_socket=DBObjectPool.__capture_db_socket)
        return DBObjectPool.__ld_db_inst
    
    @staticmethod
    def _get_ar_db_inst():
        if not DBObjectPool.__ar_db_inst:
            DBObjectPool.__ar_db_inst = ARCmdDBHandler(unix_socket=DBObjectPool.__capture_db_socket)
        return DBObjectPool.__ar_db_inst
    
    @staticmethod
    def _get_ranlib_db_inst():
        if not DBObjectPool.__ranlib_db_inst:
            DBObjectPool.__ranlib_db_inst = RanlibCmdDBHandler(unix_socket=DBObjectPool.__capture_db_socket)
        return DBObjectPool.__ranlib_db_inst

    @staticmethod
    def _get_strip_db_inst():
        if not DBObjectPool.__strip_db_inst:
            DBObjectPool.__strip_db_inst = StripCmdDBHandler(unix_socket=DBObjectPool.__capture_db_socket)
        return DBObjectPool.__strip_db_inst
    
    @staticmethod
    def _get_filecache_db_inst():
        if not DBObjectPool.__filecache_db_inst:
            DBObjectPool.__filecache_db_inst = FileCacheDBHandler(unix_socket=DBObjectPool.__filecache_db_socket)
        return DBObjectPool.__filecache_db_inst
    
    @staticmethod
    def get_db_inst_by_name(name=""):
        
        db_inst_dict = {
            "gcc": DBObjectPool._get_gcc_db_inst,
            "clang": DBObjectPool._get_clang_db_inst,
            "ld": DBObjectPool._get_ld_db_inst,
            "ar": DBObjectPool._get_ar_db_inst,
            "ranlib": DBObjectPool._get_ranlib_db_inst,
            "strip": DBObjectPool._get_strip_db_inst,
            
            "filecache": DBObjectPool._get_filecache_db_inst
        }
        
        func = db_inst_dict.get(name, None)
        
        if not func:
            raise Exception("[ERROR] Don't support such compiler %s", name)
        
        return func()
        
        